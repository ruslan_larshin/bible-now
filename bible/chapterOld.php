<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$Bibles = new \Larshin\Bibles\Bibles();
$arChapter = $Bibles->getChapter($_REQUEST['BOOK'],$_REQUEST['CHAPTER']);
$pager = new \Larshin\Pager\Pager($arChapter -> book -> chapterCount , $arChapter -> chapterNum);
$view = new \Larshin\View\Mainview();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($arChapter -> book -> name , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='main'>
	<?$view -> viewChapter($arChapter , $pager);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>