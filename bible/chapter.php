<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$chapter = \Bible\Verses::getVerses($request['BOOK'], $request['CHAPTER']);
$books = \Bible\Books::getListIblock(['cacheTime' => \Main\Settings::cashTimeD7])['books'];
$firstVerse = array_shift($chapter['verses'])['text'];
foreach ($books as $keyBook => $book) {
  if (
    ($book['name'] == $request['BOOK']) || ($book['fullName'] == $request['BOOK'])
    || ($book['shortName'] == $request['BOOK']) || ($book['code'] == $request['BOOK'])
  ) {
    $chapter['book'] = [
      'now' => $book,
      'preview' => $books[$keyBook - 1],
      'next' => $books[$keyBook + 1],
    ];
  }
}
global $APPLICATION;
$APPLICATION->SetTitle("Библия : {$chapter['book']['now']['name']} {$request['CHAPTER']} глава");
$APPLICATION->SetPageProperty('title', "Библия : {$chapter['book']['now']['name']} {$request['CHAPTER']} глава");
$APPLICATION->SetPageProperty("keywords", "Библия, {$chapter['book']['now']['name']}, {$chapter['book']['now']['fullName']}");
$APPLICATION->SetPageProperty("description", "Библия : {$chapter['book']['now']['fullName']} {$request['CHAPTER']} глава. \r\n {$firstVerse}");

$book = \Bible\Books::getBook($request['BOOK'])['book'];
$breadcrumbs = [
  [
    'title' => 'Библия',
    'url' => '/bible/',
  ],
  [
    'title' => $book['name'],
  ]
];


$Bibles = new \Larshin\Bibles\Bibles();
$arChapter = $Bibles->getChapter($_REQUEST['BOOK'], $_REQUEST['CHAPTER']);
$pager = new \Larshin\Pager\Pager($arChapter->book->chapterCount, $arChapter->chapterNum);
$view = new \Larshin\View\Mainview();

?>

    <div class='main'>
      <? $APPLICATION->IncludeComponent(
        'bible:breadcrumbs',
        '',
        ['items' => $breadcrumbs],
      ); ?>
      <? $APPLICATION->IncludeComponent(
        'bible:chapter2',
        '',
        [
            'book' => $request['BOOK'],
            'chapter' => $request['CHAPTER'],
        ],
      ); ?>

    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>