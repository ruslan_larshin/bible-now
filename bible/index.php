<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
//require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bible.php');
$arBible = new \Larshin\Bibles\Bibles();
$arView = new \Larshin\View\MainView();
$arBook = $arBible -> getBookList();
$arView -> viewBibleInMain($arBook);
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>