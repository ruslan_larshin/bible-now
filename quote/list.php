<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Краткая Библия цитатами");
//1) получим список всех книг Библии!
$bookObject = new \Larshin\Bibles\Bibles();
$bookList = $bookObject -> getBookList();
$quoteObject = new \Larshin\Quote\Quote();
$result = $quoteObject -> getListByBooks();

?>
<div class='quoteList'>
	<?foreach($result as $key => $zavet){?>
		<div class='littleTitle'><?if($key =='OLD'){echo 'Ветхий Завет'; } if($key =='NEW'){echo 'Новый Завет'; }?></div>
			<?foreach($zavet as $book => $quoteList){?>
				<div class='group_title'><?=$book?></div>
				<?foreach($quoteList as $i => $quoteElement){?>
					<div class='qouteItem Flex'>
						<span class="triangle  quoteDetail quoteDetail<?=$quoteElement -> code?>" data-id='<?=$quoteElement -> code?>'>
							<img  class = 'open'  class="sub_button sub_open sub_open_1" data-id="1" src="/local/img/triangleright.png">
							<img class ='close'  class="sub_button sub_close sub_close_1" data-id="1" src="/local/img/triangleup.png">
						</span>&nbsp;
						<div class='itemTitle'><span><?=$i+1?>.&nbsp;</span> <a href = '/quote/<?=$quoteElement -> code?>/' > <?=$quoteElement -> name?></a> </div>
					</div>
					<div class='resultQuoteDeatil resultQuoteDeatil<?=$quoteElement->code?>  borderPopup'></div>
				<?}?>
			<?}?> 
	<?}?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>