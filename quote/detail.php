<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Краткая Библия цитатами");
//1) получим список всех книг Библии!
$api = new \Larshin\MainTools\api();
$tools = new \Larshin\MainTools\Tools();
$arRequest = $api ->getRequest();	
$arQuote = new \Larshin\Quote\Quote();
$arQuote = $arQuote -> getByCode($arRequest['quoteId']);
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($arQuote -> book , '/bible/' . $tools -> translit($arQuote -> book) . '/1/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Краткая Библия цитатами' , '/quote/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($arQuote -> name , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='mainCenter'>
	<div class='title2 bold center'><?=$arQuote -> name ?></div>
	<div class='quoteDetail'>
		<?$view -> viewChapterQuote($arQuote);?>
	</div>
</div>
