<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bible.php');
$arBible = new \Larshin\Bible\Bible();
$searchObject = new \Larshin\Search\Symphony();
$arRequest = $arBible ->getRequest();
$arSymphony = $searchObject->symphonyMainList($arRequest["search"]);
$APPLICATION->SetPageProperty("title", 'Симфония Библии');
$APPLICATION->SetPageProperty("description",'Симфония Библии - это сборник всех слов, встречающихся в синодальном переводе Священного Писания с  указанием всех мест их упоминания.');
$APPLICATION->SetPageProperty("keywords", 'Симфония Библии, Симфония Библии онлайн, симфония для Библии, библейская симфония, библия поиск по слову');  
?>
<div class='searchSymphony active' data-search=''>
	<input class='search_input searchSymphonyInput' type='text' placeholder='Введите запрос для поиска' value='<?=$arRequest["search"]?>'/>
</div>
<div class='searchResult active' data-search='search=<?=$_REQUEST["search"]?>'>
	<div class='alphabetList Flex'> 
		<?foreach(alphabet as $letter){?>
			<div class='letter'><a href ='/symphony/?search=<?=$letter?>'><?=$letter?></a></div>
		<?}?>
	</div>

	<div class='Flex'>
		<?$view -> viewSymphonyList($arSymphony); ?>
	</div>
</div>
<style>
.searchHeader{
	display: none;  
}
</style>
<script>
/*
load_light_result=function(data , flag = false){  
	if($('.searchSymphony').hasClass('active') || flag){
		$('.searchSymphony').removeClass('active');
		$.ajax({
			type: "POST", 
			url: "/local/ajax/searchSymphonyResult.php", 
			data: data,
				success: function(html){ 
					$(".searchResult").html(html); 
					history.pushState(null, null, '/symphony/?'+data);
					$('.searchSymphony').attr('data-search' , '2')
					$('.searchSymphony').addClass('active')
				}
		});
	}
}
load_search_from_enter=function(){
		var keyCode = event.keyCode ? event.keyCode :
		event.charCode ? event.charCode :
		event.which ? event.which : void 0;
		if(keyCode == 13)
		{
			if($('.searchSymphonyInput').val()!=''){
				load_light_result('search='+$('.searchSymphonyInput').val() , true);
			}
		}
}

$(function() {
	$('body').on('keyup', '.searchSymphonyInput', function(event){
		load_light_result('search='+$('.searchSymphonyInput').val());
	});
	$('body').on('blur', '.searchSymphonyInput', function(event){
		load_light_result('search='+$('.searchSymphonyInput').val() , true);
	});
});*/
</script>
<script>
$(function() {
	loadJsFromSearch('searchResult' , 'searchSymphonyInput' , 'symphony');
	loadInput('searchSymphonyInput');
});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
