<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bible.php');
$arBible = new \Larshin\Bibles\Bibles();
//$arSymphony = $arBible->symphonyDetail($_REQUEST['word']);
$searchObject = new \Larshin\Search\Symphony();
$arSymphony['VERSE'] = $searchObject ->getVerseByIdSymphonyResult($_REQUEST['word']);
$arSymphony['SYMPHONY'] = $searchObject ->getById($_REQUEST['word']);
$APPLICATION->SetPageProperty("title", 'Симфония Библии');
$APPLICATION->SetPageProperty("description",'Симфония Библии - это сборник всех слов, встречающихся в синодальном переводе Священного Писания с  указанием всех мест их упоминания.');
$APPLICATION->SetPageProperty("keywords", 'Симфония Библии, Симфония Библии онлайн, симфония для Библии, библейская симфония, библия поиск по слову');  
?>
<div class='symphonyDetail'>
	<div class='title'><?=$arSymphony['SYMPHONY'] -> name?></div>
	<div class='previewText'>В Библии слово "<span class='colorTitleBackground'><?=$arSymphony['SYMPHONY'] -> name?></span>"  упоминается в <?=count($arSymphony['VERSE'])?> стихах.</div>
	<?if($arSymphony)
		{
		foreach($arSymphony['VERSE'] as $verse)
		{
			$view -> viewVerseNoChapter($verse);
		}
	}else{
		echo 'По вашему запросу ничего не найдено';
	}
	?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>