<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/taskGusetBook/BD.php');
?>
<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8" />
	<title></title>
	<link rel="stylesheet" href="style.css" />	
	<script src="//code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
	<script src="/taskGusetBook/script.js" ></script>
</head>
<body>
	<div><h1>Гостевая книга</h1></div>
	<div class='centerBlock'>
		<div class='formAdd'>
			<form id ='addForm' name ='add' class='addForm'>
				<div class='title'>Сохранение новой записи</div>
				<div class='inputDiv'><input name='dtime' class='dtime' type ='date' /></div>
				<div class='inputDiv'><input name='name' class='name' type ='text' placeholder='введите название'/></div>
				<div class='inputDiv'><input name='email' class='email' type ='email' placeholder='введите ваш email'/></div>
				<div class='inputDiv'><input name='body' class='body' type ='text' placeholder='введите комментарий'/></div>
				<div class='center'>
					<div class='button add'>Сохранить</div>
				</div>
				<div class='result red'></div>
			</form>	
		</div>
	</div>
	
	<div class='listItems'>
		<?require_once($_SERVER['DOCUMENT_ROOT'] . '/taskGusetBook/list.php');?>
	</div>
</body>
<?require_once($_SERVER['DOCUMENT_ROOT'] . '/taskGusetBook/style.css');?>
</html>