<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по текстам Священного Писания");
?>
<div class='search searchDetail'>
	<input class='search_input searchInput' type='text' placeholder='Введите запрос для поиска' value='<?=$_REQUEST["search"]?>'/>
</div>
<div class="searchResult">
<?
    global $APPLICATION;
    $APPLICATION->IncludeComponent(
      'bible:search.result',
      '',
      ['search' => $_REQUEST['search']],
    );
?>
</div>
<style> 
.searchHeader{
	display: none;  
}
</style>
<script>
    $(document).ready(function(){
        let search = new Search('searchInput', Search.loadSearchResult);
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
