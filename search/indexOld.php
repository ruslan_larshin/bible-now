<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<div class='search searchDetail'>
	<input class='search_input searchInput' type='text' placeholder='Введите запрос для поиска' value='<?=$_REQUEST["search"]?>'/>
</div>
<div class='searchResult active' data-search='search=<?=$_REQUEST["search"]?>'>
	<?if($_REQUEST["search"]){?>
		<?require_once($_SERVER['DOCUMENT_ROOT'] . '/local/ajax/search/searchResult.php');?>
	<?}?>
</div>
<script>
$(function() {
	loadJsFromSearch('searchResult' , 'searchInput' );
	loadInput('searchInput');
});
</script>
<style> 
.searchHeader{
	display: none;  
}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
