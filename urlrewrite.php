<?php
$arUrlRewrite=array (
  10 => 
  array (
    'CONDITION' => '#^/learn/apocrypha/([-_0-9a-zA-Zа-яА-Я ]+)/([0-9]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'bookCode=$1&chapter=$2',
    'ID' => '',
    'PATH' => '/learn/apocrypha/chapter.php',
    'SORT' => 100,
  ),
  9 => 
  array (
    'CONDITION' => '#^/learn/apocrypha/([-_0-9a-zA-Zа-яА-Я ]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'code=$1',
    'ID' => '',
    'PATH' => '/learn/apocrypha/chapterList.php',
    'SORT' => 100,
  ),
  4 => 
  array (
    'CONDITION' => '#^/bible/([-_0-9a-zA-Zа-яА-Я ]+)/([0-9]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'BOOK=$1&CHAPTER=$2',
    'ID' => '',
    'PATH' => '/bible/chapter.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/learn/rubric/([-_0-9a-zA-Zа-яА-Я ]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'code=$1',
    'ID' => '',
    'PATH' => '/learn/rubric/detail.php',
    'SORT' => 100,
  ),
  8 => 
  array (
    'CONDITION' => '#^/learn/sermon/([-_0-9a-zA-Zа-яА-Я ]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'code=$1',
    'ID' => '',
    'PATH' => '/learn/sermon/detail.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/symphony/([-_0-9a-zA-Zа-яА-Я ]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'word=$1',
    'ID' => '',
    'PATH' => '/symphony/detail.php',
    'SORT' => 100,
  ),
  3 => 
  array (
    'CONDITION' => '#^/bible/([-_0-9a-zA-Zа-яА-Я ]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'BOOK=$1',
    'ID' => '',
    'PATH' => '/bible/book.php',
    'SORT' => 100,
  ),
  6 => 
  array (
    'CONDITION' => '#^/quote/([-_0-9a-zA-Zа-яА-Я ]+)/([\\?]*)([\\s\\S]*)$#',
    'RULE' => 'quoteId=$1',
    'ID' => '',
    'PATH' => '/quote/detail.php',
    'SORT' => 100,
  ),
  5 => 
  array (
    'CONDITION' => '#^/services/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/services/index.php',
    'SORT' => 100,
  ),
  1 => 
  array (
    'CONDITION' => '#^/products/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/products/index.php',
    'SORT' => 100,
  ),
  2 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
);
