<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<link rel="stylesheet" href="/local/style/main.css" />
<div><h1>Список инстурментов для работы с сайтом<h1></div>
	<ol>
		<li>
			<div>импорт Библии из базы данных /tools/bibleUpload/FullBible.txt производится в HightLoadBlocks <a href ='/tools/bibleUpload/makeHgloadBibleFromText.php' >Начать импорт</a></div>
		</li>
		<li>
			<div>импорт книг Библии из базы данных(не дуюблировать) 221 /tools/bibleUpload/FullBible.txt производится в HightLoadBlocks <a href ='/tools/bibleUpload/makeHgloadBooksFromText.php' >Начать импорт</a></div>
		</li>	
		<li>
			<div>импорт симфонии Библии из базы данных(не дуюблировать) 221 /tools/bibleUpload/FullBible.txt производится в HightLoadBlocks <a href ='http://bible-now.ru/tools/bibleUpload/makeSymphony.php' >Начать импорт</a></div>
		</li>
        <li>
            <div>Обновление стихов для краткой Библии цитатами <a href ='http://bible-now.ru/tools/quotes/setVerses/' >Запустить</a></div>
        </li>
	<ol>
<br/>
<?
    $genBookMenu = [
        [
            'title' => 'Генерация Библии цитатами',
            'buttonName' => 'Запустить',
            'url' => '/local/mybook/gen/quotes/'
        ],
        [
            'title' => 'Генерация теста',
            'buttonName' => 'Запустить',
            'url' => '/local/mybook/gen/test/'
        ],
        [
            'title' => 'Генерация тематических подборок',
            'buttonName' => 'Запустить',
            'url' => '/local/mybook/gen/topic/'
        ],
      [
        'title' => 'Генерация проповедей',
        'buttonName' => 'Запустить',
        'url' => '/local/mybook/gen/sermon/'
      ]
    ];
?>
<div><h2>Генерация книги<h1></div>
<ol>
    <?foreach($genBookMenu as $item){?>
    <li>
        <div class="flexLeft"><?=$item['title']?>
            <a href ='<?=$item['url']?>' target="_blank">
                <div class="button"><?=$item['buttonName']?></div>
            </a>
        </div>
    <br/>
    </li>
    <?}?>

<ol>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>