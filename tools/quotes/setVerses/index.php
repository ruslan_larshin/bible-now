<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$quote = \Bible\Quote::getList()['quotes'];
$booksList = \Bible\Books::getList(['idKeys' => 'NUMBER_VALUE'])['books'];
foreach($quote as $key => $item){
    $versesNew = [];
    $quote[$key]['book'] = $booksList[$item['book']];
    foreach($item['verseView'] as $view){
       $verses =  \Bible\Verses::makeFilterByQuote($view)['verses'];
       $versesItems = \Bible\Verses::getVerses($verses['book'], $verses['chapter'], $verses['verses'])['verses'];
       $verseIds = [];
       foreach($versesItems as $verse){
         $verseIds[] = intval($verse['id']);
       }
       $quote[$key]['verseNew'][] = implode('-', $verseIds);
    }
    if(!empty($quote[$key]['verseNew'])){
      \CIBlockElement::SetPropertyValuesEx($quote[$key]['id'], \Bible\Quote::ibCode, ['VERSES' => $quote[$key]['verseNew']]);
    }
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>