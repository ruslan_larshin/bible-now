<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$peoples = \Bible\Peoples::getList(
  [
    'sort' => ['NAME' => 'ASC'],
    'filter' => [
      'PROPERTY_PEOPLE' => false,
      'PROPERTY_CITY' => false,
    ],
    'limit' => 100,
    'select' => ['id', 'name', 'city', 'people'],
  ]
)['peoples'];
?>
<div>
    <div class="flex">
        <div class="name">Название</div>
        <div><input class="city" type='text' value="Город"/></div>
        <div><input class="people" type='text' value="Человек"/></div>
    </div>
  <? foreach ($peoples as $people) { ?>
      <div class="flex">
          <div class="name"><?= $people['name'] ?></div>
          <div><input class="city" data-id='<?= $people['id'] ?>' type='text' value="<?= $people['city'] ?>"/></div>
          <div><input class="people" data-id='<?= $people['id'] ?>' type='text' value="<?= $people['people'] ?>"/></div>
          <div class="text">
            <? $verse =\Bible\Symphony::getList(
              [
                'filter' => ['UF_NAME' => "{$people['name']}"],
                'limit' => 1
              ]
            )['symphony'] ?: [1];
            $verse = array_shift($verse);
            $verse = $verse['verseIds'][0];
            $verse =\Bible\Verses::getList(
              [
                'filter' => ['ID' => $verse],
                'limit' => 1
              ]
            )['verses'] ?: [1];
            $verse = array_shift($verse);
            $verse['text'] = str_replace($people['name'], "<b>{$people['name']}</b>", $verse['text']);
            ?>
              <?=$verse['text']?>
          </div>
      </div>
  <? } ?>
</div>
<?

?>
<style>
    .flex {
        align-items: center;
        display: flex;
        margin-bottom: 10px;
    }

    .name {
        width: 200px;
    }

    .city, .people {
        width: 200px;
        margin-right: 50px;
    }
    .text{
        width: 500px;
        max-width: 500px;
    }
</style>
<script>
    $(document).ready(function () {
        $('body').on('input', '.city', function () {
            let url = '/tools/PeoplesInCity/ajax.php';
            let data = {
                'id': $(this).attr('data-id'),
                'city': $(this).val(),
                'code': 'CITY',
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'html',
                success: function (html) {
                    console.log(data);
                },
            });
        });

        $('body').on('input', '.people', function () {
            let url = '/tools/PeoplesInCity/ajax.php';
            let data = {
                'id': $(this).attr('data-id'),
                'city': $(this).val(),
                'code': 'PEOPLE',
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'html',
                success: function (html) {
                    console.log(data);
                },
            });
        });

    });
</script>