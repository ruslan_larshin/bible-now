<?
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);
define('CHK_EVENT', true);
define('BX_WITH_ON_AFTER_EPILOG', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
set_time_limit(0);
ini_set('max_execution_time', '0');
$count = 1000;
$page = $_REQUEST['page'] ?? 1;
$result = [];
$symphony = \Bible\Symphony::getList(['order' => ['ID' => 'ASC'], 'filter' => ['UF_MAIN_WORD' => false], 'nav' => ['limit' => $count, 'offset' => ($page -1) * $count]], true)['symphony'];
foreach($symphony as $word){
  if(!empty($word['mainWord'])){
    continue;
  }
  $lexems = \Api\Lingvo::getLexems($word['name']);
  if(!empty($lexems['error'])){
    view($lexems); continue;
  }
  $mainWord = $lexems['main'];
  $lexems = $lexems['words'];
  if(empty($lexems) && empty($mainWord)){
    continue;
  }
  $symphonyLexems = [];
  if(!empty($lexems)) {
    $symphonyLexems = \Bible\Symphony::getList(['filter' => ['UF_NAME' => $lexems]], true)['symphony'];
  }
  $add = [
    'lexem' => $lexems,
    'lexemIds' => $word['verseIds'],
    'mainWord' => $mainWord,
  ];
  foreach($symphonyLexems as $item){
    $add['lexem'][$item['name']] = $item['name'];
    $add['lexemIds'] = array_merge($add['lexemIds'] ?? [], $item['verseIds']);
  }
  $add['lexem'] = array_values($add['lexem']);
  $add['lexemIds'] = implode('-', $add['lexemIds']);
  $result[] = \Bible\Symphony::update($word['id'], $add);
}
view($result);
?>
