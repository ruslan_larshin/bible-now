<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
set_time_limit(0);
ini_set('max_execution_time', '0');
$arBook=array(
	'1'=>"Бытие",
	'2'=>"Исход",
	'3'=>"Левит",
	'4'=>"Числа",
	'5'=>"Второзаконие",
	'6'=>"Иисус Навин",
	'7'=>"Судей",
	'8'=>"Руфь",
	'9'=>"1 Цартсв",
	'10'=>"2 Цартсв",
	'11'=>"3 Цартсв",
	'12'=>"4 Цартсв",
	'13'=>"1 Паралипоменон",
	'14'=>"2 Паралипоменон",
	'15'=>"1 Ездры",
	'16'=>"Неемии",
	'17'=>"2 Ездры",
	'18'=>"Товит",
	'19'=>"Иудифь",
	'20'=>"Есфирь",
	'21'=>"Иов",
	'22'=>"Псалтирь",
	'23'=>"Притчи",
	'24'=>"Екклесиаста",
	'25'=>"Песнь Песней",
	'26'=>"Премудрости",
	'27'=>"Сирахова",
	'28'=>"Исаия",
	'29'=>"Иеремия",
	'30'=>"Плач Иеремии",
	'31'=>"Послание Иеремии",
	'32'=>"Варух",
	'33'=>"Иезекииль",
	'34'=>"Даниил",
	'35'=>"Осия",
	'36'=>"Иоиль",
	'37'=>"Амос",
	'38'=>"Авдий",
	'39'=>"Иона",
	'40'=>"Михей",
	'41'=>"Наум",
	'42'=>"Аввакум",
	'43'=>"Софония",
	'44'=>"Аггей",
	'45'=>"Захария",
	'46'=>"Малахия",
	'47'=>"1 Маккавейская",
	'48'=>"2 Маккавейская",
	'49'=>"3 Маккавейская",
	'50'=>"3 Ездры",
	'51'=>"От Матфея",
	'52'=>"От Марка",
	'53'=>"От Луки",
	'54'=>"От Иоанна",
	'55'=>"Деяние",
	'56'=>"Иакова",
	'57'=>"1 Петра",
	'58'=>"2 Петра",
	'59'=>"1 Иоанна",
	'60'=>"2 Иоанна",
	'61'=>"3 Иоанна",
	'62'=>"Иуда",
	'63'=>"Римлянам",
	'64'=>"1 Коринфянам",
	'65'=>"2 Коринфянам",
	'66'=>"Галатам",
	'67'=>"Ефесянам",
	'68'=>"Филлипийцм",
	'69'=>"Коллосянам",
	'70'=>"1 Фессалоникийцм",
	'71'=>"2 Фессалоникийцм",
	'72'=>"1 Тимофею",
	'73'=>"2 Тимофею",
	'74'=>"Титу",
	'75'=>"Филимону",
	'76'=>"Евреям",
	'77'=>"Откровение",
);
function mb_strtoupper_first($str, $encoding = 'UTF8')
{
    return
        mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
        mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
}

$verseObject = new \Larshin\Bibles\Verse();
//1)этап получим список новой симфонии какие данные уже есть
$wordInBase = array();
$hgnewSymphony = new \Larshin\MainTools\HighLoad(242);
$newSymphony = $hgnewSymphony -> getList(array('UF_NAME' , 'UF_ID') , array());
foreach($newSymphony['items'] as $value)
{
	$wordInBase[$value['UF_NAME']] = $value['UF_NAME'];
}
//view($wordInBase);
//2)этап получим список старой симфонии с полями 
$hgOldSymphony = new \Larshin\MainTools\HighLoad(228);
$wordAll = array();
$oldSymphony = $hgOldSymphony -> getList(array('UF_NAME') , array(/*'UF_NAME' => 'давала'*/) ,'ID' , 'ASC' , 100000);
foreach($oldSymphony['items'] as $item)
{
	$wordAll[$item['UF_NAME']] = $item['UF_NAME'];
}
//view($wordAll);
//view($wordInBase);
//3) этап - будем получать сортировку на каждое слово и сохранять идентификаторы!
foreach($wordAll as $word)
{
	if(!$wordInBase[$word] )
	{
		//view($word);
		$Books = new \Larshin\MainTools\HighLoad(Bible);
		$arFilter = Array(
			   Array(
				  "LOGIC"=>"OR",
				  Array(
					 "UF_TEXT" => '%' . $word . '%',
				  ),
				  Array(
					 "UF_TEXT" => '%' . mb_strtoupper_first($word) . '%',
				  )
			   )
			);
		$arId = '';
		$rsData  = $Books ->getList(array('ID' , 'UF_TEXT') , $arFilter);
		foreach($rsData['items'] as $item){
			//view($item);//нам надо определить что это слово, а не вхождение
			$flag = false;
			$buf = preg_replace('%[^A-Za-zА-Яа-я0-9 ]/u%', '', $item['UF_TEXT']);
			$buf = mb_strtolower(trim(str_replace(array('.','!',',','"',"'",' - ',':','(',')','[',']',';','--','?','<','>','1','2','3','4','5','6','7','8','9','0','=','+','`','~','@','#','№'), ' ', $buf))); 
			$buf = explode(' ',$buf);
			foreach($buf as $words)
			{
				if($words == $word)
					$flag = true;
			}
			//view($flag);
			if($flag)
				$arId = $arId . $item['ID']. '-' ;
		}
		$result = $hgnewSymphony -> add(array('UF_NAME' => $word , "UF_ID" => $arId));
		$wordInBase[$word] = $word;
	}
} 
?>