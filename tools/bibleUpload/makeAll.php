<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
set_time_limit(0);
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bible.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Main/Main.php');

$arBook=array(
	'1'=>"Бытие",
	'2'=>"Исход",
	'3'=>"Левит",
	'4'=>"Числа",
	'5'=>"Второзаконие",
	'6'=>"Иисус Навин",
	'7'=>"Судей",
	'8'=>"Руфь",
	'9'=>"1 Цартсв",
	'10'=>"2 Цартсв",
	'11'=>"3 Цартсв",
	'12'=>"4 Цартсв",
	'13'=>"1 Паралипоменон",
	'14'=>"2 Паралипоменон",
	'15'=>"1 Ездры",
	'16'=>"Неемии",
	'17'=>"2 Ездры",
	'18'=>"Товит",
	'19'=>"Иудифь",
	'20'=>"Есфирь",
	'21'=>"Иов",
	'22'=>"Псалтирь",
	'23'=>"Притчи",
	'24'=>"Екклесиаста",
	'25'=>"Песнь Песней",
	'26'=>"Премудрости",
	'27'=>"Сирахова",
	'28'=>"Исаия",
	'29'=>"Иеремия",
	'30'=>"Плач Иеремии",
	'31'=>"Послание Иеремии",
	'32'=>"Варух",
	'33'=>"Иезекииль",
	'34'=>"Даниил",
	'35'=>"Осия",
	'36'=>"Иоиль",
	'37'=>"Амос",
	'38'=>"Авдий",
	'39'=>"Иона",
	'40'=>"Михей",
	'41'=>"Наум",
	'42'=>"Аввакум",
	'43'=>"Софония",
	'44'=>"Аггей",
	'45'=>"Захария",
	'46'=>"Малахия",
	'47'=>"1 Маккавейская",
	'48'=>"2 Маккавейская",
	'49'=>"3 Маккавейская",
	'50'=>"3 Ездры",
	'51'=>"От Матфея",
	'52'=>"От Марка",
	'53'=>"От Луки",
	'54'=>"От Иоанна",
	'55'=>"Деяние",
	'56'=>"Иакова",
	'57'=>"1 Петра",
	'58'=>"2 Петра",
	'59'=>"1 Иоанна",
	'60'=>"2 Иоанна",
	'61'=>"3 Иоанна",
	'62'=>"Иуда",
	'63'=>"Римлянам",
	'64'=>"1 Коринфянам",
	'65'=>"2 Коринфянам",
	'66'=>"Галатам",
	'67'=>"Ефесянам",
	'68'=>"Филлипийцм",
	'69'=>"Коллосянам",
	'70'=>"1 Фессалоникийцм",
	'71'=>"2 Фессалоникийцм",
	'72'=>"1 Тимофею",
	'73'=>"2 Тимофею",
	'74'=>"Титу",
	'75'=>"Филимону",
	'76'=>"Евреям",
	'77'=>"Откровение",
);

$arBookShort=array(
	'1'=>"Быт",
	'2'=>"Исх",
	'3'=>"Лев",
	'4'=>"Чис",
	'5'=>"Втор",
	'6'=>"Нав",
	'7'=>"Суд",
	'8'=>"Руф",
	'9'=>"1Цар",
	'10'=>"2Цар",
	'11'=>"3Цар",
	'12'=>"4Цар",
	'13'=>"1Пар",
	'14'=>"2Пар",
	'15'=>"1Езд",
	'16'=>"Неем",
	'17'=>"2Езд",
	'18'=>"Тов",
	'19'=>"Иудифь",
	'20'=>"Есф",
	'21'=>"Иов",
	'22'=>"Пс",
	'23'=>"Прит",
	'24'=>"Еккл",
	'25'=>"Песн",
	'26'=>"Прем",
	'27'=>"Сир",
	'28'=>"Ис",
	'29'=>"Иер",
	'30'=>"Плач",
	'31'=>"ПосИер",
	'32'=>"Вар",
	'33'=>"Иез",
	'34'=>"Дан",
	'35'=>"Ос",
	'36'=>"Иоил",
	'37'=>"Ам",
	'38'=>"Авд",
	'39'=>"Ион",
	'40'=>"Мих",
	'41'=>"Наум",
	'42'=>"Ав",
	'43'=>"Соф",
	'44'=>"Аг",
	'45'=>"Зах",
	'46'=>"Мал",
	'47'=>"1Макк",
	'48'=>"2Макк",
	'49'=>"3Макк",
	'50'=>"3Езд",
	'51'=>"Мф",
	'52'=>"Мк",
	'53'=>"Лк",
	'54'=>"Ин",
	'55'=>"Деян",
	'56'=>"Иак",
	'57'=>"1Пет",
	'58'=>"2Пет",
	'59'=>"1Ин",
	'60'=>"2Ин",
	'61'=>"3Ин",
	'62'=>"Иуд",
	'63'=>"Рим",
	'64'=>"1Кор",
	'65'=>"2Кор",
	'66'=>"Гал",
	'67'=>"Еф",
	'68'=>"Филл",
	'69'=>"Колл",
	'70'=>"1Фесс",
	'71'=>"2Фесс",
	'72'=>"1Тим",
	'73'=>"2Тим",
	'74'=>"Тит",
	'75'=>"Флм",
	'76'=>"Евр",
	'77'=>"Откр",
);
$Nekanon=array(17,18,19,26,27,31,32,47,48,49,50);
$file = fopen($_SERVER["DOCUMENT_ROOT"]."/tools/bibleUpload/FullBible.txt","r");
if(!$file)
{
  echo("Ошибка открытия файла");
}
else
{
  $buff = fread ($file,10000000);
  $buff=str_replace('===','@',$buff);
  $arBible=explode('==',$buff);
  $arFullBible=array();
  $Book='';
  foreach($arBible as $value){
	if($value!=' '){
		if(strlen($value)<200){
			$Book=$value;
		}else{
			$Text=$value;
			$arChapter=array();
			$bufText=explode('@',$Text);
			foreach($bufText as $chapter){
				if(strlen($chapter)<8){
					$chapterNum=$chapter;
				}else{
					$bufChapter=$chapter;
						for($i=0;$i<200;$i++){
							$bufChapter=str_replace((200-$i),'+',$bufChapter);
						}
					$bufChapter=explode('+',$bufChapter);
					$arChapter[]=array(
						'Number'=>$chapterNum,
						"Text"=>$bufChapter
					);
				}
			}
			$arFullBible[]=array(
				"Book"=>$Book,
				"TEXT"=>$arChapter
			);
			
		}
	}
  }
}
$id = 241;
$Bible = new \Larshin\Bible\Bible();
$Hg = new \Larshin\MainTools\HighLoad($id);
$tools = new \Larshin\MainTools\Tools();
$result = $Hg -> getList(array('UF_SHORT' , 'UF_CHAPTER' , 'UF_VERSE') , array() , 'ID' , 'ASC' , 100000 , 0 , 1);
$oldBible = array();
foreach($result as $item)
{
	$oldBible[$item['UF_SHORT'] . '-' .  $item['UF_CHAPTER'] . '-' . $item['UF_VERSE']] = 1;
}
//$result ->entityHgload::getList('select' => array('ID', 'UF_SHORT' , 'UF_CHAPTER' , 'UF_CHAPTER') );
view($oldBible);
foreach($arFullBible as $key => &$value){
	$value['BookFull'] = trim($value['Book']);
	$value['BookShort'] = $arBookShort[$key+1];
	$value['Book'] =  $arBook[$key+1];
	if($key == 0 || 1==1)
	{
			$zavet = 'Ветхий';
			if($key > 49)
				$zavet = 'Новый';
			foreach($value['TEXT'] as $key=>$arChapter)
			{
				foreach($arChapter['Text'] as $verseKey => $verse)
				{
					if($verse && $verseKey!=0)
					{
						if(!$oldBible[$value['BookShort'] . '-' . $arChapter['Number']*1 . '-' . $verseKey]){
							$result = $Bible->initHighLoad($id)::add(array('UF_SHORT' => $value['BookShort'], 'UF_BOOK' => $value['Book'],'UF_FULL'=>$value['BookFull'],'UF_TEXT'=>$verse,'UF_ZAVET'=>$zavet,'UF_CHAPTER'=>$arChapter['Number']*1,'UF_VERSE'=>$verseKey ,'UF_CODE_BOOK' => $tools-> translit($value['BookFull'])));
							//view($value['BookShort'] . $arChapter['Number']*1 . $verseKey);
						}
						//echo '<pre>'; print_r($result); echo '</pre>'; 
					}
				}
			}
		
	}
}

//for($i=72;$i<175;$i++)
	// Bitrix\Highloadblock\HighloadBlockTable::delete($i);

//echo '<pre>'; print_r($arFullBible); echo '</pre>';