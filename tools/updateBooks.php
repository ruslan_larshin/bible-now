<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
//получим все стихи у которых книги фесс
$bibleObject = new \Larshin\Bibles\Bibles();
$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
$result2 = $bibleAll -> getList(array('UF_SHORT' , 'UF_FULL' , 'UF_TEXT' , 'UF_ZAVET' , 'UF_CHAPTER' , 'UF_VERSE' , 'UF_CODE_BOOK' ,"ID" , 'UF_BOOK') , array('UF_BOOK' => array('2 Фессалоникийцм' , '1 Фессалоникийцм' , 'Филлипийцм') ) , 'ID' , 'ASC' , 100000);
foreach($result2['items'] as $item)
{
	$bibleAll -> update($item['ID'] , array('UF_BOOK' => str_replace('цм','цам',$item['UF_BOOK'])));
	//view($item);
}


/*
	Array
(
    [UF_SHORT] => 2Фесс
    [UF_FULL] => Второе послание к Фессалоникийцам (Солунянам) святого апостола Павла
    [UF_TEXT] =>  Не помните ли, что я, еще находясь у вас, говорил вам это?

    [UF_ZAVET] => Новый
    [UF_CHAPTER] => 2
    [UF_VERSE] => 5
    [UF_CODE_BOOK] => VtoroeposlaniekFessalonikiycamSolunyanamsvyatogoapostolaPavla
    [ID] => 355254
    [UF_BOOK] => 2 Фессалоникийцм
)*/?>