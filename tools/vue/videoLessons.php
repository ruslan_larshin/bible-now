<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vuex@4.0.0/dist/vuex.global.js"></script>

</head>
<body>
<template>
    <div id="v-main-wrapper">
        <p>{{title}}</p>
    </div>
    <div id="restApi">
        <div id="mainImg">
            <img :src="itemImage"/>
        </div>
    </div>
</template>
<script>
    const url = 'https://lira.notamedia.ru/';
    new Vue({
        el: '#v-main-wrapper',
        data() {
            return {
                title: 'Main wrapper'
            };
        },
        mounted() {
            console.log('Hello')
        }
    });

    new Vue({
        el: '#restApi',
        data() {
            return {
                itemImage: null
            };
        },
        mounted() {
            axios
                .get('https://lira.notamedia.ru/local/nota/mainPage/')
                .then(response => (this.itemImage = url + response.data.mainNews.slider[0].url));
        }
    });
    /*export default{
        name : 'v-main-wrapper',
        components:{},
        props: {},
        data(){
            return{
                title: 'Main wrapper'
            }
        },
        computed:{},
        methods:{},
        watch:{},
        mounted() {
            console.log('Hello')
        }
    }*/
</script>
<style>
    #v-main-wrapper{
        display: flex;
        max-width: 900px;
        margin: 0 auto;
        align-items: center;
        justify-content: center;
    }
</style>
</body>

