<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vuex@4.0.0/dist/vuex.global.js"></script>
</head>
<body>
<div id="restApi">
    <div id="v-main-wrapper">
        <p>{{title}}</p>
    </div>
    <div id="components-demo">
        <button-counter></button-counter>
    </div>
    <div id="components-demo2">
        <button-counter></button-counter>
    </div>
</div>

<div id="reactiveLessons">
    <span>{{text}}</span>
</div>
<button
</body>
<script>
    const url = 'https://lira.notamedia.ru/';

    var vm = new Vue({
        el: '#reactiveLessons',
        components:{},
        props: {},
        data() {
            return {
                text: 'text'
            };
        },
        computed:{},
        methods:{},
        watch:{},
        mounted() {
            console.log('Hello')
        }
    });

    new Vue({
        el: '#v-main-wrapper',
        components:{},
        props: {},
        data() {
            return {
                title: 'Main wrapper'
            };
        },
        computed:{},
        methods:{},
        watch:{},
        mounted() {
            console.log('Hello')
        }
    });

    Vue.component('button-counter', {
        data: function () {
            return {
                count: 0
            }
        },
        template: '<button v-on:click="count++">Счётчик кликов — {{ count }}</button>'
    })
    new Vue({ el: '#components-demo' })
    new Vue({ el: '#components-demo2' })
</script>
</html>
<style>
    :root {
        --cubic: #372e6c;
        --white: white;
    }

    #bannerHeader img{
        width: 100%;
    }
    #menusBlock{
        width: 100%;
        height: 55px;
        background-color: var(--cubic);
        display: flex;
        align-items: center;
        padding-left: 10px;
    }
    .socialIcon{
        width: 30px;
        height: 30px;
        background-color: var(--white);
        border-radius: 5px;
    }
</style>