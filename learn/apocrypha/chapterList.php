<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$apocryphaObject = new \Larshin\Apocrypha\Apocrypha();
$listApocryphaBook = $apocryphaObject -> getListChapter($_REQUEST['code']);
ksort($listApocryphaBook);
$infoAboutBook = $apocryphaObject -> getInfoByBook($_REQUEST['code']);
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Апокрифы' , '/learn/apocrypha/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($infoAboutBook -> name , '' );
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='main'> 
	<div class='title1'><?=$infoAboutBook -> name?></div>
	<div class='font'><?echo ($infoAboutBook -> description);?></div>
	<br/>
	<?foreach($listApocryphaBook as $chapter => $numVerse){?>
		<div> 
			<a href = '/learn/apocrypha/<?=$infoAboutBook -> code?>/<?=$chapter?>/'  class='font noDecoration black'>
				Глава <?=$chapter?> <span class='azure'>(<?=$numVerse?> стих<?$view -> endWord($numVerse)?>)</span>
			</a>
		</div>
	<?}?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>