<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$apocryphaObject = new \Larshin\Apocrypha\Apocrypha();
$listApocryphaBook = $apocryphaObject -> getListBooks();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Апокрифы' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='main'>
	<?$view -> viewListBook($listApocryphaBook);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>