<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$apocryphaObject = new \Larshin\Apocrypha\Apocrypha();
$arChapter = $apocryphaObject->getChapter($_REQUEST['bookCode'],$_REQUEST['chapter']);
$pager = new \Larshin\Pager\Pager($arChapter -> book -> chapterCount , $arChapter -> chapterNum);
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Апокрифы' , '/learn/apocrypha/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($arChapter -> book -> name , '/learn/apocrypha/' . $arChapter -> book  -> code . '/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($arChapter -> book -> name .' Глава ' . $_REQUEST['chapter'] , '' );
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='main'>
	<?$view -> viewChapter($arChapter , $pager , null , '/learn/apocrypha/' );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>