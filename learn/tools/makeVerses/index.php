<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Печать стихов для проповедывания");
$Bibles = new \Larshin\Bibles\Bibles();
$arChapter = $Bibles->getChapter($_REQUEST['BOOK'],$_REQUEST['CHAPTER']);
$pager = new \Larshin\Pager\Pager($arChapter -> book -> chapterCount , $arChapter -> chapterNum);
$view = new \Larshin\View\Mainview();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Инструменты для работы с Библией' , '/learn/tools/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Печать стихов для проповеди' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='main'>
	<form class='formData'>
		<div class='title2'>Введите сюда ссылки из Библии</div>
		<div>
			<input class='nameVerseWord search_input' type = 'text' placeholder = 'Введите навзание вашей подборки' />
			<textarea class='bigTextarea noClear' type='textarea'  placeholder =''>Введите сюда ссылки формата Пс 3: 1-1</textarea>
		</div>
		<div class='right'>
			<div class='button add verseByWord'>Сохранить</div>
		</div>
		<br/>
	</form>
	<div class='searchResult'></div>
	<??>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>