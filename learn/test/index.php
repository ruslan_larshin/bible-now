<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$apocryphaObject = new \Larshin\Apocrypha\Apocrypha();
$listApocryphaBook = $apocryphaObject -> getListBooks();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библейский тест' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
$testObject = new \Larshin\Test\Test();
$viewTestObject = new \Larshin\View\Test();
$bookList = $testObject -> getBookListrByTest();
foreach($bookList as $book => $cnt){
    $listBookSelect [] = ['id' => $book , 'value' => $book . ' (' .$cnt . ')'];
}
?>
<div class='main unselectable'>
	<div class='title1 center'>Библейcкий тест</div>
		<div class='mainCenter'>
			
			<div class='tabContainer'>
				<div class='tabTitle Flex Width100'>
					<div id='tabs1'  data-id='1' class='tabTtitleElment bold verticalCenter active'>Вопросы по библии</div>
					<div id='tabs2'  data-id='2' class='tabTtitleElment bold verticalCenter noActive'>Игра</div>
				</div>
			</div>
		
			<div  class='tabContent'  id='tabsElement1'>
                <div class="filterTest">
                    <div class="center">Фильтр вопросов <span class="noFilter btn1 pointer">(скрыть)</span><span class="noFilter btn2 pointer">(показать)</span></div>
                    <br />
                    <div class="DesctopFlex">
                        <div class="groupLevel groupFilter ">
                            <?for($i = 1; $i <5 ;$i++){?>
                                <div class="Flex">
                                    <div class="checkbox" data-level="<?=$i?>"></div><div class="unselectable" data-level="<?=$i?>"> <?=$i?> уровень сложности</div>
                                </div>
                            <?}?>
                        </div>
                        <div class="groupBook groupFilter ">
                            <?$viewTestObject -> selectListMulty($listBookSelect , 'По книгам')?>
                            <br/>
                            <div class="Flex">
                                <div class="checkbox oneAnswer" ></div><div class="unselectable" data-level="<?=$i?>"> Только с одним ответом</div>
                            </div>
                            <div class="Flex">
                                <div class="checkbox viewBook" ></div><div class="unselectable" > Показывать книги</div>
                            </div>
                            <div class="Flex">
                                <div class="checkbox noViewAnswer" ></div><div class="unselectable" > Скрывать ответ</div>
                            </div>
                        </div>
                        <div class="groupButton groupFilter center ">
                            <div class="button testListFilter">Найти</div>
                        </div>
                    </div>
                </div>
                <div class="testListresultAjax">
                    <?require($_SERVER["DOCUMENT_ROOT"]."/local/ajax/test/listGame.php");?>
                </div>
			</div>
			<div class='tabContent noDisplay' id='tabsElement2'>
                <?require($_SERVER["DOCUMENT_ROOT"]."/local/game/index.php");?>
			</div>
		</div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>