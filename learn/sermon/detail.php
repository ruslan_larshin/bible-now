<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$sermonObject = new \Larshin\Sermon\Sermon();
$sermon = $sermonObject-> getByCode($_REQUEST['code']);
$view = new \Larshin\View\Mainview();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Проповеди' , '/learn/sermon/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($sermon -> name, '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class=''>
	<div class='title2 bold center'><?=$sermon -> name?></div>
	<div class=''>
		<div><?=$sermon -> text?></div>
	</div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?> 