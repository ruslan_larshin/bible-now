<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$Bibles = new \Larshin\Bibles\Bibles();
$view = new \Larshin\View\Mainview();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Проповеди' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>

<div class='title'>Проповеди</div>
<div class='searchSermon active' style= 'max-width: 1000px;' data-search='<?=$_REQUEST["search"]?>'>
	<input class='search_input  searchSermonInput' type='text' placeholder='Введите запрос для поиска тематической подборки' value='<?=$_REQUEST["search"]?>'/>
</div>
<br/> 
<div class='sermonResult active' data-search='search=<?=$_REQUEST["search"]?>'> 
	<?require_once($_SERVER["DOCUMENT_ROOT"] . "/local/ajax/sermon/sermonResult.php");?>
</div>
<script>
$(function() {
	loadJsFromSearch('sermonResult' , 'searchSermonInput' , 'sermon');
	loadInput('searchSermonInput');
});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?> 