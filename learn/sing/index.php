<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Проповеди' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<? $APPLICATION->IncludeComponent(
  'main:search',
  '',
  $arParams = [
    'title' => 'Пою Господу',
    'class' => 'sing',
    'url' => '/learn/sing/',
    'pause' => 100,
    'ajaxUrl' => '/learn/sing/ajax.php',
    'placeholder' => 'Введите название псалма или часть текста'
  ],
); ?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>