<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT']."/local/img/svg/index.php");
if(empty($request['id'])){
  die('Ошибка: не передан идентификатор псалтыря');
}
$sing = \Learn\Sing::getList(
  [
    'filter' => ['ID' => $request['id']],
    'nav' => ['limit' => 1]
  ]
)['items'][0];
$copyText = "{$sing['name']} \r\n {$sing['text']} \r\n [{$sing['path']}]";
?>
<div class="songComponent flexCenter">
  <div class="songComponentDiv">
    <div class="title2 bold center">
        <span class="copyText"
              title="скопировано"
              data-copy="<?=$copyText?>"><?=$GLOBALS['svg']['copy']?>
        </span><?=$sing['name']?>
    </div>
    <div class="songText"><?=$sing['text']?></div>
    <div class="songPath right blue offset  ">
      [ <?=$sing['path']?> ]
    </div>
  </div>
</div>
<style>
    .songText {
       white-space: pre-wrap;
    }
    .songComponent{
        width: 100%;
        height: 100%;
    }
</style>
