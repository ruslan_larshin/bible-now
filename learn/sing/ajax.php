<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
$sings = \Learn\Sing::getList(
  [
    'order' => ['NAME' => 'ASC'],
    'filter' => !empty($request['search']) ? ['NAME' => '%' . $request['search'] . '%'] : [],
  ]
)['items'];

foreach($sings as $key => $sing){
  $sings[$key]['title'] = "{$sing['name']} <span class='blue white-space: nowrap'> [{$sing['path']}]</span>" ;
  //$sings[$key]['href'] = '/learn/sing/' . $sing['id'] . '/';
  $sings[$key]['popup'] = '/learn/sing/detail.php?id=' . $sing['id'];
}
global $APPLICATION;
$APPLICATION->IncludeComponent(
  'main:list',
  '',
  $arParams = [
    'class' => 'singList',
    'items' => $sings,
  ],
);

?>