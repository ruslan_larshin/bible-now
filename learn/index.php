<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$Bibles = new \Larshin\Bibles\Bibles();
$arChapter = $Bibles->getChapter($_REQUEST['BOOK'], $_REQUEST['CHAPTER']);
$pager = new \Larshin\Pager\Pager($arChapter->book->chapterCount, $arChapter->chapterNum);
$view = new \Larshin\View\Mainview();
$arBreadcrumbs = array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия', '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания', '');
$arBreadcrumbs[0]->view($arBreadcrumbs);

$arLearn = array(
  array(
    'name' => 'Тематическая подборка',
    'url' => '/learn/rubric/',
  ),
  array(
    'name' => 'Проповеди',
    'url' => '/learn/sermon/',
  ),
  array(
    'name' => 'Апокрифы',
    'url' => '/learn/apocrypha/',
  ),
  array(
    'name' => 'Словари',
    'url' => '/learn/dictionary/',
  ),
  array(
    'name' => 'Инструменты для изучения',
    'url' => '/learn/tools/',
  ),
  array(
    'name' => 'Вопросы по Библии',
    'url' => '/learn/test/',
  ),
  [
    'name' => 'Пою Господу',
    'url' => '/learn/sing/',
  ],
);
?>
    <div class='main'>
        <ul>
          <? foreach ($arLearn as $item) { ?>
              <li>
                  <a class='noDecoration title2 font black' href='<?= $item['url'] ?>'><?= $item['name'] ?></a>
              </li>
          <? } ?>
        </ul>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>