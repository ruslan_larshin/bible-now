<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$Bibles = new \Larshin\Bibles\Bibles();
$tools = new \Larshin\MainTools\Tools();
$rubricObject = new \Larshin\Rubric\Rubric();
$rubric = $rubricObject -> getByCode($_REQUEST['code']);
//$b = array('productGroup' => array(1,2) , 'brand' => 'code' , 'comments' => 'text');
//echo json_encode($b, JSON_UNESCAPED_UNICODE);
//view($rubric);
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Тематическая подборка' , '/learn/rubric/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs($rubric->name , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='mainCenter'>
	<?if(count($rubric -> verseOne) > 0){?>
		<div class='tabContainer'>
			<div class='tabTitle Flex Width100'>
				<div id='tabs1'  data-id='1' class='tabTtitleElment bold verticalCenter active'>Полная</div>
				<div id='tabs2'  data-id='2' class='tabTtitleElment bold verticalCenter noActive'>Цитаты</div>
			</div>
		</div>
	<?}?>
	<div  <?if(count($rubric -> verseOne) > 0){?> class='tabContent' <?}?> id='tabsElement1'>
		<? $view -> viewVerseByBooksGroup($rubric -> name , $rubric -> code , $rubric -> verse);?>
	</div>
	<div class='tabContent noDisplay' id='tabsElement2'>
		<? $view -> viewVerseByBooksGroup($rubric -> name , $rubric -> code  , $rubric -> verseOne ,'one');?>
	</div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>