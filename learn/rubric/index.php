<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библия");
$Bibles = new \Larshin\Bibles\Bibles();
$arChapter = $Bibles->getChapter($_REQUEST['BOOK'],$_REQUEST['CHAPTER']);
$pager = new \Larshin\Pager\Pager($arChapter -> book -> chapterCount , $arChapter -> chapterNum);
$view = new \Larshin\View\Mainview();
$arBreadcrumbs =array();
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Библия' , '/bible/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Изучение Священного Писания' , '/learn/');
$arBreadcrumbs[] = new \Larshin\Breadcrumbs\Breadcrumbs('Тематическая подборка' , '');
$arBreadcrumbs[0] -> view($arBreadcrumbs);
?>
<div class='title'>Список тематик</div>
<div class='searchRubric active' data-search=''>
	<input class='search_input  searchRubricInput' type='text' placeholder='Введите запрос для поиска тематической подборки' value='<?=$arRequest["search"]?>'/>
</div>
<div class='rubricResult active' data-search='search=<?=$_REQUEST["search"]?>'> 
	<?require_once($_SERVER["DOCUMENT_ROOT"] . "/local/ajax/rubric/rubricResult.php");?>
</div>
<script>
$(function() {
	loadJsFromSearch('rubricResult' , 'searchRubricInput' , 'rubric');
	loadInput('searchRubricInput');
});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>