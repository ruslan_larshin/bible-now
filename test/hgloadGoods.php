<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Hgload/Hgload.php');

global $USER;
?>
<script src="/local/js/jquery.js"></script>
<form class='save' id='save_form'>
	<div><input name='UF_NAME' type='text' placeholder='Наименование' id='NAME' /></div>
	<div><input name='UF_PRICE' type='number' placeholder='Цена' id='PRICE'/></div>
	<div><img class='fileLoad' src = '/local/img/noPhoto.jpg'/></div>
	<div><input name='UF_PICTURE' type="file" accept = "image/*" placeholder='' id='PICTURE'/></div>
	<div><input  name='UF_DATE' type='date' placeholder='01.10.21' id='DATE'/></div>
</form>
<div class='submit'>Сохранить</div> 
<div class='result'></div>
<script>   
$(document).ready(function(){
	$('body').on('click', '.submit', function(){
		var form = $('#save_form')[0]; 
		var data = new FormData(form);
		$.ajax({
			type: "POST",
			enctype: 'multipart/form-data',
			url: "/local/ajax/saveGoods.php",
			data: data,
			processData: false,
			contentType: false,
			cache: false,
			timeout: 800000,
			success: function (html) {
				$(".result").html(html);  
			},
			error: function (e) {
				alert('Ошибка аякса-файл не запущен!"');
			}
		});
	});
	$('#PICTURE').change(function () {
        var input = $(this)[0];
        if (input.files && input.files[0]) {
            if (input.files[0].type.match('image.*')) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.fileLoad').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                console.log('ошибка, не изображение');
            }
        } else {
            console.log('хьюстон у нас проблема');
        }
    });
});
</script> 
<style>
.fileLoad{
	width: 300px;
	border: 1px solid black;
	padding: 30px;
}
</style>
