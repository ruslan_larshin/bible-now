<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php"); 
require_once($_SERVER['DOCUMENT_ROOT'].'/local/HighLoad/lib/Hgload/Hgload.php');
global $USER;
$arGoods = new \Hgload\Hgload(); 
$arRequest = $arGoods->getRequest();
$arList = $arGoods->getListGoods($arRequest['PAGE']); 
?>
<style>
.navPages:hover{
	color : #ca3e6f;
}	
.navPages{
	font-weight: bold;
	margin: 0 5px;
    background-color: #fff;
    color: #2d2556;
    min-width: 30px;
    border-radius: 2px;
	text-decoration: none;
	padding: 0 4px;
    background: #fff;
    vertical-align: top;
    display: inline-block;
    font-size: 13px;
    height: 28px;
    line-height: 28px;
    cursor: pointer;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    text-align: center;
	border: 1px solid #cdccd7;
    color: #7e7a98;
}
.navPages.points{
	border:none;
	background-color: #f8f9fb;
}
.navPages.active{
	border: 1px solid #533eca;
    background-color: #fff;
    color: #7e7a98;
}
.pager{
	display: flex;
}
.blockBlue{
    background: linear-gradient(#69D4FF, #fcfcfc 100%);
    text-align: center;
    box-shadow: #00a9ee 1px 1px 1px 0px;
    cursor: pointer;
    border-top-right-radius: 15px;
    border-bottom-right-radius: 15px;
    border-bottom-left-radius: 15px;
    user-select: none;
	padding: 10px 20px 10px 20px;
}
.itemList .save,.itemList.active .editPopup{
	display: none;
}
.itemList.active .save{
	display: block;
}
.hgloadList{
	width : 100%;
}
.itemList{
	width: 100%;
	background-color: white;
	display: flex;
	height: 60px;
	align-items: center;
	justify-content: center;
}
.hgloadPicture img{
	max-width: 60px;
    max-height: 60px;
}
.hgloadId{
	width: 5%;
}
.hgloadName{
	width: 45%;
}
.hgloadPrice{
	width: 10%;
}
.hgloadPicture{
	width: 7%;
}
.hgloadDate{
	width: 8%;
}
.success{
	color: green;
	font-weight: bold;
}
.errors{ 
	color: red;
	font-weight: bold;
}
.addHgload{
	width: 200px;
	margin-bottom: 30px;
}
.itemListFirst{
	display: flex;
    font-size: 14px;
    line-height: 16px;
    font-weight: 700;
    font-style: normal;
    color: #c7c5d3;
	align-items: center;
    justify-content: center;
	width: 100%;
    margin: 0 -5%;
}
.main{
	background-color: #f8f9fb;
	padding: 2%;
}
.btnEdit{
	background-color: rgba(0,0,0,0);
    border-color: #eee;
    color: #533eca;
    margin-right: 16px;
}
.btn{
	display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
    position: relative;
    text-align: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    overflow: hidden;
    font-size: 14px;
    line-height: 14px;
    font-weight: 400;
    font-style: normal;
    padding: 10px 20px;
    border-width: 1px;
    border-style: solid;
    border-radius: 4px;
	cursor: pointer;
}
.btnDel{
    background-color: #533eca;
    border-color: #533eca;
    color: #fff;
}
.v-field__input {
    position: relative;
    min-width: 60%;
    max-width: 60%;
    color: black;
    font-size: 14px;
    line-height: 24px;
    padding: 5px 12px;
    border: 1px solid #eee;
    -webkit-box-shadow: 0 1px 0 #eeedf5;
    box-shadow: 0 1px 0 #eeedf5;
    background-color: #fff;
    border-radius: 2px;
    overflow: hidden;
	outline:none;
}
input::-webkit-input-placeholder       {opacity: 1; transition: opacity 0.3s ease;}
input::-moz-placeholder                {opacity: 1; transition: opacity 0.3s ease;}
input:-moz-placeholder                 {opacity: 1; transition: opacity 0.3s ease;}
input:-ms-input-placeholder            {opacity: 1; transition: opacity 0.3s ease;}
input:focus::-webkit-input-placeholder {opacity: 0; transition: opacity 0.3s ease;}
input:focus::-moz-placeholder          {opacity: 0; transition: opacity 0.3s ease;}
input:focus:-moz-placeholder           {opacity: 0; transition: opacity 0.3s ease;}
input:focus:-ms-input-placeholder      {opacity: 0; transition: opacity 0.3s ease;}
.control__button{
    width: 24px;
    min-width: 24px;
    background-color: rgba(0,0,0,0);
    line-height: 24px;
    height: 24px;
    border: 2px solid #eee;
    border-radius: 2px;
    -webkit-transition: background-color .3s;
    -o-transition: background-color .3s;
    transition: background-color .3s;
    cursor: pointer;
    background-size: 14px;
    background-position: 50%;
    background-repeat: no-repeat;
	margin: 20px 20px 20px 0px;
}
.flex{
	display: flex;
	align-items: center;
}
.control__button.active{
	background-color: #982a75;
    border-color: #982a75; 
}
</style>
<script src="/local/js/jquery.js"></script>
<div class='main'>
<h1>Список товаров из HgloadBlock 2</h1>
<div class='addHgload blockBlue'>Добавить элемент</div>
<div class='topService'>
	<input value="" type="text" name="content-head-field-search" id="searchList"max="" min="" maxlength="" placeholder="Поиск по ключевым словам" step="" autocomplete="on" class="v-field__input">
	<div class='flex'><div class="control__button allCheckbox"></div><div>Выбрать все</div></div>
</div>
<div class='hgloadList'>
	<?$APPLICATION->IncludeComponent("larshin:listHgload", ".default", array('PAGE'=>$arRequest['PAGE']),false);?> 
</div>

</div>
<footer>
<?$APPLICATION->IncludeComponent("larshin:popup", "main", array(),false);?> 
</footer>
<script>
var jsonUpdate;
$('body').on('keyup','#searchList',function(){
	//alert($(this).val()); 
});
$('body').on('click','.control__button',function(){
	if(!$(this).hasClass('allCheckbox')){
		$(this).toggleClass('active');
		$('.allCheckbox').removeClass('active');
	}else{
		if($(this).hasClass('active')){
			$('.control__button').removeClass('active');
		}else{
			$('.control__button').addClass('active');
		}
	}
});
//редактирование в строке таблицы(доп вариант)
$('body').on('click','.edit',function(){
	if(!$('.popup').hasClass('stop')){
		$('.popup').addClass('stop')
		var id = $(this).attr('data-id');
		var data = 'ID='+id;
		$.ajax({
			type: "POST", 
			url: "/local/ajax/editHgloadLine.php", 
			data: data,
				success: function(html){ 
					$(".itemList"+id).html(html);					
					$(".itemList"+id).addClass('active');	
					$('.popup').removeClass('stop');
				}
		});
	}
});
//--редактирование в строке таблицы(доп вариант)
	
//редактирование popup
$('body').on('click','.editPopup',function(){
	if(!$('.popup').hasClass('stop')){
		$('.popup').addClass('stop')
		var id = $(this).attr('data-id');
		popupOpen();
		var data = 'ID='+id;
		$.ajax({
			type: "POST", 
			url: "/local/ajax/Hgload/edit.php", 
			data: data,
				success: function(html){ 
					$(".contentPopup").html(html);	
					$('.popup').removeClass('stop');					
				}
		});
	}
});
//--редактирование popup

//addNew popup
$('body').on('click','.addHgload',function(){
	if(!$('.popup').hasClass('stop')){
		$('.popup').addClass('stop')
		popupOpen();
		$.ajax({
			type: "POST", 
			url: "/local/ajax/Hgload/add.php", 
				success: function(html){ 
					$(".contentPopup").html(html);	
					$('.popup').removeClass('stop');						
				}
		});
	}
});
//--addNew popup

function hgloadSave(data,url)
{
	$('.errorPopup').html(''); 
	$('.resultPopup').html(''); 
	$(".resultPopup").html('Подождите! Идет загрузка данных на сервер!');  
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		url: url,
		data: data,
		processData: false,
		contentType: false,
		cache: false,
		timeout: 800000,
		success: function (html) {
			$(".resultPopup").html(''); 
			jsonUpdate = JSON.parse(html);
			if(jsonUpdate.SUCCESS != 'undefined'){
				$('.resultPopup').html(jsonUpdate.SUCCESS);
			}
			if(jsonUpdate.ERROR != 'undefined'){
				$('.errorPopup').html(jsonUpdate.ERRORS); 
			}		
		},
		error: function (e) {
			alert('Ошибка аякса-файл не запущен!"');
		}
	});
}

$('body').on('click', '.savePopup', function(){
	var id = $(this).attr('data-id');
	var form = $('.form'+id)[0]; 
	var data = new FormData(form);
	data.append('ID',id);
	hgloadSave(data,"/local/ajax/Hgload/update.php")

});	

$('body').on('click', '.addPopup', function(){
	var form = $('.formAdd')[0]; 
	var data = new FormData(form);
	hgloadSave(data,"/local/ajax/Hgload/save.php")

});	
	
$(document).ready(function(){
	$('body').on('click', '.save', function(){
		var id = $(this).attr('data-id');
		var form = $('#form'+id)[0]; 
		var data = new FormData(form);
		data.append('ID',id);
		$.ajax({
			type: "POST",
			enctype: 'multipart/form-data',
			url: "/local/ajax/Hgload/update.php",
			data: data,
			processData: false,
			contentType: false,
			cache: false,
			timeout: 800000,
			success: function (html) {
				$(".result"+id).html(html);  
			},
			error: function (e) {
				alert('Ошибка аякса-файл не запущен!"');
			}
		});
	});
	$('#PICTURE').change(function () {
		var input = $(this)[0];
		if (input.files && input.files[0]) {
			if (input.files[0].type.match('image.*')) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.fileLoad').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				console.log('ошибка, не изображение');
			}
		} else {
			console.log('хьюстон у нас проблема');
		}
	});
});
</script>	
