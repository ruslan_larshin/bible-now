<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по текстам Священного Писания");
?>
  <div id="map" style="width: 100%; height:500px"></div>

  <script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
  <script type="text/javascript">
      ymaps.ready(init);
      function init() {
          var myMap = new ymaps.Map("map", {
              center: [55.75985606898725,37.61054750000002],
              zoom: 12
          }, {
              searchControlProvider: 'yandex#search'
          });

          var myCollection = new ymaps.GeoObjectCollection();

          // Добавим метку красного цвета.
          var myPlacemark = new ymaps.Placemark([
              55.75985606898725,37.61054750000002
          ], {
              balloonContent: 'Тверская 9'
          }, {
              preset: 'islands#icon',
              iconColor: '#ff0000'
          });
          myCollection.add(myPlacemark);

          myMap.geoObjects.add(myCollection);
      }
  </script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>