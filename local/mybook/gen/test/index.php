<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<link rel="stylesheet" href="/local/style/main.css" />
<a href="/local/mybook/gen/test/result.doc">
    <div class="button">Скачать</div>
</a>
<?
$questions = \Test\Questions::getList(['books' => true])['questions'];
$level = 0;
$number = 1;
foreach($questions as $question){
    if($question['level'] > $level){
      $level = intval($question['level']);
      $number = 1;
      ?><div class="title2">Уровень сложности : <?=$question['level']?></div><?
    }
     ?><div class="title3"><?=$level?>.<?=$number?> <?=$question['name']?></div><?
      for($i = 1; $i < 5; $i++){
          ?>
          <div class="flex">
              <div class="number small"><?=$i?>. </div>
              <div class="verseText">
                <?=$question['variant' . $i]?>
              </div>
          </div>
        <?
      }
      ?>
        <br/>
    <div class="verseText">
      Правильные ответы : <?=implode(', ', $question['true'])?>
    </div>
    <?if(!empty($question['booksString'])){?>
        <div class="verseText">
          Вопрос по книгам: <?=$question['booksString']?>
        </div>
    <?}?>
    <br/>
  <?
  $number++;
}

$phpWord = new \PhpOffice\PhpWord\PhpWord();
$properties = $phpWord->getDocInfo();
$properties->setTitle('Вопросы по Библии');
$section = $phpWord->addSection();
$section -> addText(
  htmlspecialchars('Вопросы по Библии'),
  ['name' => 'Arial', 'size' => 18, 'color' => 'black', 'bold' => true, 'italic' => false],
  ['align' => 'center', 'spaceBefore' => 10]
);
$level = 0;
$number = 1;
foreach($questions as $question){
  if($question['level'] > $level){
    $level = intval($question['level']);
    $number = 1;
    $section->addText(
      htmlspecialchars('Уровень сложности : ' . $level),
      ['name' => 'Arial', 'size' => 14, 'color' => 'black', 'bold' => true, 'italic' => false],
      ['align' => 'left']
    );
  }
  $section->addText(
    htmlspecialchars($level . '.' . $number .' ' . $question['name']),
    ['name' => 'Arial', 'size' => 12, 'color' => 'black', 'bold' => true, 'italic' => false],
    ['align' => 'left', 'space' => ['line' => 0]]
  );
  for($i = 1; $i < 5; $i++){
    $section->addText(
      htmlspecialchars('   ' . $i . '. ' . $question['variant' . $i]),
      ['name' => 'Arial', 'size' => 10, 'color' => 'black', 'bold' => false, 'italic' => false],
      ['align' => 'left', 'space' => ['line' => 0, 'after' => 0]]
    );
  }

  $section->addText(
    htmlspecialchars(' Правильные ответы : ' . implode(', ', $question['true'])),
    ['name' => 'Arial', 'size' => 10, 'color' => 'black', 'bold' => false, 'italic' => false],
    ['align' => 'left', 'after' => 0, 'space' => ['line' => 0, 'before' => 20, 'after' => 10 ]]
  );

  $section->addText(
    htmlspecialchars(' Вопрос по книгам: ' . $question['booksString']),
    ['name' => 'Arial', 'size' => 10, 'color' => 'black', 'bold' => false, 'italic' => false],
    ['align' => 'left', 'space' => ['line' => 0, 'after' => 400]]
  );

  $number++;
}
$url = '/local/mybook/gen/test/result.doc';
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$result = $objWriter->save($_SERVER["DOCUMENT_ROOT"] . $url);
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>
