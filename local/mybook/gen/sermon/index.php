<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<link rel="stylesheet" href="/local/style/main.css" />
<a href="/local/mybook/gen/sermon/result.doc">
    <div class="button">Скачать</div>
</a>
<?
$sermons = \Learn\Sermons::getList()['items'];
foreach($sermons as $sermon){
  ?><div class="title2"><?=$sermon['name']?></div>
      <div class="flex">
        <div class="verseText">
          <?=$sermon['textHtml']?>
        </div>
      </div>
  <br/>
<?
}

$phpWord = new \PhpOffice\PhpWord\PhpWord();
$multipleTabsStyleName = 'multipleTab';
$properties = $phpWord->getDocInfo();
$properties->setTitle('Проповеди');
$section = $phpWord->addSection();
$section -> addText(
  htmlspecialchars('Проповеди'),
  ['name' => 'Arial', 'size' => 18, 'color' => 'black', 'bold' => true, 'italic' => false],
  ['align' => 'center', 'spaceBefore' => 10]
);
foreach($sermons as $sermon){
      $section->addText(
        htmlspecialchars($sermon['name']),
        ['name' => 'Arial', 'size' => 14, 'color' => 'black', 'bold' => true, 'italic' => false],
        ['align' => 'left', 'after' => 40 , 'space' => ['line' => 20, 'after' => 70]]
      );
      foreach($sermon['textArray'] as $text){
          if($text['type'] == 'verse') {
            $section->addText(
              htmlspecialchars('[ ' . $text['text'] . ' ]'),
              ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => true, 'italic' => true],
              ['align' => 'right', 'space' => ['line' => 20, 'after' => 30]]
            );
          }elseif($text['type'] == 'quote'){
            $section->addText(
              htmlspecialchars($text['text']),
              ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => false, 'italic' => false],
              ['align' => 'left', 'before' => 0, 'after' => 0, 'space' => ['line' => 0, 'after' => 20]]
            );
          }else{
            $section->addText(
              htmlspecialchars($text['text']),
              ['name' => 'Arial', 'size' => 10, 'color' => 'black', 'bold' => false, 'italic' => false],
              ['align' => 'left', 'before' => 0, 'after' => 0, 'space' => ['line' => 0, 'after' => 20]]
            );
          }
      }
  $section->addText(
    htmlspecialchars(''),
    ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => true, 'italic' => true],
    ['align' => 'right', 'space' => ['line' => 0, 'after' => 30]]
  );
}
$url = '/local/mybook/gen/sermon/result.doc';
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$result = $objWriter->save($_SERVER["DOCUMENT_ROOT"] . $url);
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>
