<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<link rel="stylesheet" href="/local/style/main.css" />
<a href="/local/mybook/gen/quotes/result.doc">
    <div class="button">Скачать</div>
</a>
<?
$quote = \Bible\Quote::getBibleQuotes()['books'];
foreach($quote as $bookKey => $book){
  ?><div class="title2"><?=$book['name']?></div><?
  foreach($book['quotes'] as $groupKey => $group){
    ?><div class="flex title3"><?=$bookKey?>.<?=($groupKey+1)?> <?=$group['name']?></div><?
    foreach($group['versesItems'] as $verses){
      foreach($verses['items'] as $item){
        ?>
          <div class="flex">
            <div class="number"><?=$item['chapter']?> : <?=$item['verseNumber']?></div>
            <div class="verseText">
              <?=$item['text']?>
            </div>
          </div>
        <?
      }
      ?>
      <div class="quote">
       [ <?=$verses['title']?> ]
      </div>
      <?
    }
  }
}
$phpWord = new \PhpOffice\PhpWord\PhpWord();
$multipleTabsStyleName = 'multipleTab';
$properties = $phpWord->getDocInfo();
$properties->setTitle('Краткая Библия цитатами');
$section = $phpWord->addSection();
$section -> addText(
  htmlspecialchars('Краткая Библия цитатами'),
  ['name' => 'Arial', 'size' => 18, 'color' => 'black', 'bold' => true, 'italic' => false],
  ['align' => 'center', 'spaceBefore' => 10]
);
foreach($quote as $bookKey => $book){
    if(!empty($book['quotes'])) {
      $section->addText(
        htmlspecialchars($book['name']),
        ['name' => 'Arial', 'size' => 14, 'color' => 'black', 'bold' => true, 'italic' => false],
        ['align' => 'left', 'after' => 40]
      );
      foreach ($book['quotes'] as $groupKey => $group) {
        $section->addText(
          htmlspecialchars($bookKey . '.' . ($groupKey + 1) . ' ' . $group['name']),
          ['name' => 'Arial', 'size' => 12, 'color' => 'black', 'bold' => true, 'italic' => false],
          ['align' => 'left', 'after' => 20]
        );

        foreach ($group['versesItems'] as $verses) {
          $text = '';
          foreach ($verses['items'] as $item) {
            $text .= $item['chapter'] . ' : ' . $item['verseNumber'] . '  ' . $item['text'] . "\r\n";
          }
          $section->addText(
            htmlspecialchars($text),
            ['name' => 'Arial', 'size' => 10, 'color' => 'black', 'bold' => false, 'italic' => false],
            ['align' => 'left', 'before' => 0, 'after' => 0, 'space' => ['line' => 10]]
          );
          $section->addText(
            htmlspecialchars('[ ' . $verses['title'] . ' ]'),
            ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => true, 'italic' => true],
            ['align' => 'right', 'spaceBefore' => 40, 'after' => 40]
          );
        }

      }
      $section->addPageBreak();
    }
}
$url = '/local/mybook/gen/quotes/result.doc';
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$result = $objWriter->save($_SERVER["DOCUMENT_ROOT"] . $url);
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>
