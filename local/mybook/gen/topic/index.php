<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<link rel="stylesheet" href="/local/style/main.css" />
<a href="/local/mybook/gen/topic/result.doc">
    <div class="button">Скачать</div>
</a>
<?
$topics = \Bible\Topics::getList()['items'];
foreach($topics as $topic){
  ?><div class="title2"><?=$topic['name']?></div><?
  foreach($topic['verses'] as $groupKey => $group){
    foreach($group['verses'] as $verses){
        ?>
          <div class="flex">
            <div class="number"><?=$verses['chapter']?> : <?=$verses['verseNumber']?></div>
            <div class="verseText">
              <?=$verses['text']?>
            </div>
          </div>
        <?

    }
    ?>
      <div class="quote">
          [ <?=$group['path']?> ]
      </div>
    <?
  }
}

$phpWord = new \PhpOffice\PhpWord\PhpWord();
$multipleTabsStyleName = 'multipleTab';
$properties = $phpWord->getDocInfo();
$properties->setTitle('Тематические подборки');
$section = $phpWord->addSection();
$section -> addText(
  htmlspecialchars('Тематические подборки'),
  ['name' => 'Arial', 'size' => 18, 'color' => 'black', 'bold' => true, 'italic' => false],
  ['align' => 'center', 'spaceBefore' => 10]
);
foreach($topics  as $topic){
      $section->addText(
        htmlspecialchars($topic['name']),
        ['name' => 'Arial', 'size' => 14, 'color' => 'black', 'bold' => true, 'italic' => false],
        ['align' => 'left', 'after' => 40]
      );
        foreach($topic['verses'] as $groupKey => $group){
          foreach($group['verses'] as $item){
              $section->addText(
                htmlspecialchars($item['verseNumber'] . '. ' . $item['text']),
                ['name' => 'Arial', 'size' => 10, 'color' => 'black', 'bold' => false, 'italic' => false],
                ['align' => 'left', 'before' => 0, 'after' => 0, 'space' => ['line' => 0, 'after' => 0]]
              );
        }
          $section->addText(
            htmlspecialchars('[ ' . $group['path'] . ' ]'),
            ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => true, 'italic' => true],
            ['align' => 'right', 'space' => ['line' => 20, 'after' => 30]]
          );
          $section->addText(
            htmlspecialchars(''),
            ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => true, 'italic' => true],
            ['align' => 'right', 'space' => ['line' => 0, 'after' => 10]]
          );

      }
  $section->addText(
    htmlspecialchars(''),
    ['name' => 'Arial', 'size' => 10, 'color' => 'blue', 'bold' => true, 'italic' => true],
    ['align' => 'right', 'space' => ['line' => 0, 'after' => 30]]
  );
}
$url = '/local/mybook/gen/topic/result.doc';
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$result = $objWriter->save($_SERVER["DOCUMENT_ROOT"] . $url);
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>
