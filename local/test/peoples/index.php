<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$bigAlphabet = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р',
  'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'];
$explode = ['Я', "Он", "Свои", "Своих", "Нашему", "Своему", "Твой", "Ты", "Жена", "Нас", "Мне", "Мое", "Мой", "Мною", "Мою", "Моя", "Она"
  , "В", "Моего", "Тебя", "Ним", "Твоя", "И", "Себе", "Меня", "Своего", "Твоему", "Своею", "Раб", "Раб", "Раб", "Раб"
  , "Мои", "Они", "Тебе", "Некто", "Моем", "Но", "Худо", "Нет", "Ему", "Свой", "Что", "Его"
  , "Моим", "Моих", "Мною", "Твоей", "Мне", "Кто", "Твою", "Твоею", "Твои", "Моих", "Твои", "Своим", "Свое"
  , "Себя", "Твоего", "Твоим", "Твоими", "Твое", "Которым", "Твои", "Твои", "Твои", "Твои", "Твои", "Твои", "Твои"];
$verses = \Bible\Verses::getList(
  [
    //'nav' => ['limit' => 10000],
    'cacheTime' => \Main\Cache::cacheTime
  ]
)['verses'];
$words = [];
$books = \Bible\Books::getListIblock(['key' => 'PROPERTY_CODE_VALUE', 'select' => ['code', 'id', 'shortName']])['books'];
foreach($verses as $verse){
  $verse['text'] = str_replace(['!',';','?'], '.',$verse['text']);
  $text = array_filter(str_replace([':', '[', ']', ',', ';', '"'], '', explode('.', $verse['text'] )));
  foreach($text as $item){
    $buf = explode(' ', trim($item));
    unset($buf[0]);
    foreach($buf as $value){
      if(in_array(mb_substr($value, 0, 1) ?? '1', $bigAlphabet ) && !in_array($value, $explode) ?? (mb_strlen($value) > 1)){
        if(empty($words[$value])) {
          $words[$value] = [
            'books' => [$books[$verse['book']]['id']],
            'path' => [
              "{$books[$verse['book']]['shortName']} {$verse['chapter']} : {$verse['verseNumber']}",
            ],
          ];
        }else{
          $words[$value]['books'][] =  $books[$verse['book']]['id'];
          $words[$value]['path'][] =  "{$books[$verse['book']]['shortName']} {$verse['chapter']} : {$verse['verseNumber']}";
        }
      }
    }
  }
}
foreach($words as $key => $word){
  $words[$key]['books'] = array_unique($word['books']);
  $words[$key]['path'] = array_unique($word['path']);
}

foreach($words as $name => $word){
  $str = mb_substr($name, 0, -1);
  if(!empty($words[$str])){
    $words[$name]['parent'] = $str;
  }else{
    $str = mb_substr($name, 0, -2);
    if(!empty($words[$str])){
      $words[$name]['parent'] = $str;
    }
  }

}
/*
foreach($words as $name => $word){
  $word['name'] = $name;
  $word['section'] = 4;
  $newItem = \Bible\Peoples::add($word);
 // view($word);
  view($newItem);
  //die();
}*/
$peoples = \Bible\Peoples::getList(
  [
    'select' => ['id', 'name', 'parent'],
    'key' => 'NAME',
    'cacheTime' => 3600,
  ]
)['peoples'];
foreach($words as $name => $word){
  if(!empty($word['parent'])){
    $item = \Bible\Peoples::update($peoples[$name]['id'], ['parent' => $peoples[$word['parent']]['id']]);
    view($item);
  }
}
//view($words);
?>