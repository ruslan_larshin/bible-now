<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$peoples = \Bible\Peoples::getList(['select' => ['id', 'name', 'parent']])['peoples'];
view($peoples);
?>