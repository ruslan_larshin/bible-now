<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");?>

<div class="filterTest">
    <div class="DesctopFlex">
        <div class="groupLevel groupFilter ">
            <div>Выберите&nbsp;уровень&nbsp;сложности (можно&nbsp;несколько)</div>
            <br/>
            <?for($i = 1; $i <5 ;$i++){?>
                <div class="Flex">
                    <div class="checkbox" data-level="<?=$i?>"></div><div class="unselectable" data-level="<?=$i?>"> <?=$i?> уровень сложности</div>
                </div>
            <?}?>
        </div>
        <div class="groupBook groupFilter ">
            <?$viewTestObject -> selectListMulty($listBookSelect , 'По книгам')?>
            <br/>
            <div class="Flex">
                <div class="checkbox oneAnswer" ></div><div class="unselectable" data-level="<?=$i?>"> Только с одним ответом</div>
            </div>
            <div class="Flex">
                <div class="checkbox stopError" ></div><div class="unselectable" > Останавливать при ошибке</div>
            </div>
        </div>
        <div class="groupButton groupFilter center ">
            <div class="button startGame">Начать игру</div>
        </div>
    </div>

    <div class="gameMain">

    </div>
</div>
