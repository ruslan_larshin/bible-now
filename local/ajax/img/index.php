<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/img/svg/index.php');
$key = $_REQUEST['key'];
?>
<?if(!empty($_REQUEST['src'])){?>
    <div class="imgPopup flexCenter">
        <img src="<?=$_REQUEST['src']?>" />
    </div>
<?}else{?>
    <?$imgs = explode('-',$_REQUEST['imgs']);?>
    <div class="imgPopup flexCenter">
        <?if($key > 0){?>
            <div class="popupOpenButtonData"
                 data-url="/local/ajax/img/"
                 data-imgs="<?=$_REQUEST['imgs']?>"
                 data-key="<?=($key - 1)?>"
            ><?print_r($GLOBALS['svg']['rowLeft']);?></div>
        <?}?>
        <img src="<?=\CFile::GetPath($imgs[$_REQUEST['key']])?>" />
      <?if($key < (count($imgs ?? []) - 1)){?>
          <div class="popupOpenButtonData"
               data-url="/local/ajax/img/"
               data-imgs="<?=$_REQUEST['imgs']?>"
               data-key="<?=($key + 1)?>"
          ><?print_r($GLOBALS['svg']['rowRight']);?></div>
      <?}?>
    </div>
<?}?>
