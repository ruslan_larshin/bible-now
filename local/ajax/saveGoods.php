<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Hgload/Hgload.php');
$arGoods = new \Hgload\Hgload(); 
use Bitrix\Main\Page\Asset;
use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;	
	
Loader::includeModule("highloadblock"); 

$arRequest = $arGoods->getRequest();
try
{
	// Массив полей для добавления
	if($arRequest['UF_DATE'])
	{
		$Date = explode('-',$arRequest['UF_DATE']);
		$Date = $Date[2].'.'.$Date[1].'.'.$Date[0];
	}
	$data = array(
		'UF_NAME'=>$arRequest['UF_NAME'] ?? 'Имя не задано', 
		'UF_PICTURE'=>$_FILES['UF_PICTURE'],
		'UF_PRICE'=>$arRequest['UF_PRICE'] ?? '0',
		'UF_DATE'=>$Date ?? '21.21.2021',
	);

	$result = $arGoods->add($data);
	if(!$result->getid()){
		$errors = $result->	getErrorMessages();
		$errors[]=$errors[0];
		foreach($errors as $error){
			echo '<pre>'; print_r('Сохранение не прошло : ' . $error); echo '</pre>';
			
		}
		throw new SystemException($error); 
	}else{
		echo '<pre>'; print_r('Сохранение  прошло успешно : id = ' . $result->getid()); echo '</pre>';
	}
}
catch (SystemException $exception)
{
    echo $exception->getMessage();
}

?>