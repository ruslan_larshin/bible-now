<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");?>
<?//получаем строку поиска с парам етрами- отдаем результат поиска
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bible.php');
$arBible = new \Larshin\Bible\Bible();
$searchObject = new \Larshin\Search\Symphony();
$arRequest = $arBible ->getRequest();
$arSymphony = $searchObject->symphonyMainList($_REQUEST["search"]);
?>
<div class='alphabetList Flex'> 
	<?foreach(alphabet as $letter){?>
		<div class='letter'><a href ='/symphony/?search=<?=$letter?>'><?=$letter?></a></div>
	<?}?>
</div>

<div class='Flex'>
	<?if($arSymphony){?>
		<?$view -> viewSymphonyList($arSymphony);?>
	<?}else{?>
		<?$view -> noResult();?>
	<?}?>
</div>
