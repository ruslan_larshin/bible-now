<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$request = $_REQUEST;
global $APPLICATION;
 $APPLICATION->IncludeComponent(
    $request['component'],
    $request['template'],
    $request['params'] ?? [],
); ?>

