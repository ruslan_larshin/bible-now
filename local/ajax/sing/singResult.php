<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
$sings = \Learn\Sing::getList(
  [
    'filter' => !empty($request['search']) ? ['NAME' => '%' . $request['search'] . '%'] : [],
  ]
);
view($sings);
?>
<?foreach($sings as $sing){?>
  <div class =''>
    <div><a class ='blue title2 font noDecoration black' href = '/learn/sermon/<?=$sermon -> code?>/' ><?=$sermon -> name?></a></div>
    <div class='border margin'>
      <div  class='cutText '>
        <?=$sing['name']?>
      </div>
    </div>
  </div>
<?}?>