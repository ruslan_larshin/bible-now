<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
$api = new \Larshin\MainTools\api();
$arRequest = $api ->getRequest();	
$arBibles = new \Larshin\Bibles\Bibles();
$arVerse = new \Larshin\Bibles\Verse();
$buf=explode(PHP_EOL,$_REQUEST['text']);

$phpWord = new  \PhpOffice\PhpWord\PhpWord(); 
$phpWord->setDefaultFontName('Times New Roman');
$phpWord->setDefaultFontSize(10) ;
$properties = $phpWord->getDocInfo(); 

$sectionStyle = array(
	//'orientation' => 'landscape',
	'marginTop' => \PhpOffice\PhpWord\Shared\Converter::pixelToTwip(10),
	'marginLeft' => 600,
	'marginRight' => 600,
	'colsNum' => 1,
	'pageNumberingStart' => 1,
	'borderBottomSize'=>100,
	'borderBottomColor'=>'C0C0C0'
);

$section = $phpWord->addSection($sectionStyle); 
$TextRun = $section->addTextRun();

$textWord = '';
$i = 1;
foreach($buf as $text)
{
	$result = $arVerse  -> getVerseByString($text);
	$buf = $arVerse -> getVerseFromStringArray($result);
	
		foreach($buf as $verse)
		{
			$book = $verse ->full;
			$short = $verse ->short;
			$chapter = $verse ->chapter;
		}
		
		$textWord = $textWord . $book . '\n';
		$res[] = array(
			'book' => $book,
			'short' => $short,
			'chapter' => $chapter,
			'path' => $text,
			'text' => $buf,
		);
	if($buf)
	{	
		$TextRun->addText( $i . '. ' .  $verse -> full . ' ' , array('color'=>'black' , 'size' => 12, 'bold' => true ,'spaceAfter'=> 10));
		$TextRun->addTextBreak(2);
		$i++;
		foreach($buf as $verse)
		{
			$TextRun->addText( $verse -> short . ' '. $verse -> chapter . ' : ' . $verse -> number . ' ' , array('color'=>'blue'));
			$TextRun->addText($verse -> text . PHP_EOL);
			$TextRun->addTextBreak();
		}
		
		$TextRun->addText('                                                                                                                                                      (' . $text . ')', array('color'=>'blue') ,array('align'=>'right','spaceBefore'=>10));
		$TextRun->addTextBreak(2);
	}
		
		/*$fontStyle = array('name'=>'Arial', 'size'=>8, 'color'=>'blue', 'bold'=>false, 'italic'=>false);
		$parStyle = array('align'=>'right','spaceBefore'=>0);
		$section->addText(htmlspecialchars(' (' . $text . ')'), $fontStyle,$parStyle);*/
	
	$textWord = $textWord . $text . '\n';
}



$properties->setCreator('Руслан Ларшин');
$properties->setCompany('Bible-live.ru');
$properties->setTitle('Список цитат для проповеди');
$properties->setDescription('Список цитат для проповеди');
$properties->setCategory('My category');
$properties->setLastModifiedBy('My name');
$properties->setCreated(mktime(0, 0, 0, 3, 12, 2022));
$properties->setModified(mktime(0, 0, 0, 3, 14, 2022));
$properties->setSubject('Цитаты Библии');
$properties->setKeywords('бибиля, цитаты'); 



//$text = "PHPWord is a library written in pure PHP that provides a set of classes to write to and read from different document file formats.";

$namneObject = $_REQUEST['name'] ?? 'docs';
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
$objWriter->save($namneObject .'.docx');
$i =1;
?>
<div class='viewVerses'>
	<?foreach($res as $item){
		if($item['text'])
		{
			?><div class='title3 bold '><?=$i . '. ' .$item['book']?></div>
			<?$view -> viewVerseBlock($item); $i++;?>
			<div class='blue bold right'>(<?=$item['path']?>)</div>
		<?}else{?>
			<br/>
			<div class='red'> Внимание! не найден результат! <span class='blue bold'>(<?=$item['path']?>)</span></div>
			<br/>
		<?}?>
	<?}?>
	<a href='/local/ajax/tools/<?=$namneObject?>.docx'>Скачать word файл</a>
</div>
