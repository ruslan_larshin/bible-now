<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
if((empty($_REQUEST['book']) || empty($_REQUEST['chapter'])) && empty($_REQUEST['path'])){
  die('Не указаны обязательные параметры');
}
?>
<?
global $APPLICATION;
$APPLICATION->IncludeComponent(
  'bible:chapter',
  'full',
  $arParams = [
    'path' => $_REQUEST['path'],
    'full' => true,
  ],
); ?>