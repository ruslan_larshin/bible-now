<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
	
$rubricObject = new \Larshin\Rubric\Rubric();
$searchObject = new \Larshin\Search\Search();
if($_REQUEST['search'])
	$arRequset['search'] = $searchObject -> updateText($_REQUEST['search']);
$rubric = $rubricObject -> getList(array('search' => $arRequset['search']));
$rubric4 = $rubricObject -> deleteArrayFromTable($rubric , 4);
$rubric3 = $rubricObject -> deleteArrayFromTable($rubric , 3);
$rubric2 = $rubricObject -> deleteArrayFromTable($rubric , 2);
$rubric1 = $rubricObject -> deleteArrayFromTable($rubric , 1);
?>
<?if($rubric){?>
	<div class='searchResult active' data-search='search=<?=$_REQUEST["search"]?>'>
		<div class='Flex'>

			<div class = 'rubric4 Flex width100 noDisplay'>
				<?foreach($rubric4 as $i => $rubric){?>
					<div class='width24 paddingRight'>
						<?foreach($rubric as $letter => $item){?>
							<div class='letterBlockA'><?=$letter?></div>
							<?foreach($item as $rubric){?>
								<div><a class ='black noDecoration' href = '/learn/rubric/<?=$rubric -> code?>/'><?=$rubric -> name?></a></div>
							<?}?>
							<br/>
						<?}?>
					</div>
				<?}?>
			</div>
			<div class = 'rubric3 Flex width100 noDisplay'>
				<?foreach($rubric3 as $i => $rubric){?>
					<div class='width30 paddingRight'>
						<?foreach($rubric as $letter => $item){?>
							<div><?=$letter?></div>
							<?foreach($item as $rubric){?>
								<div><a class ='black noDecoration' href = '/learn/rubric/<?=$rubric -> code?>/'><?=$rubric -> name?></a></div>
							<?}?>
							<br/>
						<?}?>
					</div>
				<?}?>
			</div>
			<div class = 'rubric2 Flex width100 noDisplay'>
				<?foreach($rubric2 as $i => $rubric){?>
					<div class='width50 paddingRight'>
						<?foreach($rubric as $letter => $item){?>
							<div><?=$letter?></div>
							<?foreach($item as $rubric){?>
								<div><a class ='black noDecoration' href = '/learn/rubric/<?=$rubric -> code?>/'><?=$rubric -> name?></a></div>
							<?}?>
							<br/>
						<?}?>
					</div>
				<?}?>
			</div>
			<div class = 'rubric1 Flex width100 noDisplay'>
				<?foreach($rubric1 as $i => $rubric){?>
					<div class='width100 paddingRight'>
						<?foreach($rubric as $letter => $item){?>
							<div><?=$letter?></div>
							<?foreach($item as $rubric){?>
								<div><a class ='black noDecoration' href = '/learn/rubric/<?=$rubric -> code?>/'><?=$rubric -> name?></a></div>
							<?}?>
							<br/>
						<?}?>
					</div>
				<?}?>
			</div>
		</div>
	</div>
<?}else{
	$view -> noResult();
}?>