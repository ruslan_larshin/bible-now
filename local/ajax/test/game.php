<?require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
$api = new \Larshin\MainTools\api();
$arRequest = $api ->getRequest();
$testObject = new \Larshin\Test\Test();
$testView = new \Larshin\View\Test();
$Params = [
            'page'=> $_REQUEST['page'] ?? 1,
            'topCount' => $_REQUEST['topCount'] ?? 10,
         ];
if($_REQUEST['book']){
    $book = explode(',' , $_REQUEST['book']);
    foreach($book as $item){
        if($item && $item != '1')
            $Params['filter']['PROPERTY_BOOK'][] = $item;
    }
}
if($_REQUEST['level']){
    $level = explode(',' , $_REQUEST['level']);
    foreach($level as $item){
        if($item && $item != '0')
            $Params['filter']['PROPERTY_LEVEL'][] = $item;
    }
}
if($_REQUEST['noViewAnswer']){
    $param['noViewAnswer'] = true;
}
if($_REQUEST['viewBook']){
    $param['viewBook'] = true;
}
if($_REQUEST['oneAnswer']){
    $Params['filter']['PROPERTY_ANSWER'] = ['1/0/0/0' , '0/1/0/0' ,'0/0/1/0' ,'0/0/0/1' ,];
}
unset($_REQUEST['page']);
$testlist = $testObject -> getListByGame($Params);
?>
<?if($testlist['questions']){?>
    <?foreach($testlist['questions'] as $key => $item){?>
        <?$testView -> viewOneQuestion($item , intval(($Params['page']-1) * $Params['topCount'])  + intval($key) + 1 , $param); ?>
    <?}?>
<?}else{?>
    По данному фильтру нет вопросов!
<?}?>
<?php if(intval($testlist['pager']['page']) < intval(intval($testlist['pager']['pageCount']))){?>
    <div class="moreQuestion button button<?=intval($testlist['pager']['page'])?>" data-filter="<?=http_build_query($_REQUEST)?>" data-page = '<?=(intval($testlist['pager']['page']) + 1)?>'>Показать еще</div>
<?}?>
