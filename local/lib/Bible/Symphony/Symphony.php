<?

namespace Bible;

\CModule::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Symphony extends \Main\HighLoad
{
  public const hlId = '242';

  public static function getMap()
  {
    return [
      'id' => 'ID',
      'name' => 'UF_NAME',
      'verseIds' => 'UF_ID',
      'lexem' => 'UF_LEXEM',
      'lexemIds' => 'UF_LEXEM_IDS',
      'mainWord' => 'UF_MAIN_WORD',
      'dictionaryWords' => 'UF_DICTIONARY_WORDS',
      'dictionaryIds' => 'UF_DICTIONARY_IDS',
    ];
  }

  public static function getByName($word){
    if(empty($word)){
      return ['error' => 'Введите слово'];
    }
    $result = self::getList(
      [
        'filter' => ['UF_NAME' => $word],
        'cache' => ['ttl' => \Main\Settings::cashTimeD7],
      ],
    )['symphony'] ?? [];
    return ['symphony' => array_shift($result)];
  }


  public static function getList($request = [], $light = true)
  {
    try {
      $items = self::getListEmpty(self::hlId, self::getMap(), $request)['items'];
      foreach($items as $symphonyId => $value){
        $items[$symphonyId]['verseIds'] = array_filter(explode('-', $value['verseIds']));
        $items[$symphonyId]['lexemIds'] = array_filter(explode('-', $value['lexemIds']));
        $items[$symphonyId]['dictionaryIds'] = array_filter(explode('-', $value['dictionaryIds']));
        $verseIds = array_merge($verseIds ?? [], $items[$symphonyId]['verseIds'] ?? []);
      }
      if(!$light){
        $verses = \Bible\Verses::getList(['filter' => ['ID' => $verseIds ?? [0]]])['verses'];
        foreach($items as $symphonyId => $value){
          foreach($value['verseIds'] as $verseId){
            $items[$symphonyId]['verses'][$verseId] = $verses[$verseId];
          }
        }
      }
      return ['symphony' => $items];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function update($elId, $data)
  {
    try {
     $update = self::updateEmpty($elId, self::hlId, self::getMap(), $data);
     return $update;
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

}