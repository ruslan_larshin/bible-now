<?

namespace Bible;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Cities extends \Main\Iblock
{
  public const iblockCode = 'cities';

  public static function getMap()
  {
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'section' => 'IBLOCK_SECTION_ID',
      'paths' => 'PROPERTY_PATHS',
      'verseIds' => 'PROPERTY_VERSE_IDS',
      'city' => 'PROPERTY_CITY',
      'books' => 'PROPERTY_BOOKS',
      'symphonyIds' => 'PROPERTY_SYMPHONY_IDS',
      'parent' => 'PROPERTY_PARENT',
      'map' => 'PROPERTY_MAP',
    ];
  }

  public static function getList($request = [])
  {
    try {
      $result = self::getListEmpty(self::iblockCode, self::getMap(), $request)['items'];
      return ['cities' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function add($request)
  {
    return self::addEmpty(self::iblockCode, self::getMap(), $request, true);
  }

  public static function update($peopleId, $request)
  {
    return self::updateEmpty($peopleId, self::iblockCode, self::getMap(), $request);
  }
}