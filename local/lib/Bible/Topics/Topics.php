<?
namespace Bible;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Topics extends \Main\Iblock
{
  public const iblockCode = 'rubric';
  public static function getMap(){
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'verses' => 'PROPERTY_VERSES_VIEW',
    ];
  }

  public static function getList($request = [], $light = false){
    try {
      $request['sort'] = ['NAME' => 'ASC'];
      $result = self::getListEmpty(self::iblockCode, self::getMap(), $request)['items'];
      if(!$light){
        foreach($result as $key => $item){
          $buf = [];
          foreach($item['verses'] as $verses){
            $buf[] = [
              'verses' => \Bible\Verses::getVersesByPath($verses)['verses'],
              'path' => $verses,
            ];
          }
          $result[$key]['verses'] = $buf;
        }
      }
      return ['items' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }
}
