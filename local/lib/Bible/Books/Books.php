<?
namespace Bible;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Books extends \Main\Iblock
{
  public const ibCode = 'bibleBooks';
  public static function getMap(){
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'fullName' => 'PROPERTY_FULL_NAME',
      'shortName' => 'PROPERTY_SHORT_NAME',
      'chapterCount' => 'PROPERTY_CHAPTER_COUNT',
      'verseCount' => 'PROPERTY_VERSE_COUNT',
      'code' => 'PROPERTY_CODE',
      'zavet' =>'PROPERTY_ZAVET',
      'number' =>'PROPERTY_NUMBER',
    ];
  }

  public static function add($request)
  {
    try {
      $el = new \CIBlockElement;
      $arLoadProductArray = [
        "IBLOCK_SECTION_ID" => ($request['zavet'] == 'new') ? 2 : 1,
        "IBLOCK_ID" => self::getIdByCode(self::ibCode)['id'],
        "PROPERTY_VALUES" => [
          'FULL_NAME' => $request['fullName'],
          'SHORT_NAME' => $request['shortName'],
          'CHAPTER_COUNT' => $request['chapterCount'],
          'VERSE_COUNT' => $request['verseCount'],
          'NUMBER' => $request['number'],
          'CODE' => $request['code'],
          'ZAVET' => ($request['zavet'] == 'new') ? 3 : 4,
        ],
        "NAME" => $request['name'],
        "CODE" => $request['code'],
        "SORT" => $request['sort'],
        "ACTIVE" => "Y",
      ];
      if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
        return ['success' => true, 'id' => $PRODUCT_ID, 'data' => $arLoadProductArray, 'request' => $request];
      }
      else {
        return ['error' => $el->LAST_ERROR];
      }
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getListIblock($request = []){
    try {
      $result = self::getListEmpty(self::ibCode, self::getMap(), $request)['items'];
      return ['books' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getList($request = []){
    try {
      $select = ['ID', 'NAME', 'CODE', 'FULL_NAME_' => 'FULL_NAME', 'SHORT_NAME_' => 'SHORT_NAME',
        'CHAPTER_COUNT_' => 'CHAPTER_COUNT', 'VERSE_COUNT_' => 'VERSE_COUNT',
        'CODE_' => 'CODE', 'ZAVET_' => 'ZAVET', 'NUMBER_' => 'NUMBER', 'SORT', 'IBLOCK_SECTION_ID'];
      $filter = $request['filter'] ?? [];
      $books = \Bitrix\Iblock\Elements\ElementBibleBooksTable::getList([
        'select' => $select,
        'order' => ['NUMBER_VALUE' => 'ASC'],
        'filter' => $filter,
        'cache' => ['ttl' => \Main\Settings::cashTimeD7, 'cache_joins' => true,],
      ])->fetchAll();
      foreach ($books as $book) {
        $key = (!empty($request['idKeys'])) ? $book[$request['idKeys']] : intval($book['ID']);
        $group = $book['IBLOCK_SECTION_ID'] == 2 ? 'new' : 'old';
        $result[$key] = [
          'id' => $book['ID'],
          'code' => $book['CODE'],
          'sort' => $book['SORT'],
          'name' => $book['NAME'],
          'fullName' => $book['FULL_NAME_VALUE'],
          'shortName' => $book['SHORT_NAME_VALUE'],
          'chapterCount' => intval($book['CHAPTER_COUNT_VALUE']),
          'verseCount' => intval($book['VERSE_COUNT_VALUE']),
          'zavet' => $group,
          'number' => intval($book['NUMBER_VALUE']),
        ];
      }

      return ['books' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getBook($bookName){
    try{
      if(empty($bookName)){
        return ['error' => 'Невалидное имя книги'];
      }
      $book = \Bible\Books::getListIblock(
        [
          'filter' => [
            [
              'LOGIC' => 'OR',
              ['PROPERTY_FULL_NAME' => $bookName],
              ['PROPERTY_SHORT_NAME' => $bookName],
              ['PROPERTY_CODE' => $bookName],
              ['NAME' => $bookName],
            ],
            'cacheTime' => \Main\Settings::cashTimeD7,
          ]
        ]
      )['books'][0];
     return ['book' => $book ];
    }catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

}
