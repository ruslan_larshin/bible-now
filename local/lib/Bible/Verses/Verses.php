<?

namespace Bible;

\CModule::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Verses extends \Main\HighLoad
{
  public const hlId = '241';

  public static function getMap()
  {
    return [
      'id' => 'ID',
      'text' => 'UF_TEXT',
      'chapter' => 'UF_CHAPTER',
      'verseNumber' => 'UF_VERSE',
      'book' => 'UF_CODE_BOOK',
    ];
  }

  public static function getByIds($ids){
    if(empty($ids)){
      return ['error' => 'введите массив идентификаторов'];
    }
    $result = self::getList(
      [
        'filter' => ['ID' => $ids],
        'cache' => ['ttl' => \Main\Settings::cashTimeD7],
      ],
    )['verses'];
    return ['verses' => $result];
  }

  public static function getVerses($book, $chapter, $verseNumbers = [])
  {
    try {
      $request['cacheTime'] = \Main\Settings::cashTimeD7;
      $request['filter'] = [
        'UF_CHAPTER' => intval($chapter) ?: 0,
        [
          'LOGIC' => 'OR',
          ['UF_CODE_BOOK' => $book],
          ['UF_SHORT' => $book],
          ['UF_FULL' => $book],
          ['UF_BOOK' => $book],
        ]
      ];
      if(!empty($verseNumbers)){
        $request['filter']['UF_VERSE'] = $verseNumbers;
      }
      $result = self::getList($request);
      return $result;
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getList($request = [])
  {
    try {
      $items = self::getListEmpty(self::hlId, self::getMap(), $request)['items'];
      foreach($items as $key => $item){
        $items[$key]['text'] = str_replace(PHP_EOL, '', trim($item['text']));
        $items[$key]['verseNumber'] = intval($item['verseNumber']);
        $items[$key]['chapter'] = intval($item['chapter']);
      }
      return ['verses' => $items];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function makeFilterByQuote($quote)
  {
    $quote = str_replace('–', '-',$quote);
    if (empty($quote)) {
      return ['error' => 'Невалидная строка'];
    }
    $verseString = explode(' ', $quote);
    $result = [
      'book' => $verseString[0],
      'chapter' => $verseString[1],
      'verses' => [],
    ];
    if($result['chapter'] != intval($result['chapter'])){
      return ['error' => 'Невалидная глава ' . $result['chapter']];
    }
    $quote = str_replace(' ', '', trim($quote));
    $verseStringTrim = str_replace([$verseString[0], $verseString[1] . ':'], '', $quote);
    $buf = explode(',', $verseStringTrim);

    foreach ($buf as $verseNumbers) {

      if (strpos($verseNumbers, '-') == false) {
        $result['verses'][] = intval($verseNumbers);
      }
      else {

        $diapazonVerses = explode('-', $verseNumbers);
        for ($k = $diapazonVerses[0]; $k <= $diapazonVerses[1]; $k++) {
          $result['verses'][] = intval($k);
        }
      }
    }
    return ['verses' => $result];
  }

  public static function getVersesByPath($path, $fullChapter = false)
  {
    try {
      if (empty($path)) {
        return ['error' => 'Невалидная строка'];
      }
      $verses =  \Bible\Verses::makeFilterByQuote($path)['verses'];
      if(!empty($verses['book']) && !empty($verses['chapter']) && !empty($verses['verses'])) {
        $versesItems = \Bible\Verses::getVerses($verses['book'], $verses['chapter'], !$fullChapter ? $verses['verses'] : [])['verses'];
        if(!empty($fullChapter)){
          foreach($versesItems as $id => $verse){
            if(in_array($verse['verseNumber'] ?? 0, $verses['verses'] ?? [])){
              $versesItems[$id]['bold'] = 'true';
            }
          }
        }

      }else{
        return ['error' => 'невалидные данные', 'pathError' => $path];
      }
      return ['verses' => $versesItems];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }

  }
}