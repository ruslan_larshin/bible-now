<?
namespace Yandex;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Speller
{
  public const spellerYandex = 'https://speller.yandex.net/services/spellservice.json/checkText';

  public static function getResult($word){
    if(empty($word)){
      return ['error' => 'Пустые данные'];
    }
    $result = \Main\Curl::getCurlJson(self::spellerYandex, 'GET', ['text' => $word])['result'][0];
    $type = empty($result['s']) ? 'simple' : 'vars';
    $result = $result['s'] ?? [$result];
    return ['result' => $result, 'type' => $type];
  }

}