<?
namespace Yandex;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Dictionary
{
  public const key = 'dict.1.1.20230627T134126Z.b50c1508b5906871.e69e561e0853ffa8a5aa52d8e1031a9fd6d0f008';
  public const dictionaryYandex = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup';

  public static function getResult($word){
    if(empty($word)){
      return ['error' => 'Пустые данные'];
    }
    $params = [
      'text' => $word,
      'lang' => 'ru-ru',
      'key' => self::key,
    ];
    $result = \Main\Curl::getCurlJson(self::dictionaryYandex, 'GET', $params)['result'];
    $words = [];
    $words[$word] = $word;
    foreach($result['def'] as $item){
      $words[$item['text']] = $item['text'];
      foreach($item['syn'] as $syn){
        $words[$syn['text']] = $syn['text'];
      }
      foreach($item['tr'] as $itemSyn){
        $words[$itemSyn['text']] = $itemSyn['text'];
        foreach($itemSyn['syn'] as $syn){
          $words[$syn['text']] = $syn['text'];
        }
      }
    }
    $words = array_values($words);
    $words = array_filter($words);
    return ['words' => $words];
  }

}