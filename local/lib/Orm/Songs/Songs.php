<?php
namespace Orm;

class SongsTable extends \Main\Orm
{
  public static function getTableName()
  {
    return 'songs4';
  }

  public static function getMap()
  {
    return [
      new \Bitrix\Main\Entity\IntegerField('ID', [
        'primary' => true,
        'autocomplete' => true
      ]),
      new \Bitrix\Main\Entity\StringField('NAME'),
      new \Bitrix\Main\Entity\StringField('TITLE'),
      new \Bitrix\Main\Entity\StringField('PUSH'),
      new \Bitrix\Main\Entity\DateField('PUBLISH_DATE',[
        'default_value' => new \Bitrix\Main\Type\Date //значение по умолчанию
      ]),
    ];
  }
}