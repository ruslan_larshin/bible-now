<?

namespace Learn;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Sing extends \Main\Iblock
{
  public const iblockCode = 'sing';

  public static function getMap()
  {
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'path' => 'PROPERTY_PATH',
      'text' => 'PROPERTY_TEXT',
    ];
  }

  public static function getList($request = [])
  {
    try {
      $result = self::getListEmpty(self::iblockCode, self::getMap(), $request)['items'];
      return ['items' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function add($request)
  {
    return self::addEmpty(self::iblockCode, self::getMap(), $request);
  }
}