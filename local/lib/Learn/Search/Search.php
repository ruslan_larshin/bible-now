<?
namespace Learn;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Search extends \Bible\Symphony
{
  public static function search($text, $long = false){
    $text = \Main\Text::cleanText($text)['text'];
    $result = [];
    $resultWords = [];
    $words = explode(' ', $text) ?? [];
    foreach($words as $word){
      if(mb_strlen($word) < 2){
        continue;
      }
      if(!empty($long)) {
        $word = self::getMorfs([$word])['words'];
      }
      $symphony = self::getByName($word)['symphony'];
      if(empty($symphony)){
        $symphony = self::getByName($word . '%')['symphony'];
      }

      if(empty($symphony)){
        $wordsDictionary = self::getByWord($word)['words'];
        $symphony['verseIds'] = [];
        foreach($wordsDictionary as $item){
          $symphonyDictionary = self::getByName($item)['symphony'];
          $resultWords[] = $item;
          $symphony['verseIds'] = array_merge($symphony['verseIds'] ?? [], $symphonyDictionary['verseIds'] ?? []);
        }
        $symphony['verseIds'] = array_unique($symphony['verseIds']);
      }else{
        $resultWords[] = $symphony['name'];
      }
      foreach($symphony['verseIds'] as $ids){
        $result[$ids] = intval($result[$ids]) + 1 ;
      }
    }

    $buf = [];
    foreach($result as $id => $count){
      $buf[$count][] = $id;
    }
    $resultMax = max(array_keys($buf));
    $resultIds = $buf[$resultMax] ?? [];
    $verse = self::getVersesByIds($resultIds, $resultWords)['books'];
    return [
      'text' => $text,
      'ids' => $resultIds,
      'words' => $resultWords,
      'max' => $resultMax,
      'startWords' => $words,
      'view' => $verse
    ];
  }

  public static function summResult($result, $ids){
   foreach($ids as $id){
     if(empty($result[$id])){
       $result[$id] = 1;
     }else{
       $result[$id]  = intval($result[$id]) + 1;
     }
   }
   return $result;
  }

  public static function searchParams($text, $lexems = false, $synonyms = false){
    $text = \Main\Text::cleanText($text)['text'];
    $result = [];
    $resultWords = [];
    $words = explode(' ', $text) ?? [];
    foreach($words as $word){
      if(mb_strlen($word) < 2){
        continue;
      }
      $resultWords[] = $word;
      $symphony = array_shift(self::getList(['filter' => ['UF_NAME' => $word], 'nav' => ['limit' => 1]])['symphony']);

      if(empty($symphony)){
        $symphony = self::getList(['filter' => ['UF_NAME' => $word . '%'], 'nav' => ['limit' => 100]])['symphony'];
        if(empty($symphony)){
          $symphony = self::getList(['filter' => ['UF_NAME' => '%' . $word . '%'], 'nav' => ['limit' => 100]])['symphony'];
        }
        $buf = [];
        foreach($symphony as $item){
          $buf['dictionaryWords'] = array_merge($buf['dictionaryWords'] ?? [], $item['dictionaryWords'] ?? []);
          $buf['lexem'] = array_merge($buf['lexem'] ?? [], $item['lexem'] ?? []);
          $buf['verseIds'] = array_merge($buf['verseIds'] ?? [], $item['verseIds'] ?? []);
          $buf['dictionaryIds'] = array_merge($buf['dictionaryIds'] ?? [], $item['dictionaryIds'] ?? []);
          $resultWords[$item['name']] = $item['name'];
        }
        $symphony = $buf;
        $resultWords = array_values($resultWords);
      }
      if(!empty($symphony)){
        $lexemsWords = $symphony['lexem'] ?? [];
        $dictionaryWords = $symphony['dictionaryWords'] ?? [];
      }else{
        continue;
      }
      $verseIds = $symphony['verseIds'];
      $result = self::summResult($result,$verseIds);
      $maxResultLight = $result;
      if($lexems) {
        $symphonyLexems = self::getList(['filter' => ['UF_NAME' => $symphony['lexem']]])['symphony'];
        foreach($symphonyLexems as $symphonyLexemsItem){
          $verseIds += $symphonyLexemsItem['verseIds'];
          $result = self::summResult($result,$symphonyLexemsItem['verseIds']);
        }
        if($synonyms){
          $symphonySynonyms = self::getList(['filter' => ['UF_NAME' => $symphony['lexem']]])['symphony'];
         foreach($symphonySynonyms as $item){
           $verseIds += $item['dictionaryIds'];
           $result = self::summResult($result,$item['dictionaryIds']);
           $dictionaryWords += $item['dictionaryWords'];
         }
        }
      }
    }
    $resultWords = array_unique(array_filter($resultWords + ($synonyms ? $dictionaryWords : []) + ($lexems ? $lexemsWords : [])));

    $buf = [];
    foreach($result as $id => $count){
      $buf[$count][] = $id;
    }
    $bufLight = [];
    foreach($maxResultLight as $id => $count){
      $bufLight[$count][] = $id;
    }
    $resultMax = max(array_keys($bufLight));
    $resultMaxLexems = max(array_keys($buf));
    $resultIds = $buf[$resultMax] ?? [];
    if($resultMax < $resultMaxLexems){
      for($i = $resultMax; $i <= $resultMaxLexems; $i++){
        $resultIds = array_merge($resultIds ?? [] ,$buf[$i] ?? []);
      }
    }
    $verse = self::getVersesByIds($resultIds, $resultWords)['books'];
    return [
      'text' => $text,
      'ids' => $resultIds,
      'words' => $resultWords,
      'max' => $resultMax,
      'startWords' => $words,
      'view' => $verse
    ];
  }

  public static function getMorfs($words){
    $buf = $words;
    foreach($words as $word){
      $buf = array_merge($buf ?? [], \Api\Lingvo::getLexems($word)['words']);
    }
    foreach($buf as $key => $word){
      $wordBuf = explode(',', $word);
      if(!empty($wordBuf[1])){
        $buf[$key] = $wordBuf[0];
      }
      $buf[$key] = \Main\Text::cleanText($buf[$key])['text'];
    }
    $buf = array_filter($buf);
    return ['words' => $buf];
  }

  public static function getVersesByIds($ids, $words = []){
    $books = \Bible\Books::getList(['idKeys' => "CODE"])['books'];
    $verses = \Bible\Verses::getByIds($ids)['verses'];
    foreach($verses as $verse){
      if(!empty($words)) {
        $verse['bold'] = \Main\Text::boldResult($verse['text'], $words)['text'];
      }
      $books[$verse['book']]['items'][] = $verse;
    }
    foreach($books as $keyBook => $book){
      if(empty($book['items'])){
        unset($books[$keyBook]);
      }
    }
    return ['books' => $books];
  }

  public static function getByWord($word){
    try{
      $words = [];
      $word = trim($word);
      if(mb_strlen($word) < 2){
        return false;
      }
      $searchResult = \Bible\Symphony::getList(['filter' => ['UF_NAME' => $word]], true)['symphony'];

      if(empty($serachResult)){
        $yandex = \Yandex\Speller::getResult($word);

        foreach($yandex['result'] as $var){
          $searchResult = array_merge($searchResult ?? [], \Bible\Symphony::getList(['filter' => ['UF_NAME' => $var]], true)['symphony'] ?? []);
        }
      }
     if(empty($searchResult)){
        $words = \Api\Lingvo::getLexems($_REQUEST['search'])['words'];
      }
      if(empty($searchResult)){
        $searchResult = \Bible\Symphony::getList(['filter' => ['UF_NAME' => $word . '%']], true)['symphony'];
      }
      if(empty($searchResult)){
        $searchResult = \Bible\Symphony::getList(['filter' => ['UF_NAME' => '%' . $word . '%']], true)['symphony'];
      }
      foreach($searchResult as $symphony){
        $words[] = $symphony['data'];
      }
      return ['words' => $words];
    }catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }
}
