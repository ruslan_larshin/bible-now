<?
namespace Learn;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Sermons extends \Main\Iblock
{
  public const iblockCode = 'sermon';
  public static function getMap(){
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'text' => 'DETAIL_TEXT',
      'title' => 'PROPERTY_TITLE',
    ];
  }

  public const replace = [
    '[quote]' => "<div class='quote'>",
    '[/quote]' => "</div>",
    '{verse}' => "<div class='verse right'>( ",
    '{/verse}' => " )</div>",
  ];

  public static function getList($request = []){
    try {
      $request['sort'] = ['NAME' => 'ASC'];
      $result = self::getListEmpty(self::iblockCode, self::getMap(), $request)['items'];
      foreach($result as $key => $sermon){
        $text = $sermon['text'];
        foreach(self::replace as $code => $replace){
          $text = str_replace($code, $replace, $text);
        }
        $result[$key]['textHtml'] = $text;
        $result[$key]['textArray'] = self::sermonByArray($sermon['text'])['text'];
      }
      return ['items' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function sermonByArray($text){
    $textArray = [];
    $text = html_entity_decode($text);
    $text = str_replace(['<br />', '<br/>', '<br>', '<p>'] , "\r\n", $text);
    while(mb_strlen($text) > 0){
      $first = mb_strpos( $text, '[quote]');
      $firstVerse = mb_strpos( $text, '{verse}');
      if($first > 0 && $firstVerse > 0){
        $buf = mb_substr($text, 0, $first);
        $textArray[] = [
          'type' => 'text',
          'text' => $buf,
        ];
      }elseif($first < $firstVerse) {
        $second = mb_strpos($text, '[/quote]') + mb_strlen('[/quote]');
        $buf = mb_substr($text, 0, $second);
        $textArray[] = [
          'type' => 'quote',
          'text' =>  trim(str_replace(['[quote]', '[/quote]'], '',$buf)),
        ];
      }elseif($first > 0 || $firstVerse > 0){
        $second = mb_strpos($text, '{/verse}') + mb_strlen('{/verse}');
        $buf = mb_substr($text, 0, $second);
        $textArray[] = [
          'type' => 'verse',
          'text' => trim(str_replace(['{verse}', '{/verse}'], '', $buf)),
        ];
      }else{
        $buf = $text;
        $textArray[] = [
          'type' => 'text',
          'text' => $buf,
        ];
      }
      $text = mb_substr($text, mb_strlen($buf));
    }
    return ['text' => $textArray];
  }
}
