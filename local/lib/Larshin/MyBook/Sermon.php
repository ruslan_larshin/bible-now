<?php
namespace Larshin\Sermon;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Sermon
{
	public $text;
	public $name;
	public $code;
	public $keywords;
	public $title;
	
	public function __construct()
    {
		return null;
    }
	
	public function allSelect()
	{
		return array('NAME' , 'DETAIL_TEXT' , 'CODE' , 'ID' ,'PROPERTY_KEYWORDS' , 'PROPERTY_TITLE');
	}
	
	public function updateQuote($text)
	{
		$result = $text;
		$text = str_replace('[quote]' , '<div class= "azure ">' , $text);
		$text = str_replace('[/quote]' , '</div>' ,$text);
		$text = str_replace('{verse}' , '<div class ="right blue">(' ,$text);
		$text = str_replace('{/verse}' , ')</div >' ,$text);
		return $text;
	}
	
	public function getList($arParams = array('search' => '' , 'code' => ''))
	{
		$result = null;
		$tools = new \Larshin\MainTools\Tools();
		$Filter = array('IBLOCK_ID' => sermonIbId , 'ACTIVE' => 'Y' , 'INCLUDE_SUBSECTIONS' => 'Y') ;
		if($arParams['search'])
			$Filter['NAME'] = '%' . $arParams['search'] . '%';
		if($arParams['code'])
			$Filter['CODE'] = $arParams['code'];
		$res = \CIBlockElement::GetList(array("NAME" => "ASC"), $Filter , false, false , array('NAME' , 'CODE' , 'ID' ,'DETAIL_TEXT' ));
		while ($data = $res->GetNextElement()) 
		{
			$sermonData = $data -> GetFields(); 
			$sermon = new \Larshin\Sermon\Sermon();
			$sermon -> id = $sermonData['ID'];
			$sermon -> name = $sermonData['NAME'];
			$sermon -> code = $sermonData['CODE'];
			$sermon -> text = $tools -> noTags( $this -> updateQuote($sermonData['~DETAIL_TEXT']));
		//	$sermon -> keywords = $sermonData['PROPERTY_KEYWORDS_VALUE'];
			//$sermon -> title = $sermonData['PROPERTY_TITLE_VALUE'];
			$result[] = $sermon;
		}
		return $result;
	}
	
	public function getByCode($code)
	{
		$result = array();
		$tools = new \Larshin\MainTools\Tools();
		if(!$code)
		{
			$result['error'][] = 'Введите символьный код проповеди';
		}
		if(!$result['error'])
		{
			$res = \CIBlockElement::GetList(array("NAME" => "ASC"), array('IBLOCK_ID' => sermonIbId , 'ACTIVE' => 'Y' , 'INCLUDE_SUBSECTIONS' => 'Y' , 'CODE' => $code) , false, false , $this -> allSelect());
			while ($data = $res->GetNextElement()) 
			{
				$sermonData = $data -> GetFields(); 
				$sermon = new \Larshin\Sermon\Sermon();
				$sermon -> id = $sermonData['ID'];
				$sermon -> name = $sermonData['NAME'];
				$sermon -> code = $sermonData['CODE'];
				$sermon -> text = $this -> updateQuote($sermonData['~DETAIL_TEXT']);
				$sermon -> keywords = $sermonData['PROPERTY_KEYWORDS_VALUE'];
				$sermon -> title = $sermonData['PROPERTY_TITLE_VALUE'];
				$result = $sermon;
			}
		}
		return $result; 
	}
}