<?php
namespace Larshin\View;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Test
{
	public function __construct()
    {
		return true;
    }
	
	public function viewOneQuestion($test , $number = null ,$params = [])
	{
		if($test -> question)
		{
			?>
				<div class='title3 bold'><?if($number){ echo $number . '. ';}?><?=$test -> question?></div>
				<ol>
					<li><div class='variantTest unselectable' data-answer='<?=$test -> answer[0]?> '><?=$test -> variant1?></div></li>
					<li><div class='variantTest unselectable' data-answer='<?=$test -> answer[1]?>' ><?=$test -> variant2?></div></li>
					<li><div class='variantTest unselectable' data-answer='<?=$test -> answer[2]?>' ><?=$test -> variant3?></div></li>
					<li><div class='variantTest unselectable' data-answer='<?=$test -> answer[3]?>' ><?=$test -> variant4?></div></li>
				</ol>
				<div class'levelTest'>Сложность : <?=$test-> level?> уровень (из 4)</div>
				<?if(!$params['noViewAnswer']){?>
                    <div class='viewAnswer'><span class='text'>Показать ответ:</span> <span class='answerText'><?=$test -> answerText?></span></div>
                <?}?>
                <?if($params['viewBook']){?>
                    <div class='viewBook'><span class='text'>Книги:</span> <span class=''>
                            <?foreach($test -> book as $key => $book){?>
                                <?=$book?> <?if($key < count($test -> book) -1 ){?>,<?}?>
                             <?}?>
                    </span></div>
                <?}?>
			    <div class="line"></div>
            <?
		}
		return true;
	}

	public function selectListMulty($list ,$defaultText){
	    //$list = ['id' => 'id', 'value' => 'value'];
        ?>
            <div class="selectMulty">
                <div class="selectItem default unselectable"><?=$defaultText?></div>
                <div class="list">
                    <?foreach($list as $item){?>
                        <div class="selectItem selecetedItemValue unselectable" data-id='<?=$item["id"]?>'><?=$item["value"]?></div>
                    <?}?>
                </div>
            </div>
        <?
    }
}




