<?php
namespace Larshin\Breadcrumbs;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Breadcrumbs
{
	public $name;
	public $url;
	
	public function __construct($name, $url)
    {
		$this -> name = $name;
		$this -> url = $url;
		return $this;
    }
	
	public function view($arBreadcrumbs)
	{
		?>
			<div class='Breadcrumbs Flex'>
				<?foreach($arBreadcrumbs as $breadcrumbs){?>
					<div class='font blue bc'>
						<a class = 'blue noDecoration' <?if($breadcrumbs -> url){?>href='<?=$breadcrumbs -> url?>'<?}?>> 
							<?=$breadcrumbs -> name?>
							<?if($breadcrumbs -> url){?>
								 &nbsp;<img class="open" data-id="1" src="/local/img/bcArrow2.png" />&nbsp;
							<?}?>
						</a>
					</div>
				<?}?>
			</div>
		<?
		return true;
	}
}
?>