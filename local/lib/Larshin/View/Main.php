<?php
namespace Larshin\View;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class MainView
{
	public function __construct()
    {
		return $this;
    }
	
	public function endWord($count , $flag = false)
	{
		$end = $count%10;
		$end2 = $count%100;
		if($end == 1 ){
		}elseif($end == 2 || $end == 3 || $end == 4)
		{
			if(!$flag)
			{
				echo 'а';
			}else{
				echo 'о';
			}
		}else{
			if(!$flag)
			{
				echo 'ов';
			}else{
				echo 'о';
			}
		}
		return null;
	}
	
	public function seachTitle($text , $count) 
	{
		$arSearch = new \Larshin\Search\Search();
		?><div class = 'searchTitle verse Flex'>По&nbsp;запросу <div class = 'cursiv bold azure'>&nbsp;"<?=$text?>"&nbsp;</div> найден<?=$this -> endWord($count , 1)?>  <div class = 'bold'><?='&nbsp;' . $count . '&nbsp;'?></div> результат<?=$this -> endWord($count)?></div></br><?
		return true;
	}
	
	public function otherWords($arOther) 
	{
		$arSearch = new \Larshin\Search\Search();
		foreach($arOther as $word => $words){
			?><div class = 'searchTitle verse Flex'>слово : <div class = 'cursiv bold azure'>&nbsp;"<?=$word?>"&nbsp;</div> исправлено на : <?
			foreach($words as $word2){
				?> <div class='cursiv'><?=$word2?> ,</div> <?
				
			}
			?></div></br><?
		}
		return true;
	}

    public function morfWords($arOther , $searcText)
    {
        $arSearch = new \Larshin\Search\Search();
        $search =url . 'search/?search='.$searcText;
             $i=1;
            ?><div class = 'searchTitle verse Flex'>запрос : <div class = 'cursiv bold azure'>&nbsp;"<?=$searcText?>"&nbsp;</div> имеет результаты по следующим формам : <?
            foreach($arOther as  $word2){
                ?> <div class='cursiv'><?=$word2?> <?if(count($arOther) > $i){?>,<?}?></div> <?
                $search .= ' ' . $word2;
                $i++;
            }
        ?><div><a style="margin-left: 20px;" class = 'cursiv  azure' href ="<?=$search . '&morf=y'?>"> найти </a></div>
        </div></br><?
        return true;
    }
	
	public function viewPager($pager ,$nowPage , $url)
	{
		?>
			<div class='pager'>
			<?foreach($pager-> pagers as  $pagerItem){?>
				<div class='pagerCantainer <?if($pagerItem -> name == $nowPage){echo 'active';}?>'><a href='<?=$url?>/<?=$pagerItem -> urlCode?>/' ><?=$pagerItem -> name?></a></div>
			<?}?>
			</div>
		<?
		return null;
	}
	
	public function viewSymphonyList($arSymphony)
	{
		foreach($arSymphony as $letter => $words){?>
		<div class='letterBlock <?if($arRequest['search']){?>Width Flex<?}?>'>
			<a  class='letterBlockA Width' href='/symphony/<?=$letter?>/'><?=mb_strtoupper($letter)?></a>
			<?foreach($words['items'] as $key=>$word){?>
				<div class='letterWord <?if($arRequest['search']){?>lineFlex<?}?>' >
					<a href='/symphony/<?=$word -> id?>/'><?=$word -> name?></a>
				</div>
			<?}?>
			<a  class='letterBlockA' href='/symphony/<?=$letter?>/'>...</a>
		</div> 
		<?}
		return null;
	}
	
	public function viewBibleInMain($arBook)
	{
		$arGroupOld=array(
			'0'=>'Пятикнижие Моисея',
			'5'=>'Исторические книги',
			'20'=>' ',
			'24'=>'Книги пророческие',
			'46'=>'Книги исторические(неканонические)',
		);
		$arGroupNew=array(
			'0'=>'Евангелия и Деяния',
			'5'=>'Соборные послания',
			'12'=>'Полания апостола Павла',
			'26'=>'Откровение',
		);

		?>
		<div class='bibleList'>
			<div class='leftColumn'>
				<div class='zavet_title'> КНИГИ ВЕТХОГО ЗАВЕТА</div>
					<?foreach($arBook['OLD'] as $key=>$book){?>
						<?if($arGroupOld[$key]){?>
							<?if($key!=0){echo '</div>';}?><div class='group_title'><?=$arGroupOld[$key]?></div><div class='group'>
						<?}?>
						<div class='bookName smallBookName'>
							<a href ='/bible/<?=$book -> code?>/1/'><?=$book -> name?></a>
						</div>
						<div class='bookName fullBookName'>
							<a href ='/bible/<?=$book -> code?>/1/'><?=$book -> fullName?></a>
						</div>
					<?}?>
				</div>
				</div>
			<div class='rightColumn'>
				<div class='zavet_title'> КНИГИ НОВОГО ЗАВЕТА</div>
					<?foreach($arBook['NEW'] as $key=>$book){?>
						<?if($arGroupNew[$key]){?>
							<?if($key!=0){echo '</div>';}?><div class='group_title'><?=$arGroupNew[$key]?></div><div class='group'>
						<?}?>
						<div class='bookName smallBookName'>
							<a href ='/bible/<?=$book -> code ?>/1/'><?=$book -> name ?></a>
						</div>
						<div class='bookName fullBookName'>
							<a href ='/bible/<?=$book -> code ?>/1/'><?=$book -> fullName?></a>
						</div>
					<?}?>
				</div>
				</div>
			</div>
		</div><?
		
		return null;
	}
	
	public function viewChapterQuote($arQuote)
	{
		$result = array();
		foreach($arQuote -> verse as $key=>$quote)
		{
			foreach($quote as $verse)
			{
				$this -> viewVerseNoChapter($verse , true);
			}
			?>
				<div class='right blue offset  '>( <?=$arQuote -> verseView[$key];?> )</div>
				</br>
			<?
		}
		return $result;
	}
	
	public function viewChapter($arChapter , $pager , $verseId = null ,$pageUrl = '/bible/' )
	{
		if($pager)
		{?>
		<div class='title1 bold'><?=$arChapter -> book -> fullName?> <span class='colorTitleBackground' style='display:none;'>Глава : <?=$arChapter -> chapterNum?></span></div><?
		$this -> viewPager($pager , $arChapter -> chapterNum , $pageUrl . $arChapter -> book -> code );
		}
			?>
				<div class='chapterBlock'>
				<?foreach($arChapter -> verses as $verse){?>
					<div class='verse <?if($verseId == $verse -> id){echo 'blue';}?>'>
						<div class='verseNumber font'><?=$verse -> number ?>. </div>
						<div class='verseText'><?=$verse -> text?></div>
					</div>
				<?}?>
			</div>
		<?
		if($pager)
			$this -> viewPager($pager , $arChapter -> chapterNum , $pageUrl . $arChapter -> book -> code );
		return null;
	}
	
	public function viewVerseNoChapter($verse , $short = false)
	{
		?>
			<div class='verse verseOpenChapter pointer <?if($short){?>black<?}?> verseOpenChapter_<?=$verse -> id?>' data-verse-id = '<?=$verse -> id?>'>
				<div class='verseNumber verseText <?if(!$short){?>bigPath<?}?>' data-verse-id = '<?=$verse -> id?>'>
					<?if(!$short){?>
						<nobr><?=$verse -> short . ' ' . $verse -> chapter . ':' . $verse -> number ?>. </nobr>
					<?}else{?>
						<nobr><?=$verse -> number ?>. </nobr>
					<?}?>
				</div> 
				<div class='verseText'><?=$verse -> text?></div>
			</div>
			<div class = 'chapterByVerseAjax result_<?=$verse -> id?>' ></div>
		<?
		return null;
	}
	
	public function noResult()
	{
		?>
			<br/>
			<div class='titleLittle font '>По данному запросу ничего не найдено!</div>
			<br/>
		<?return true;
	}
	
	public function viewRubric($array , $int )
	{
		?>
		<div class = 'rubric<?=$int?> Flex width100 noDisplay'>
			<?foreach($array as $i => $rubric){?>
				<div class='width<?if($i == 1){echo '100';}if($i == 2){echo '50';}if($i == 3){echo '30';}if($i == 4){echo '24';}?> paddingRight'>
					<?foreach($rubric as $letter => $item){?>
						<div><?=$letter?></div>
						<?foreach($item as $rubric){?>
							<div><?=$rubric -> name?></div>
						<?}?>
						<br/>
					<?}?>
				</div>
			<?}?>
		</div>
		<?
		return true;
	}
	
	public function viewVerseByBooksGroup($groupName , $code , $verseGroup , $uniqCode = '')
	{
		$result = true;
		$tools = new \Larshin\MainTools\Tools();
		?>
		<div class='title2 bold center'><?=$groupName?></div>
		<div class='center'>
			<?foreach($verseGroup as $book => $verseList2){?> 
				<?//view($verseList2)?>
				<span><a class='dottedNav' href ='/learn/rubric/<?=$code?>/#<?=$tools->translit($book) . $uniqCode?>' ><?=$book?>(<?=count($verseList2)?>)</a></span>
			<?}?>
		</div>
		<?foreach($verseGroup as $book => $verseList2){?>
			<div  id='<?=$tools->translit($book) . $uniqCode?>' class='title3 bold'><?=$book?></div>
			<?foreach($verseList2 as $verseList){?>
				<?foreach($verseList['verses'] as $verse){?>
					<?$this -> viewVerseNoChapter($verse , true)?>
				<?}?>
				<div class='blue  offset right'>( <?=$verseList['link'][0];?> )</div>
				</br>
			<?}?>
		<?}
		return $result;
	}
	
	public function viewVerseBlock($verseBlock)
	{
		foreach($verseBlock['text'] as $verse){?>
			<?$this -> viewVerseNoChapter($verse)?>
		<?}
		return true;
	}
	
	public function viewListBook($arList ,$codeUrl = 'apocrypha')
	{
		?>
		<ul class='lsitHref'>
				<?foreach($arList as $apocrypha){?>
				<li class ='title3'><a class =' font black noDecoration' href='/learn/<?=$codeUrl ?>/<?=$apocrypha -> code?>/'><?=$apocrypha-> name?></a></li>
			<?}?>
		</ul>
		<?
		return true;
	}
}




