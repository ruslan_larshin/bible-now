<?php
namespace Larshin\Apocrypha;


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;

class Apocrypha
{	
	public $code;
	public $id;
	public $name;
	
	
	public function allSelect()
	{
		$result = array('ID' , 'NAME' , 'CODE' , 'PROPERTY_CHAPTER' ,'PROPERTY_VERSE');
		return $result;
	}
	
	
	public function __construct()
    {
		return true;
    }
	
	public function getListBooks()
	{
		$result = array();
		$res = \CIBlock::GetList(Array(), Array('TYPE'=>'apocrypha', 'SITE_ID'=>SITE_ID, 'ACTIVE'=>'Y', ), true);
		while($ar_res = $res->Fetch())
		{
			$apocrypha = new \Larshin\Apocrypha\Apocrypha();
			$apocrypha -> code = $ar_res['CODE'];
			$apocrypha -> name = $ar_res['NAME'];
			$apocrypha -> id = $ar_res['ID'];
			$result[] = $apocrypha;
		}
		return $result;
	}
	
	public function getListChapter($codeBook)
	{
		$result = array();
		$res = \CIBlockElement::GetList(array(), array('IBLOCK_ID' => apocrypha[$codeBook] , 'ACTIVE' => 'Y'),  array('PROPERTY_CHAPTER'));
		while ($data = $res->GetNextElement()) 
		{
			$arFields = $data->GetFields();
			$result[$arFields['PROPERTY_CHAPTER_VALUE']] = $arFields['CNT'];
		}
		return $result;
	}
	
	public function getInfoByBook($codeBook)
	{
		$result = array();
		$res = \CIBlock::GetList(array(), array('ID' => apocrypha[$codeBook] , 'ACTIVE' => 'Y'));
		while($ar_res = $res->Fetch())
		{
			$result = new \Larshin\Apocrypha\BookInfo();
			$result -> id = $ar_res['ID'];
			$result -> name = $ar_res['NAME'];
			$result -> code = $ar_res['CODE'];
			$result -> description = $ar_res['DESCRIPTION'];
			$result -> chapterCount = count($this -> getListChapter($codeBook));
		}
		return $result;
	}
	
	public function getChapter($bookCode , $chapterNumber)
	{
	
		$result = array();
		$book = new \Larshin\Bibles\Book();
		$chapter = new \Larshin\Bibles\Chapter();
		$chapter -> book = $this -> getInfoByBook($bookCode);
		$chapter -> chapterNum = $chapterNumber;
		$res = \CIBlockElement::GetList(array('PROPERTY_VERSE' => 'ASC'), array('IBLOCK_ID' => apocrypha[$bookCode] , 'ACTIVE' => 'Y' ,'PROPERTY_CHAPTER' => $chapterNumber),  false, false, $this -> allSelect());
		while ($data = $res->GetNextElement()) 
		{
			$arFields = $data->GetFields();
			$verse = new \Larshin\Bibles\Verse();
			$verse -> book = $chapter-> book  -> name;
			$verse -> id = $arFields['ID'];
			$verse -> book =$chapter-> book  -> name;
			$verse -> bookCode = $chapter-> book  -> code;
			$verse -> short = $chapter-> book  -> name;
			$verse -> full = $chapter-> book  -> name;
			$verse -> chapter = $arFields['PROPERTY_CHAPTER_VALUE'];
			$verse -> number =  $arFields['PROPERTY_VERSE_VALUE'];
			$verse -> text =  $arFields['NAME'];
			$chapter -> verses[$arFields['PROPERTY_VERSE_VALUE']] = $verse;
		}
		ksort($chapter -> verses);
		return $chapter;
	}
}

class BookInfo
{
	public $id;
	public $name;
	public $code;
	public $description;
	public $chapterCount;
	
	public function __construct()
    {
		return true;
    }
	
}