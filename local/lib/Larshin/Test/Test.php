<?php
namespace Larshin\Test;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Test
{
	public function __construct()
    {
		return null;
    }
	
	public function allSelect()
	{
		return array('ID', 'NAME' , 'PROPERTY_VARIANT1' , 'PROPERTY_VARIANT2' , 'PROPERTY_VARIANT3' , 'PROPERTY_VARIANT4' , 'PROPERTY_LEVEL' , 'PROPERTY_ANSWER' , 'PROPERTY_BOOK' ,  'PROPERTY_CROSSWORD_ANSWER' , 'PROPERTY_ONE_ANSWER' , 'PROPERTY_CROSSWORD');
	}
	
	public function selectGame()
	{
		return array('ID', 'NAME' , 'PROPERTY_VARIANT1' , 'PROPERTY_VARIANT2' , 'PROPERTY_VARIANT3' , 'PROPERTY_VARIANT4' , 'PROPERTY_LEVEL' , 'PROPERTY_ANSWER' , 'PROPERTY_BOOK' );
	}
	
	public function getMap()
	{
		return array(
			'id' => 'id',
			'question' => 'текст вопроса (string)',
			'variant1' => 'Вариант1 (string)',
			'variant2'=> 'Вариант2 (string)',
			'variant3'=> 'Вариант3 (string)',
			'variant4'=> 'Вариант4 (string)',
			'level' => 'Уровень сложности 1-4 , (int)',
			'answer' => 'Ответ на вопрос (string)',
			'book' => 'Список книг (array( bookName => string ))',
			'bookText' => 'Список книг (string , string))',
			'crosswordAnswer' => 'Вопрос для кроссворда (bool)',
			'oneAnswer' => 'Вопрос с одним ответом (bool)',
			'crossword' => 'Ответ в кроссворде',
			'answerText' => 'Ответ через запятую для вывода(string , string)',
		);
	}
	
	public function getList($arParams = array('sort' => array('PROEPRTY_LEVEL_VALUE' => 'ASC') , 'filter' => array('IBLOCK_ID' => questionsIbId) , 'nav' => array() , 'select' => array('ID' , 'NAME')))
	{
		$result = array();
		$arParams['filter']['IBLOCK_ID'] = questionsIbId;
		$res = \CIBlockElement::GetList($arParams['sort'] , $arParams['filter'] ,  false, $arParams['nav'], $arParams['select']);
		while ($data = $res->GetNextElement()) 
		{
			$arFields = $data->GetFields();
			$test = new \Larshin\Test\Test();
			$test -> id = $arFields['ID'];
			$test -> question = $arFields['~NAME'];
			if(in_array('PROPERTY_VARIANT1' , $arParams['select']))
				$test -> variant1 = $arFields['PROPERTY_VARIANT1_VALUE'];
			if(in_array('PROPERTY_VARIANT2' , $arParams['select']))
				$test -> variant2 = $arFields['PROPERTY_VARIANT2_VALUE'];
			if(in_array('PROPERTY_VARIANT3' , $arParams['select']))
				$test -> variant3 = $arFields['PROPERTY_VARIANT3_VALUE'];
			if(in_array('PROPERTY_VARIANT4' , $arParams['select']))
				$test -> variant4 = $arFields['PROPERTY_VARIANT4_VALUE'];
			if(in_array('PROPERTY_LEVEL' , $arParams['select']))
				$test -> level = $arFields['PROPERTY_LEVEL_VALUE'];
			if(in_array('PROPERTY_ANSWER' , $arParams['select']))
			{
				$test -> answer = explode('/' , $arFields['PROPERTY_ANSWER_VALUE']);
				$test -> answerText = '';
				if($test -> answer[0] == 1)
					$test -> answerText = $test -> answerText .$test -> variant1 . ' , ';
				if($test -> answer[1] ==1)
					$test -> answerText = $test -> answerText .$test -> variant2 . ' , ';
				if($test -> answer[2] ==1)
					$test -> answerText = $test -> answerText .$test -> variant3 . ' , ' ;
				if($test -> answer[3] ==1)
					$test -> answerText = $test -> answerText . $test -> variant4. ' , ' ;
				$test -> answerText =  mb_substr($test -> answerText, 0, -2);
			}
			if(in_array('PROPERTY_BOOK' , $arParams['select']))
				$test -> book = $arFields['PROPERTY_BOOK_VALUE'];
			if(in_array('PROPERTY_CROSSWORD' , $arParams['select']))
				$test -> crossword = $arFields['PROPERTY_CROSSWORD_VALUE'];
			if(in_array('PROPERTY_CROSSWORD_ANSWER' , $arParams['select']))
			{
				$test -> crosswordAnswer = $arFields['PROPERTY_CROSSWORD_ANSWER_VALUE'];
			}
			if(in_array('PROPERTY_ONE_ANSWER' , $arParams['select']))
			{
				$test -> oneAnswer = $arFields['PROPERTY_ONE_ANSWER_VALUE'];
			}
			$result['questions'][] = $test;
            $count = intval($res->SelectedRowsCount());
			$result['pager'] = [
			    'count' => $count,
                'page' => intval($arParams['nav']['iNumPage']),
                'pageCount' => ceil($count / $arParams['nav']['nPageSize']) * 1,
            ];
		}
		return $result;
	}
	
	public function getById($id)
	{
		$result = array();
		if($id)
		{
			$arParams = array(
							'sort' => array(),
							'filter' => array('ID' => $id),
							'nav' => array('nPageSize' => 1),
							'select' => $this -> selectGame(),
						);
			$result = $this -> getList($arParams)['questions'][0];
		}
		return $result;
	}

	public function getListByGame($Params = ['filter' => [] , 'page' => 1 , 'nPageSize' => 10])
    {
        $result = [];
        $arParams = array(
            'sort' => [],
            'filter' => $Params['filter'],
            'nav' => ['nPageSize' => $Params['nPageSize'] ?? 10, 'iNumPage' => $Params['page'] ?? 1],
            'select' => $this -> selectGame(),
        );
        $result = $this -> getList($arParams);
        return $result;
    }

    public function getBookListrByTest(){
        $res = \CIBlockElement::GetList(false , ['IBLOCK_ID' => questionsIbId ],  ['PROPERTY_BOOK']);
        while ($data = $res->GetNextElement()) {
            $arFields = $data->GetFields();
            if(!empty($arFields['PROPERTY_BOOK_VALUE']))
                $buf[$arFields['PROPERTY_BOOK_VALUE']] = $arFields['CNT'];
        }
        $bibleObject =new \Larshin\Bibles\Bibles();
        $books = $bibleObject -> getBookList(true);
        foreach($books as $book)
        {
           if(!empty($buf[$book -> name]))
           {
               $result[$book -> name] = $buf[$book -> name];
           }
        }
        return $result;
    }
}