<?php
namespace Larshin\Bible;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;	
	
Loader::includeModule("highloadblock");
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/MainClass.php');
define("HighloadBooks", 227);
define("HighloadBook", 227);
define("HighloadFirstBook", 148);
define("HighloadSymphony", 228);
define("allways", 0);
define("alphabet", array('а','б','в','г','д','е','ж','з','и','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','э','ю','я'));
class Bible
{
    public function __construct(int $id=0)
    {
		return true;	 
    }
	
	public function getBookList()
	{
		$result = array();
		$rsData = $this->initHighLoad(HighloadBook)::getList(array(
		   "select" => array('ID','UF_FULL','UF_SHORT',"UF_BOOK",'UF_CHAPTER_COUNT','UF_VERSE_COUNT','UF_CODE','UF_ZAVET'),
		   "order" => array("UF_NUMBER" => "ASC"),
		   "filter" => array() , // Задаем параметры фильтра выборки
		   'limit' => 100,
		   'cache' => array(
				'ttl' => allways,
				'cache_joins' => true,
			)
		));

		while($arData = $rsData->Fetch()){
			if($arData['UF_ZAVET']=='Ветхий')
			{
				$result['OLD'][] = $arData;
			}else{
				$result['NEW'][] = $arData;
			}
		}
		return $result; 
	}
	
	public function getChapter($bookCode,$chapterNumber)
	{
		$result = array();
		$hgId = $this->getHgId($bookCode);
		$rsData = $this->initHighLoad($hgId)::getList(array(
		   "select" => array('ID','UF_TEXT','UF_VERSE',"UF_CHAPTER"),
		   "order" => array('UF_VERSE'=>'ASC'),
		   "filter" => array('UF_CHAPTER'=>$chapterNumber) , // Задаем параметры фильтра выборки
		   'limit' => 200,
		   'cache' => array(
				'ttl' => allways,
				'cache_joins' => true,
			)
		));
		while($arData = $rsData->Fetch()){
			$result['chapter'][] = $arData;
		}
		$result['book'] = $this->getBookInfo($bookCode);
		$result['pager'] = $this->getPager($result['book']['UF_CHAPTER_COUNT'],$chapterNumber);
		return $result;
	}
	
	public function getBookInfo($code)
	{
		$result = array();
		$rsData = $this->initHighLoad(HighloadBook)::getList(array(
		   "select" => array('ID','UF_NUMBER','UF_BOOK','UF_SHORT','UF_FULL','UF_ZAVET','UF_CHAPTER_COUNT','UF_CODE'),
		   "order" => array(),
		   "filter" => array('UF_CODE'=>$code) , // Задаем параметры фильтра выборки
		   'limit' => 1,
		   'cache' => array(
				'ttl' => allways,
				'cache_joins' => true,
			)
		));
		while($arData = $rsData->Fetch()){
			$result = $arData;
		}
		return $result;
	}
	
	public function searchGlobal($search)
	{
		if($search && $search != ' ')
		{
			$result = array();
			$rsData = $this->initHighLoad(HighloadSymphony)::getList(array(
			   "select" => array('ID','UF_NAME','UF_VERSE'),
			   "order" => array(),
			   "filter" => array('UF_NAME'=>$search) , // Задаем параметры фильтра выборки
			   'limit' => 100000,
			   'cache' => array(
					'ttl' => allways,
					'cache_joins' => true,
				)
			));
			while($arData = $rsData->Fetch()){
				if($arData['UF_VERSE'])
				{	
					$arVerse = array();
					foreach($arData['UF_VERSE'] as $code)
					{
						$buf = $this->getVerseFromCode($code);
						$buf['UF_TEXT'] = $this->boldResult($search,$buf['UF_TEXT']);
						$result['VERSE'][] = $buf;
					}
				}
				//$arData['VERSE'] = $arVerse;
				//$result[] = $arData;
			}
		}
		return $result;
	}
	
	public function wordList($letter,$search = '')
	{
		$result = array();
		$numCount = 10;
		if($search)
		{
			$numCount = 10000;
			$letter = $search;
		}
		if($letter)
		{
			$rsData = $this->initHighLoad(HighloadSymphony)::getList(array(
			   "select" => array('ID','UF_NAME'),
			   "order" => array('UF_NAME'=>'ASC'),
			   "filter" => array('UF_NAME' => $letter . '%','UF_VERSE'!='') , // Задаем параметры фильтра выборки
			   'limit' => $numCount,
			   'cache' => array(
					'ttl' => allways,
					'cache_joins' => true,
				)
			));
			while($arData = $rsData->Fetch()){
					$result[] = $arData;
			}
		}
		return $result;
	}
	
	public function symphonyMainList($search = '')
	{
		$result = array();
		foreach(alphabet as $letter)
		{
			if($search && $search !='' && mb_substr($search,0,1) == $letter) 
			{
				$result[$letter] = $this->wordList($letter,$search);
			}else{
				if(!$search)
					$result[$letter] = $this->wordList($letter);
			}
		}
		return $result;
	}
	
	public function symphonyDetail($wordId)
	{
		$result = array();
		if($wordId)
		{
			$rsData = $this->initHighLoad(HighloadSymphony)::getList(array(
			   "select" => array('ID','UF_NAME','UF_VERSE'),
			   "order" => array('UF_NAME'=>'ASC'),
			   "filter" => array('ID' => $wordId) , // Задаем параметры фильтра выборки
			   'limit' => 1,
			   'cache' => array(
					'ttl' => allways,
					'cache_joins' => true,
				)
			));
			while($arData = $rsData->Fetch()){
				foreach($arData['UF_VERSE'] as $code)
				{
					$buf = $this->getVerseFromCode($code);
					$buf['UF_TEXT'] = $this->boldResult($search,$buf['UF_TEXT']);
					$result['VERSE'][] = $buf;
				}
				$result['NAME'] = $arData['UF_NAME'];
			}
		}
		return $result;
	}
	
	public function boldResult($search,$text)
	{
		$resultBuf = str_replace($search,'<mark>' . $search . '</mark>', $text); 
		if($resultBuf == $text)
		{
			$resultBuf = str_replace(mb_strtoupper($search),'<mark>' . mb_strtoupper($search) . '</mark>', $text); 
		}
		if($resultBuf == $text)
		{
			$resultBuf = str_replace($this->mb_ucfirst($search),'<mark>' . $this->mb_ucfirst($search) . '</mark>', $text);  
		}
		if($resultBuf == $text)
		{
			$resultBuf = str_replace(mb_strtolower($search),'<mark>' . mb_strtolower($search) . '</mark>', $text);  
		}
		$text = $resultBuf;
		//--одно дело когда слово одно
		return trim($text);
	}
	
	public function mb_ucfirst($string, $encoding = 'UTF-8')  
	{
		$firstChar = mb_substr($string, 0, 1, $encoding);
		$then = mb_substr($string, 1, null, $encoding);
		return mb_strtoupper($firstChar, $encoding) . $then;
	}
	
	public function getVerseFromCode($code)
	{
		$result= array(); 
		$code = explode(':',$code);
		$rsData = $this->initHighLoad($code[0]*1)::getList(array(
		   "select" => array('ID','UF_TEXT','UF_ZAVET','UF_SHORT','UF_CHAPTER','UF_VERSE'),
		   "order" => array(),
		   "filter" => array('UF_VERSE'=>$code[2],'UF_CHAPTER'=>$code[1]) , // Задаем параметры фильтра выборки
		   'limit' => 1,
		   'cache' => array(
				'ttl' => allways,
				'cache_joins' => true,
			)
		));
		while($arData = $rsData->Fetch()){
			$result = $arData;
		}
		return $result;
	}
	
	public function getHgId($code)
	{
		$result = array();
		$rsData = $this->initHighLoad(HighloadBook)::getList(array(
		   "select" => array('ID','UF_NUMBER','UF_ZAVET'),
		   "order" => array(),
		   "filter" => array('UF_CODE'=>$code) , // Задаем параметры фильтра выборки
		   'limit' => 1,
		   'cache' => array(
				'ttl' => allways,
				'cache_joins' => true,
			)
		));
		while($arData = $rsData->Fetch()){
			$result = $arData['UF_NUMBER']*1 + HighloadFirstBook;
			if($arData['UF_ZAVET'] != 'Ветхий')
				$result++;
		}
		return $result;
	}
	
	public function initHighLoad($HgId)
	{
		$hlblock = HL\HighloadBlockTable::getById($HgId)->fetch(); 
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$entityHgload = $entity->getDataClass(); 
		return $entityHgload;	
	}
	
	public function getPager($pageCount,$pageNumber=1)
	{
		$result = array();
		if($pageCount <= 6){
			for($i=1;$i<=$pageCount;$i++)
			{
				$result[] = array('name'=>$i,'urlCode'=>$i);
			}
		}else{
			if($pageNumber <= 4)
			{
				for($i=1;$i<=$pageNumber+1;$i++)
					$result[] = array('name'=>$i,'urlCode'=>$i);				

				$result[] = array('name'=>'...','urlCode'=>ceil(($pageCount-$pageNumber+1)/2));
				$result[] = array('name'=>$pageCount,'urlCode'=>$pageCount);
			}else{
				$result[] = array('name'=>1,'urlCode'=>1);		
				$result[] = array('name'=>'...','urlCode'=>ceil(($pageNumber-2)/2));		
				$result[] = array('name'=>$pageNumber-1,'urlCode'=>$pageNumber-1);		
				$result[] = array('name'=>$pageNumber,'urlCode'=>$pageNumber);
				
				if($pageNumber != $pageCount && ($pageCount-$pageNumber) > 3)
					$result[] = array('name'=>$pageNumber+1,'urlCode'=>$pageNumber+1);	 
				
				if(($pageCount-$pageNumber) <= 3)
				{
					//for($i=$pageNumber-1;$i<=$pageCount;$i++)
					//	$result[] = array('name'=>$i,'urlCode'=>$i);	
				}else{
					$result[] = array('name'=>'...','urlCode'=>ceil(($pageCount-$pageNumber-1)/2+$pageNumber));
					$result[] = array('name'=>$pageCount,'urlCode'=>$pageCount);
				}
			}
		}
		return $result;
	}
	
	public function translit($str) 
	 {
		$rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
		$lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
		$str=str_replace(array(' ','"','/','_','.',';','!','^',':','+',',','«',"»",'(',')','&','?',':','/','?','#','[',']','@','!','$','&',"'",'(',')','*','+',',',';','=','~','%','{','}','|','”','>','<','E','”','№','%','Y',"’",'|','\\'),'',$str);
		if(!$str)
			$str = 'codeNote';
		return str_replace($rus, $lat, $str);
	}
	
	public function addPropertyHighload($highLoadBlockId,$name,$type,$title,$size = 20) //$type=string || integer
	{
		$userTypeEntity    = new \CUserTypeEntity();
		$userTypeData    = array(
			'ENTITY_ID'         => 'HLBLOCK_'.$highLoadBlockId,
			'FIELD_NAME'        => 'UF_' . $name,
			'USER_TYPE_ID'      => $type,
			'XML_ID'            => 'XML_ID_NAME',
			'SORT'              => 500,
			'MULTIPLE'          => 'N',
			'MANDATORY'         => 'Y',
			'SHOW_FILTER'       => 'N',
			'SHOW_IN_LIST'      => '',
			'EDIT_IN_LIST'      => '',
			'IS_SEARCHABLE'     => 'N',
			'SETTINGS'          => array(
				'DEFAULT_VALUE' => '',
				'SIZE'          => '20',
				'ROWS'          => '1',
				'MIN_LENGTH'    => '0',
				'MAX_LENGTH'    => '0',
				'REGEXP'        => '',
		),
			'EDIT_FORM_LABEL'   => array(
				'ru'    => $title,
				'en'    => 'Property name',
			),
			'LIST_COLUMN_LABEL' => array(
				'ru'    => $title,
				'en'    => 'Property name',
			),
			'LIST_FILTER_LABEL' => array(
				'ru'    => $title,
		'en'    => 'Property name',
		),
			'ERROR_MESSAGE'     => array(
				'ru'    => 'Ошибка при заполнении пользовательского свойства ' . $title,
				'en'    => 'An error in completing the user field <Property name>',
		),
			'HELP_MESSAGE'      => array(
				'ru'    => '',
		'en'    => '',
			),
		);
		$userTypeId = $userTypeEntity->Add($userTypeData);
		return $userTypeId;
	}
	
	public function getRequest()
	{
		$arRequest=array();
		$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		$postValues = $request->getPostList()->toArray(); 
		$getValues = $request->getQueryList()->toArray();
		$arRequest=array_merge($postValues,$getValues);
		
		return $arRequest;
	}
}
	