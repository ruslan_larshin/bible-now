<?php
namespace Larshin\Quote;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Quote
{
	public $id;
	public $name;
	public $verse;
	public $verseView;
	public $book;
	public $code;
	
	public function __construct()
    {
		return null;
    }
	
	public function allSelect()
	{
		return array('ID' , 'NAME' , 'PROPERTY_VERSES' , 'PROPERTY_BOOK' ,'PROPERTY_VERSES_VIEW' ,'CODE');
	}
	
	public function getList()
	{
		$result = array();
		$res = \CIBlockElement::GetList(array("ID" => "ASC"), array('IBLOCK_ID' => bibleQuoteIbId , 'ACTIVE' => 'Y' , 'INCLUDE_SUBSECTIONS' => 'Y') , false, false , $this -> allSelect());
		while ($data = $res->GetNextElement()) 
		{
			$quoteData = $data -> GetFields();
			//view($quoteData);
			$quote = new \Larshin\Quote\Quote();
			$quote -> id = $quoteData['ID'];
			$quote -> name = $quoteData['NAME'];
			$quote -> code = $quoteData['CODE'];
			$quote -> book = $quoteData['PROPERTY_BOOK_VALUE'];
			$quote -> verse = $quoteData['PROPERTY_VERSES_VALUE'];
			$quote -> verseView = $quoteData['PROPERTY_VERSES_VIEW_VALUE'];
			$result[] = $quote;
		}
		return $result;
	}
	
	public function getByCode($quoteId)
	{
		$result = array();
		if(!$quoteId)
		{
			$result['error'][]= 'Необходим идентификатор!';
		}else{
		$res = \CIBlockElement::GetList(array("ID" => "ASC"), array('IBLOCK_ID' => bibleQuoteIbId , 'ACTIVE' => 'Y' , 'INCLUDE_SUBSECTIONS' => 'Y' , 'CODE' => $quoteId) , false, false , $this -> allSelect());
		while ($data = $res->GetNextElement()) 
			{
				$quoteData = $data -> GetFields();
				$quote = new \Larshin\Quote\Quote();
				$quote -> id = $quoteData['ID'];
				$quote -> name = $quoteData['NAME'];
				$quote -> code = $quoteData['CODE'];
				$quote -> book = $quoteData['PROPERTY_BOOK_VALUE'];
				$quote -> verse = $quoteData['PROPERTY_VERSES_VALUE'];
				foreach($quoteData['PROPERTY_VERSES_VALUE'] as $key=> $quoteList)
				{
					$verseObject = new \Larshin\Bibles\Verse();
					$quote -> verse[$key] = $verseObject -> getList(explode('-' , $quoteList));
				}
				$quote -> verseView = $quoteData['PROPERTY_VERSES_VIEW_VALUE'];
				$result = $quote;
			}
		}
		return $result;
	}
	
	public function getListByBooks()
	{
		$result = array();
		$bookObject = new \Larshin\Bibles\Bibles();
		$bookList = $bookObject -> getBookList();
		$quoteList = $this -> getList();
		foreach($bookList as $key => $value)
		{
			foreach($bookList[$key] as $book)
			{
				foreach($quoteList as $quote)
				{
					if($book -> name == $quote -> book)
					{
						$result[$key][$book -> name][] = $quote;
					}
				}
			}
		}
		return $result;
		
	}

}
?>