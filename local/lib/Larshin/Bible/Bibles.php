<?php
namespace Larshin\Bibles;  

use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Bibles
{
	public function __construct()
    {
		return null;
    }

	public function getBookList($together = false)
	{
		$result = array();
		$Books = new \Larshin\MainTools\HighLoad(HighloadBook);
		$rsData  = $Books ->getList(array('ID','UF_FULL','UF_SHORT',"UF_BOOK",'UF_CHAPTER_COUNT','UF_VERSE_COUNT','UF_CODE','UF_ZAVET'));
		foreach($rsData['items'] as $key => $book)
		{
			$item = new \Larshin\Bibles\Book();
			$item -> id = $book['ID'];
			$item -> number = $key;
			$item -> name = $book['UF_BOOK'];
			$item -> shortName = $book['UF_SHORT'];
			$item -> fullName = $book['UF_FULL'];
			$item -> zavet = $book['UF_ZAVET'];
			$item -> chapterCount = $book['UF_CHAPTER_COUNT'];
			$item -> code = $book['UF_CODE'];
			if($book['UF_ZAVET']=='Ветхий')
			{
				if(!$together)
				{
					$result['OLD'][] = $item;
				}else{
					$result[] = $item;
				}
			}else{
				if(!$together)
				{
					$result['NEW'][] = $item;
				}else{
					$result[] = $item;
				}
			}
		}
	
		return $result; 
	}
	
	public function getChapter($bookCode , $chapterNumber)
	{
		$result = array();
		$Books = new \Larshin\MainTools\HighLoad(Bible);
		$book = new \Larshin\Bibles\Book();
		$chapter = new \Larshin\Bibles\Chapter();
		$chapter -> book = $book -> getBookInfo($bookCode);
		$chapter -> chapterNum = $chapterNumber;
		$rsData  = $Books ->getList(array('UF_SHORT' , 'UF_FULL' , 'UF_TEXT' , 'UF_ZAVET' , 'UF_CHAPTER' , 'UF_VERSE' , 'UF_CODE_BOOK' , 'ID' , 'UF_BOOK' , ) , array('UF_CHAPTER'=>$chapterNumber , 'UF_CODE_BOOK' => $bookCode));
		foreach($rsData['items'] as $item){
			$verse = new \Larshin\Bibles\Verse();
			$verse -> book = $chapter -> book;
			$verse -> id = $item['ID'];
			$verse -> book = $item['UF_BOOK'];
			$verse -> bookCode = $item['UF_CODE_BOOK'];
			$verse -> short = $item['UF_SHORT'];
			$verse -> full = $item['UF_FULL'];
			$verse -> chapter = $chapter -> chapterNum;
			$verse -> number = $item['UF_VERSE'];
			$verse -> text = $item['UF_TEXT'];
			$chapter -> verses[] = $verse;
		}
		return $chapter;
	}
	
	public function getChapterByVerseId($idVerse)
	{
		$result = null;
		if($idVerse)
		{
			$arVerse = new \Larshin\Bibles\Verse();
			$verse = $arVerse -> getById($idVerse);
			if($verse -> bookCode && $verse -> chapter)
			{
				$result = $this -> getChapter($verse -> bookCode , $verse -> chapter);
			}else{
				$result['error'][] = 'Данный стих не найден!';
			}
		}else{
			$result['error'][] = 'не указан идентификатор стиха!';
		}
		return $result;
	}
}

class Verse
{
	public $id;
	public $book;
	public $bookCode;
	public $short;
	public $full;
	public $chapter;
	public $number;
	public $text;
	
	public function __construct()
    {
		return null;
    }
	
	public function allSelect()
	{
		return array('UF_SHORT' , 'UF_FULL' , 'UF_TEXT' , 'UF_ZAVET' , 'UF_CHAPTER' , 'UF_VERSE' , 'UF_CODE_BOOK' ,"ID" , 'UF_BOOK');
	}
	
	public function getById($id)
	{
		$result = null;
		if(!$id){
			$result['ERROR'][] = 'Введите идентификатор!';
		}
		if(!$result['ERROR'])
		{
			$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
			$result = $bibleAll -> getList($this -> allSelect() , array('ID' => $id) , 'ID' , 'ASC' , 1);
			if(!$result['items'][0]['UF_TEXT'])
			{
				$result['ERROR'][] = 'невалидный стих!';
			}else{
				$this -> id = $result['items'][0]['ID'];
				$this -> book = $result['items'][0]['UF_BOOK'];
				$this -> bookCode = $result['items'][0]['UF_CODE_BOOK'];
				$this -> chapter = $result['items'][0]['UF_CHAPTER'];
				$this -> number = $result['items'][0]['UF_VERSE'];
				$this -> text = $result['items'][0]['UF_TEXT'];
				$this -> short = $result['items'][0]['UF_SHORT'];
				$this -> full = $result['items'][0]['UF_FULL'];
				$result = $this;
			}
		}
		return $result;
	}
	
	public function getList($arID)
	{
		$result = null;
		if(!$arID){
			$result['ERROR'][] = 'Введите идентификатор!';
		}
		if(!$result['ERROR'])
		{
			$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
			foreach($arID as $key => $id)
			{
				if(!$id)
					unset($arID[$key]);
			}
			$result2 = $bibleAll -> getList($this -> allSelect() , array('ID' => $arID) , 'ID' , 'ASC' , count($arID));
			if(!$result2['items'][0]['UF_TEXT'])
			{
				$result['ERROR'][] = 'невалидный стих!';
			}else{
				foreach($result2['items'] as $item)
				{
					$verse = new \Larshin\Bibles\Verse();
					$verse -> id = $item['ID'];
					$verse -> book = $item['UF_BOOK'];
					$verse -> bookCode = $item['UF_CODE_BOOK'];
					$verse -> chapter = $item['UF_CHAPTER'];
					$verse -> number = $item['UF_VERSE'];
					$verse -> text = $item['UF_TEXT']; 
					$verse -> short = $item['UF_SHORT'];
					$verse -> full = $item['UF_FULL'];
					$result[$item['ID']] = $verse;
				}
			}
		}
		return $result;
	}
	
	public function getChapter($book , $chapter)
	{
		$result = null;
		if(!$book){
			$result['ERROR'][] = 'Введите книгу!';
		}
		if(!$chapter){
			$result['ERROR'][] = 'Введите главу!';
		}
		if(!$result['ERROR'])
		{
			$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
			$result = $Hg -> getList($this -> allSelect() , array('ID' => $id) , 'ID' , 'ASC' , 1);
			if(!$result['UF_TEXT'])
			{
				$result['ERROR'][] = 'невалидный стих!';
			}
		}
		return $result;
	}
	
	public function makeShortLink($bufVerse)
	{
		$result = array();
		$link = '';
		$lastNumber = -10;
		//$link = $bufVerse['book'] . ' ' . $bufVerse['chapter'] . ' : ';
		foreach($bufVerse['verseArray'] as $chapter => &$verseList)
		{
			foreach($verseList as $key => $value)
			{
				if($key == 0)
				{
					$lastNumber = $value;
					$link = $value;
				}elseif( ($value - $lastNumber) > 1){
					$lastNumber = $value;
					$link = $link . ',' . $value;
				}elseif(!$verseList[$key + 1]){
					$lastNumber = $value;
					$link = $link . ',' . $value;
				}elseif( ($verseList[$key + 1] - $value) > 1 ){
					$link = $link . ',' . $value;
					$lastNumber = $value;
				}else{
					$link = $link . '-';
					$lastNumber = $value;
				}
				
				
				
			}	
			$link = str_replace('-,' , '-', $link);
			for($i = 0; $i < count($verseList); $i++)
			{
				$link = str_replace('--' , '-', $link);
			}
			$result[] = $bufVerse['book'] . ' ' . $bufVerse['chapter'] . ' : ' . $link;
		}
		return $result;
	}
	
	public function getVerseByString($text)
	{
		$result = array();
		$text = trim ( str_replace(array('.',';'),'',$text) );
		$verse['book'] = explode(' ' ,$text)[0];
		$text = str_replace($verse['book'] , '', $text);
		$text = str_replace(' ' , '', $text);
		$verse['chapter'] = explode(':' ,$text)[0];
		$text = explode(':' ,$text)[1];
		$bufVerse = explode(',' , $text);
		$bufVerse2 = array();
		foreach($bufVerse as $item)
		{
			if(str_replace('-','',$item) != $item)
			{
				$buf3 = explode('-',$item);
				if($buf3[0] && $buf3[1] && !$buf3[3])
				{
					for($i = $buf3[0]*1 ; $i <= $buf3[1] *1; $i++)
					{
						$bufVerse2[$i * 1] = $i * 1;
					}
				}
			}else{
				$bufVerse2[$item * 1] = $item * 1;
			}
		}
		$verse['verses']= $bufVerse2;
		foreach($verse['verses'] as $number)
		{
			$verseClass  = new \Larshin\Bibles\Verse();
			$verseClass -> number = $number;
			$verseClass -> short = $verse['book'];
			$verseClass -> chapter = $verse['chapter'];
			$result[] = $verseClass;
		}
		
		return $result;
	}
	
	public function getVerseFromString($verses)
	{
		$result = array();
		$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
		$result2 = $bibleAll -> getList($this -> allSelect() , array('UF_CHAPTER' => $verses -> chapter , 'UF_VERSE' => $verses -> number , 'UF_SHORT' => $verses -> short) , 'ID' , 'ASC' );
		foreach($result2['items'] as $item)
		{
			$verse = new \Larshin\Bibles\Verse();
			$verse -> id = $item['ID'];
			$verse -> book = $item['UF_BOOK'];
			$verse -> bookCode = $item['UF_CODE_BOOK'];
			$verse -> chapter = $item['UF_CHAPTER'];
			$verse -> number = $item['UF_VERSE'];
			$verse -> text = $item['UF_TEXT']; 
			$verse -> short = $item['UF_SHORT'];
			$verse -> full = $item['UF_FULL'];
			$result[$item['ID']] = $verse;
		}		
		return $result;
	}
	
	public function getVerseFromStringArray($verses)
	{
		$result = array();
		$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
		$arVerse = array();
		foreach($verses as $item){
			$arVerse[] = $item -> number;
		}
		//view(array('UF_CHAPTER' => $verses[0] -> chapter , 'UF_VERSE' => $arVerse , 'UF_SHORT' => $verses[0] -> short));
		$result2 = $bibleAll -> getList($this -> allSelect() , array('UF_CHAPTER' => $verses[0] -> chapter , 'UF_VERSE' => $arVerse , 'UF_SHORT' => $verses[0] -> short) , 'ID' , 'ASC' );
		foreach($result2['items'] as $item)
		{
			$verse = new \Larshin\Bibles\Verse();
			$verse -> id = $item['ID'];
			$verse -> book = $item['UF_BOOK'];
			$verse -> bookCode = $item['UF_CODE_BOOK'];
			$verse -> chapter = $item['UF_CHAPTER'];
			$verse -> number = $item['UF_VERSE'];
			$verse -> text = $item['UF_TEXT']; 
			$verse -> short = $item['UF_SHORT'];
			$verse -> full = $item['UF_FULL'];
			$result[$item['ID']] = $verse;
		}		
		return $result;
	}
}

class Chapter
{
	public $book;
	public $chapterNum;
	public $verses;

	public function __construct()
	{
		return null;
	}
}

class Book
{
	public $id;
	public $number;
	public $name;
	public $shortName;
	public $fullName;
	public $zavet;
	public $chapterCount;
	public $code;

	public function __construct()
	{
		return null;
	}
	
	public function getBookInfo($code)
	{
		$result = array();
		$rsData = new \Larshin\MainTools\HighLoad(HighloadBook);
		$rsData = $rsData -> getList(array('ID','UF_NUMBER','UF_BOOK','UF_SHORT','UF_FULL','UF_ZAVET','UF_CHAPTER_COUNT','UF_CODE') , array('UF_CODE'=>$code) , 'ID' , 'ASC' , 1);
		foreach($rsData['items'] as $arData){
			$this -> id = $arData['ID'];
			$this -> number = $arData['UF_NUMBER'];
			$this -> name = $arData['UF_BOOK'];
			$this -> shortName = $arData['UF_SHORT'];
			$this -> fullName = $arData['UF_FULL'];
			$this -> zavet = $arData['UF_ZAVET'];
			$this -> chapterCount = $arData['UF_CHAPTER_COUNT'];
			$this -> code = $arData['UF_CODE'];
			$result = $this; 
		}
		return $result;
	}
}
	
?>