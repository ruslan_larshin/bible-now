<?php
namespace Larshin\Search;


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;	


class Search
{
	public $idWordInSymphony;
	public $count;
	public $point;
	public $words;
	public $verse;
	
    public function __construct()
    {
		return null;
	}
	
	public function updateText($text)
	{
		$result = null;
		//обработаем текст для сохранений разбив на слова - далее синонимы и формы слова
		$tools = new \Larshin\MainTools\Tools();
		//2)а теперь перекодируем в кириллицу
		$text = $tools -> switcherRu($text);
		//1) удалим все спецсимволы нужны ли числа?? пока оставим
		$text = $tools -> unTranslit(trim($text));
		return $text;
	}
	
	public function lightSearch($text ,$all = false)
	{
		$result = null;
		$result['count'] = 0;
		$otherWord = array();
		$arVerse = new \Larshin\Bibles\Verse();
		$maxCount = 1;
		$arVerseBase = array();
		$arVerseIdAll = array();
		$arSymphony = new \Larshin\Search\Symphony();
		$tools = new \Larshin\MainTools\Tools();
		//$arVerse = new \Larshin\Bibles\Verse();
		$text = $this -> updateText($text);
		if($text)
		{
			//2) разобьем на слова и получим все значени, соберем значения вместе?! как ?! нужно еще определить по колличеству попаданий!?
			$arWords = explode(' ' , $text);
			foreach($arWords as $word)
			{
				if($word && mb_strlen($word)> 1)
				{
				    //тут соберем все морфологические формы слова!
                    $morf = new \Larshin\MainTools\HighLoad(HgMorfDictionary);
                    $morfInclude = $morf -> getList(['ID' ,'UF_WORDS'] , ['UF_WORDS' => $word] , 'ID' ,'ASC' , 50000);
                    foreach($morfInclude['items'][0]['UF_WORDS'] as $var){
                        if($var)
                            $result['morf'][$var] =$var;
                    }
					$symphonyResult = $arSymphony -> getByName($word);
					if($symphonyResult -> verseId)
					{
						$arVerseId = explode('-' , $symphonyResult -> verseId);
						foreach($arVerseId as $verseId)
						{
							if($verseId)
							{
								if(!$result['items'][$verseId])
								{
									$search = null;
									$search = new \Larshin\Search\Search();
									$search -> words[] = $words;
									$search -> id = $verseId;
									$search -> idWordInSymphony = $symphonyResult -> id;
									$search -> count = 1;
									$arVerseIdAll[$verseId] = $verseId;
									$result['items'][$verseId] = $search; 
									$result['count']++;
									
								}else{
									$result['items'][$verseId] -> count = $result['items'][$verseId] -> count + 1;
									if($maxCount < $result['items'][$verseId] -> count)
										$maxCount = $result['items'][$verseId] -> count;
									$result['items'][$verseId] -> words[] = $words;
								}
							}	
						}
					}else{//слово не найдено ищем в ошибках?*
						//1) найдем вложения данного слова
					}
				}
			}
			//мы будем искать по ошибкам только если мало результатов по известномуу!
			if(count($result['items']) <= 1 || ( count($arWords) > 4  && count($result['items']) <= 100))
			{
				foreach($arWords as $word)
				{
					if($word && mb_strlen($word)> 1)
					{
						$symphonyResultAll = $arSymphony -> getByNameSearch('%' . $word . '%');
						foreach($symphonyResultAll as $symphonyResult)
						{
						    if($word != $symphonyResult -> name)
							    $result['other'][$word][] = $symphonyResult -> name;
							//view($symphonyResultAll);
							$arVerseId = explode('-' , $symphonyResult -> verseId);
							foreach($arVerseId as $verseId)
							{
								if($verseId)
								{
									if(!$result['items'][$verseId])
									{
										$search = null;
										$search = new \Larshin\Search\Search();
										$search -> words[] = $words;
										$search -> id = $verseId;
										$search -> idWordInSymphony = $symphonyResult -> id;
										$search -> count = 1;
										$arVerseIdAll[$verseId] = $verseId;
										$result['items'][$verseId] = $search; 
										$result['count']++;
										
									}else{
										$result['items'][$verseId] -> count = $result['items'][$verseId] -> count + 1;
										if($maxCount < $result['items'][$verseId] -> count)
											$maxCount = $result['items'][$verseId] -> count;
										$result['items'][$verseId] -> words[] = $words;
									}
								}	
							}
						}
					}	
			}
			}
		}
		if($arVerseIdAll)
		{
			$arVerseBase = $arVerse -> getList($arVerseIdAll);
		}
		foreach($result['items'] as $verseId => &$item)
		{
			$item -> verse = $arVerseBase[$verseId];
			foreach($arWords as $word)
			{
				if($word && mb_strlen($word)> 1)
				{
					$item -> verse -> text = $tools -> boldResult($word ,  $item -> verse -> text);
				}
			}
		}
		//пересортируем по count(нахождению в строке)
		$result['sort'] = array('maxCount' =>  $maxCount);
		foreach($result['items'] as $idVerse => $value)
		{
			if( ($maxCount - $value -> count) < 3 || $all)
			    $result['sort']['items'][$value -> count][] = $idVerse;
		}
		$result['count'] = 0;
		foreach($result['sort']['items'] as $value){
			$result['count'] = $result['count'] + count($value);
		}
		return $result;
	}
}


class Symphony
{
    public function __construct()
    {
		return null;
	}
	
	public function allSelect() 
	{
		return array('ID' , 'UF_ID', 'UF_NAME');
	}
	
	public function getById($idSymphony)
	{
		$result = null;
		if($idSymphony){
			$symphony = new \Larshin\MainTools\HighLoad(Symphony);
			$resultBuf = $symphony -> getList($this -> allSelect() , array('ID' => $idSymphony) , 'UF_NAME' , 'ASC' , 1);
			foreach($resultBuf['items'] as $item)
			{
				$symphonyItem = new \Larshin\Search\SymphonyWord();
				$symphonyItem -> id = $item['ID'];
				$symphonyItem -> name = $item['UF_NAME'];
				$symphonyItem -> verseId = $item['UF_ID'];
				$result = $symphonyItem;
			}
		}else{
			$result['ERROR'][] = 'не указан идентификатор симфонии!';
		}
		return $result;
	}
	
	public function getByName($idName)
	{
		$result = null;
		if($idName){
			$symphony = new \Larshin\MainTools\HighLoad(Symphony);
			$resultBuf = $symphony -> getList($this -> allSelect() , array('UF_NAME' => $idName) , 'UF_NAME' , 'ASC' , 1);
			foreach($resultBuf['items'] as $item)
			{
				$symphonyItem = new \Larshin\Search\SymphonyWord();
				$symphonyItem -> id = $item['ID'];
				$symphonyItem -> name = $item['UF_NAME'];
				$symphonyItem -> verseId = $item['UF_ID'];
				$result = $symphonyItem;
			}
		}else{
			$result['ERROR'][] = 'не указан идентификатор симфонии!';
		}
		return $result;
	}
	
	public function getByNameSearch($idName)
	{
		$result = null;
		if($idName){
			$symphony = new \Larshin\MainTools\HighLoad(Symphony);
			$resultBuf = $symphony -> getList($this -> allSelect() , array('UF_NAME' => $idName) , 'UF_NAME' , 'ASC' , 100);
			foreach($resultBuf['items'] as $item)
			{
				$symphonyItem = new \Larshin\Search\SymphonyWord();
				$symphonyItem -> id = $item['ID'];
				$symphonyItem -> name = $item['UF_NAME'];
				$symphonyItem -> verseId = $item['UF_ID'];
				$result[] = $symphonyItem;
			}
		}else{
			$result['ERROR'][] = 'не указан идентификатор симфонии!';
		}
		return $result;
	}
	
	public function wordList($letter,$search = '')
	{
		$result = array();
		$numCount = 10;
		if($search)
		{
			$numCount = 10000;
			$letter = $search;
		}
		if($letter)
		{
			$symphony = new \Larshin\MainTools\HighLoad(Symphony);
			$resultBuf = $symphony -> getList($this -> allSelect() , array('UF_NAME' => $letter . '%',) , 'UF_NAME' , 'ASC' , $numCount);
			foreach($resultBuf['items'] as $item)
			{
				$symphonyItem = new \Larshin\Search\SymphonyWord();
				$symphonyItem -> id = $item['ID'];
				$symphonyItem -> name = $item['UF_NAME'];
				$symphonyItem -> verseId = $item['UF_ID'];
				$result['items'][] = $symphonyItem;
			}
		}
		return $result;
	}
	
	public function symphonyMainList($search = '')
	{
		$result = array();
		$searchObject = new \Larshin\Search\Search();
		$search = $searchObject -> updateText($search);
		$cache = \Bitrix\Main\Data\Cache::createInstance(); // Служба кеширования
		if ($cache->initCache(0, 'symphony' . $search, 'bible'))
		{
			$result = $cache->getVars(); // Получаем переменные
			$cache->output(); // Выводим HTML пользователю в браузер
		}
		elseif ($cache->startDataCache())
		{
			foreach(alphabet as $letter)
			{
				if($search && $search != '' && mb_substr($search,0,1) == $letter) 
				{
					$result[$letter] = $this->wordList($letter,$search);
					if(count($result[$letter]) == 0)
						unset($result[$letter]);
				}else{
					if(!$search)
						$result[$letter] = $this->wordList($letter);
				}
			}
			$cacheInvalid = false;
			if ($cacheInvalid)
			{
				$cache->abortDataCache();
			}
			 
			$cache->endDataCache($result);
		}
		return $result;
	}
	
	 public function getVerseByIdSymphonyResult($idSymphony)
	 {
		$result = null;
		$tools = new \Larshin\MainTools\Tools();
		$symphony = $this -> getById($idSymphony);
		$verseArray = explode('-' , trim($symphony -> verseId ));
		//теперь получим стихи
		foreach($verseArray as $verseId)
		{
			if($verseId)
			{
				$verseObject = new \Larshin\Bibles\Verse();
				$verse = $verseObject -> getById($verseId);
				$verse -> text = $tools -> boldResult($symphony -> name , $verse -> text);
				$result[] = $verse;
			}
		}
		return $result;
	 }
}

class SymphonyWord
{
	public $id;
	public $name;
	public $verseId;
	public $verse;
	
    public function __construct()
    {
		return null;
	}
}