<?php
namespace Nota\Bibles;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Verse
{
	public $id;
	public $short;
	public $full;
	public $zavet;
	public $chapter;
	public $verse;
	public $text;
	public $idHg;
	
	public function __construct()
    {
		return null;
    }
	
	public function allSelect()
	{
		return array('UF_SHORT' , 'UF_FULL' , 'UF_TEXT' , 'UF_ZAVET' , 'UF_CHAPTER' , 'UF_VERSE' , 'UF_CODE_BOOK');
	}
	
	public function getById($id)
	{
		$result = null;
		if(!$id){
			$result['ERROR'][] = 'Введите идентификатор!';
		}
		if(!$result['ERROR'])
		{
			$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
			$result = $Hg -> getList($this -> allSelect() , array('ID' => $id) , 'ID' , 'ASC' , 1);
			if(!$result['UF_TEXT'])
			{
				$result['ERROR'][] = 'невалидный стих!';
			}
		}
		return $result;
	}
	
	public function getChapter($book , $chapter)
	{
		$result = null;
		if(!$book){
			$result['ERROR'][] = 'Введите книгу!';
		}
		if(!$chapter){
			$result['ERROR'][] = 'Введите главу!';
		}
		if(!$result['ERROR'])
		{
			$bibleAll = new \Larshin\MainTools\HighLoad(Bible);
			$result = $Hg -> getList($this -> allSelect() , array('ID' => $id) , 'ID' , 'ASC' , 1);
			if(!$result['UF_TEXT'])
			{
				$result['ERROR'][] = 'невалидный стих!';
			}
		}
		return $result;
	}
}
?>