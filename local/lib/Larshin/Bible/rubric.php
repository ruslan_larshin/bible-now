<?php
namespace Larshin\Rubric;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Rubric
{
	public $id;
	public $name;
	public $code;
	public $verse;
	public $verseView;
	public $verseOne;
	
	public function allSelect()
	{
		return array('ID' , 'NAME' , 'PROPERTY_VERSES' , 'PROPERTY_VERSES_ONE' ,'PROPERTY_VERSES_VIEW' , 'CODE');
	}
	
	public function __construct()
    {
	view(1);
		return null;
    }
	
	public function getList()
	{
	view(1);
		$result = array();
		$res = \CIBlockElement::GetList(array("ID" => "ASC"), array('IBLOCK_ID' => bibleRubricIbId , 'ACTIVE' => 'Y' , 'INCLUDE_SUBSECTIONS' => 'Y') , false, false , $this -> allSelect());
		while ($data = $res->GetNextElement()) 
		{
			$rubricData = $data -> GetFields(); 
			
			$rubric = new \Larshin\rubric\rubric();
			$rubric -> id = $rubricData['ID'];
			$rubric -> name = $rubricData['NAME'];
			$rubric -> code = $rubricData['CODE'];
			$rubric -> verse = $rubricData['PROPERTY_VERSES_VALUE'];
			$rubric -> verseView = $rubricData['PROPERTY_VERSES_VIEW_VALUE'];
			$rubric -> verseOne = $rubricData['PROPERTY_VERSES_ONE_VALUE'];
			$result[] = $rubric;
		}
		return $result;
	}
}