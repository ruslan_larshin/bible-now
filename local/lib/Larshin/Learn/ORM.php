<?php	
namespace Nota\Entity;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type;
Loader::includeModule("highloadblock"); 

class ElementTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'b_iblock_element';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('IBLOCK_ELEMENT_ENTITY_ID_FIELD'),
            ),
            'IBLOCK_ID' => array(
                'data_type' => 'integer',
            ),
            'IBLOCK' => array(
                'data_type' => 'Iblock',
                'reference' => array('=this.IBLOCK_ID' => 'ref.ID'),
            ),	
		);
	}
}
?>