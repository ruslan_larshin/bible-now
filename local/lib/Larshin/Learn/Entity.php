<?php	
namespace Nota\Entity;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type;
Loader::includeModule("highloadblock"); 

class BookTable extends Entity\DataManager
{
	public static function onBeforeAdd(Entity\Event $event)//обработчик событий
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        if (isset($data['ISBNCODE']))
        {
            $cleanIsbn = str_replace('-', '', $data['ISBNCODE']);
            $result->modifyFields(array('ISBNCODE' => $cleanIsbn));
			echo '<pre>'; print_r($cleanIsbn); echo '</pre>';
        }
        if (isset($data['TITLE']))
        {
            $cleanIsbn =  $data['TITLE'] . ' Это поле изменено обьработчиком событий';
            $result->modifyFields(array('TITLE' => $cleanIsbn));
        }

        return $result;
    }
	
    public static function getTableName()
    {
        return 'my_book';
    }
    
    public static function getMap()
    {
        return array(
           new Entity\IntegerField('ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			new Entity\StringField('NAME'),
			new Entity\StringField('ISBNCODE', array(
				'required' => true,
				'validation' => function() {
					return array(
						/*function ($value) {
							$clean = str_replace('-', '', $value);
							
							if (preg_match('/^\d{13}$/', $clean))
							{
								return true;
							}
							else
							{
								return 'Код ISBN должен содержать 13 цифр.';
							}
						}*/
						 new Entity\Validator\RegExp('/\d{13}/'),
					);
				}
			)),
            new Entity\StringField('TITLE'),
			new Entity\DateField('PUBLISH_DATE', array(
				'default_value' => new Type\Date //значение по умолчанию
			)),
			new Entity\BooleanField('NAME', array(
				'values' => array('N', 'Y')
			)),
			new Entity\EnumField('NAME', array(
				'values' => array('VALUE1', 'VALUE2', 'VALUE3')
			))
        );
    }
}


?>