<?php
namespace Larshin\Pager;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class Pager
{
	public $pagers;
	
	public function __construct($pageCount,$pageNumber=1)
    {
		$result = array();
		if($pageCount <= 6){
			for($i=1;$i<=$pageCount;$i++)
			{
				$this -> pagers[] = new \larshin\Pager\PagerItem($i , $i);
			}
		}else{
			if($pageNumber <= 4)
			{
				for($i=1;$i<=$pageNumber+1;$i++)
					$this -> pagers[] = new \larshin\Pager\PagerItem($i , $i);			

				$this -> pagers[] = new \larshin\Pager\PagerItem('...' , ceil(($pageCount-$pageNumber+1)/2));
				$this -> pagers[] = new \larshin\Pager\PagerItem($pageCount , $pageCount);
			}else{
			
				$this -> pagers[] = new \larshin\Pager\PagerItem(1 , 1);
				$this -> pagers[] = new \larshin\Pager\PagerItem('...' , ceil(($pageNumber-2)/2));
				$this -> pagers[] = new \larshin\Pager\PagerItem($pageNumber - 1 , $pageNumber - 1);
				$this -> pagers[] = new \larshin\Pager\PagerItem($pageNumber , $pageNumber);
				
				if($pageNumber != $pageCount && ($pageCount-$pageNumber) > 3)
					$this -> pagers[] = new \larshin\Pager\PagerItem($pageNumber + 1 , $pageNumber + 1); 
				
				if(($pageCount-$pageNumber) <= 3)
				{
					for($i=$pageNumber+1;$i<=$pageCount;$i++)
						$this -> pagers[] = new \Larshin\Pager\PagerItem($i,  $i);
				}else{
					$this -> pagers[] = new \larshin\Pager\PagerItem('...' , ceil(($pageCount-$pageNumber-1)/2+$pageNumber));
					$this -> pagers[] = new \larshin\Pager\PagerItem($pageCount , $pageCount);
				}
			}
		}
		return $result;
    }
}

class PagerItem
{
	public $name;
	public $urlCode;
	
	public function __construct($name,$urlCode)
    {
		$this -> name = $name;
		$this -> urlCode = $urlCode;
		return null;
	}
}