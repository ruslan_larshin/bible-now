<?php
namespace Larshin\MainTools;  


use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity,
	Bitrix\Main\Type\DateTime;
	
class PropertyListElement
{
	public $listElement;
	public function __construct($enumId = null,$value = null)
    {
		if($value && $enumId)
		{
			$this->listElement = array(
				'enumId' => $enumId,
				'value' => $value,
			);
		}else{
			$this->listElement = null;
		}
		return $this;
    }
}

class PropertyList
{
	public $list;
	public function __construct($array = null)
    {
		if($array)
		{
			$result = null;
			foreach($array as $enumId=>$value)
			{
				if($enumId && $value)
				{
					$this->list[] = new \Nota\MainTools\PropertyListElement($enumId,$value);
				}
			}
		}else{
			//$this->lists[] = new \Nota\MainTools\PropertyListElement(0,null);
			$this->list = null;
		}
		return $result;
    }
	
}

class PropertyListHighLoad
{
	public $list;
	public function __construct($array = null)
    {
		if($array)
		{
			$result = null;
			foreach($array as $enumId=>$value)
			{
				if($value)
				{
					$this->list[] = new \Nota\MainTools\PropertyListElement($value,$value);
				}
			}
		}else{
			$result = null;
		}
		return $result;
    }
}

class Tools
{
	public function __construct()
    {
		global $APPLICATION;
		global $USER;
		if(!$USER->IsAuthorized())
		{
			//global $USER;
			//$USER->Authorize(409); // убрать!!! для теста удаленного апи
		}
		return true;
    }
	
	public function switcherRu($value)
	{
		$converter = array(
			'f' => 'а',	',' => 'б',	'd' => 'в',	'u' => 'г',	'l' => 'д',	't' => 'е',	'`' => 'ё',
			';' => 'ж',	'p' => 'з',	'b' => 'и',	'q' => 'й',	'r' => 'к',	'k' => 'л',	'v' => 'м',
			'y' => 'н',	'j' => 'о',	'g' => 'п',	'h' => 'р',	'c' => 'с',	'n' => 'т',	'e' => 'у',
			'a' => 'ф',	'[' => 'х',	'w' => 'ц',	'x' => 'ч',	'i' => 'ш',	'o' => 'щ',	'm' => 'ь',
			's' => 'ы',	']' => 'ъ',	"'" => "э",	'.' => 'ю',	'z' => 'я',					
	 
			'F' => 'А',	'<' => 'Б',	'D' => 'В',	'U' => 'Г',	'L' => 'Д',	'T' => 'Е',	'~' => 'Ё',
			':' => 'Ж',	'P' => 'З',	'B' => 'И',	'Q' => 'Й',	'R' => 'К',	'K' => 'Л',	'V' => 'М',
			'Y' => 'Н',	'J' => 'О',	'G' => 'П',	'H' => 'Р',	'C' => 'С',	'N' => 'Т',	'E' => 'У',
			'A' => 'Ф',	'{' => 'Х',	'W' => 'Ц',	'X' => 'Ч',	'I' => 'Ш',	'O' => 'Щ',	'M' => 'Ь',
			'S' => 'Ы',	'}' => 'Ъ',	'"' => 'Э',	'>' => 'Ю',	'Z' => 'Я',					
	 
			'@' => '"',	'#' => '№',	'$' => ';',	'^' => ':',	'&' => '?',	'/' => '.',	'?' => ',',
		);
	 
		$value = strtr($value, $converter);
		return $value;
	}
	
	public function convertDate($date)
	{
		$Month= array('0','янв','фев','мар','апр','мая','июн','июл','авг','сен','окт','ноя','дек');
		$result = ConvertDateTime($date, "DD", "ru") . ' ' . $Month[ConvertDateTime($date, "M", "ru")*1] . ' ' .  ConvertDateTime($date, "YYYY", "ru");
		return $result;
	}
	
	public function getFilePath($idImg) 
	{
		$result = null;
		if(!$idImg)
		{
			$result = null;
		}else{
			$url = \CFile::GetPath($idImg);
			if(!$url)
			{
				$result = null;
			}else{
				$result = $url;
			}
		}
		return $result;
	}
	
	public function getFileArrayPath($arIdImg) 
	{
		$result = null; 
		if($arIdImg)
		{
			foreach($arIdImg as $idImg)
			{
				$buf = $this->getFilePath($idImg);
				if($buf)
					$result[] = $buf;
			}
		}
		return $result;
	}
	
	public function getIdYoutube($url)
	{
		$link = null;
		if($url)
		{
			$link = str_replace('watch?v=','',$url);
			$link = explode('/',$link);
			$link = $link[count($link)-1];
		}
		if($link*1 != $link)
			$link = null;
		return $link;
	}
	
	public function arrayToList($array)
	{
		$result = null;
		foreach($array as $enumId=>$value)
		{
			if($enumId && $value)
			{
				$result[] = new \Nota\MainTools\PropertyListElement($enumId,$value);
			}
		}
		return $result;
	}
	
	public function updateHref($text)
	{
		$patt = array(
			'%\b(?<!href=[\'"])(?>https?://|www\.)([\p{L}\p{N}]+[\p{L}\p{N}\-]*\.(?:[\p{L}\p{N}\-]+\.)*[\p{L}\p{N}]{2,})(?::\d+)?(?:(?:(?:/[\p{L}\p{N}$_\.\+!\*\'\(\),\%;:@&=-]+)+|/)(?:\?[\p{L}\p{N}$_\.\+!\*\'\(\),\%;:@&=-]+)?(?:#[^\s\<\>]+)?)?(?![^<]*+</a>)%u',
			'%\b(?<!http://)(?<!https://)([\p{L}\p{N}]+[\p{L}\p{N}\-]*\.(?:[\p{L}\p{N}\-]+\.)*(?:ru|com|net))(?::\d+)?(?:(?:(?:/[\p{L}\p{N}$_\.\+!\*\'\(\),\%;:@&=-]+)+|/)(?:\?[\p{L}\p{N}$_\.\+!\*\'\(\),\%;:@&=-]+)?(?:#[^\s\<\>]+)?|\b)(?![^<]*+</a>)%u'
		);
		$repl = array(
			'<a target="_blank" href="$0">$1</a>',
			'<a target="_blank href="http://$0">$1</a>'
		);
		$text = preg_replace($patt, $repl, $text);
		return($text);
	}
	
	 public function translit($str) 
	 {
		$rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
		$lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
		$str=str_replace(array(' ','"','/','_','.',';','!','^',':','+',',','«',"»",'(',')','&','?',':','/','?','#','[',']','@','!','$','&',"'",'(',')','*','+',',',';','=','~','%','{','}','|','”','>','<','E','”','№','%','Y',"’",'|','\\'),'',$str);
		if(!$str)
			$str = 'codeNote';
		return str_replace($rus, $lat, $str);
	}
	
	public function unTranslit($str) 
	 {
		$rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
		$lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
		$str=str_replace(array('"','/','_','.',';','!','^',':','+',',','«',"»",'(',')','&','?',':','/','?','#','[',']','@','!','$','&',"'",'(',')','*','+',',',';','=','~','%','{','}','|','”','>','<','E','”','№','%','Y',"’",'|','\\'),'',trim($str));
		if(!$str)
			$str = '';
		return str_replace($lat, $rus, $str);
	}
	
	public function makeFilesArray($arr) 
	{
		$result = array();
		foreach ($arr['name'] as $key => $val) {
			$result[$key]['name'] = $val;
			$result[$key]['type'] = $arr['type'][$key];
			$result[$key]['tmp_name'] = $arr['tmp_name'][$key];
			$result[$key]['size'] = $arr['size'][$key];
		}

		$arFiles = array();
		foreach ($result as $item){
				$arFiles[] = array('VALUE' => $item); 
		}
		
		return $arFiles;
	}
	
	public function getParamsForGetList($arRequest)
	{
		$result = null;
		$nPageSize =  $arRequest['nPageSize'] ?? 10;
		$page =  $arRequest['page'] ?? 1;
		if($arRequest['all']){
			$nPageSize = 1000;
		}
		$result['arSort'] = array(($arRequest['order'] ?? 'ID') =>($arRequest['orderBy'] ?? 'DESC'));
		$result['arNav'] = array(
			'nPageSize'=>$nPageSize ,
			'iNumPage'=>$page,
		);
		$result['aFilter'] = array(); 
		if($arRequest['search'])
			$result['arFilter']['NAME'] = '%' . $arRequest['search'] . '%';
		if($arRequest['company'])
			$result['arFilter']['PROPERTY_COMPANY'] = $arRequest['company'];
		if($arRequest['premium'])
			$result['arFilter']['PROPERTY_PREMIUM_VALUE'] = $arRequest['premium'];
		return $result;
	}
	
	public function regUrl($url)
	{
		$result = null;
		if(preg_match(RegUrl,$url))
		{
			$result = true;
		}else{
			$result = false;
		}
		return $result;
	}
	
	public function regEmail($email)
	{
		if(preg_match(RegEmail,$email))
		{
			return true;
		}else{
			return false;
		}
	}
	
	function genPassword($length = 8)
	{				
		$chars = 'qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP'; 
		$size = strlen($chars) - 1; 
		$password = ''; 
		while($length--) {
			$password .= $chars[random_int(0, $size)]; 
		}
		return $password;
	}
	
	public function orderList($all = true)
	{
		$result = array(
			 array(
				'name' => 'по алфавиту',
				'value' => 'order:name=Y',
				'code' => 'name'
			),
			array(
				'name' => 'по популярности',
				'value' => 'order:popular=Y',
				'code' => 'popular'
			),
			array(
				'name' => 'по дате создания',
				'value' => 'order:date=Y',
				'code' => 'date'
			),
		);
		
		return $result;
	}
	
	public function mb_ucfirst($string, $encoding = 'UTF-8')  
	{
		$firstChar = mb_substr($string, 0, 1, $encoding);
		$then = mb_substr($string, 1, null, $encoding);
		return mb_strtoupper($firstChar, $encoding) . $then;
	}
	
	public function boldResult($search,$text)
	{
		$result = '';
		//$text = strip_tags($text);
		$text = str_replace('&nbsp;' , '', $text);
		$text = preg_replace('/\s+/', ' ', $text);
		/*$numFirst = mb_strpos($text,$search) ??  mb_strpos($text,mb_strtoupper($search)) ?? mb_strpos($text, $this -> mb_ucfirst($search));
		if(!$numFirst)
			$numFirst = mb_strpos($text,mb_strtoupper($search));	
		if(!$numFirst)
			$numFirst = mb_strpos($text, $this -> mb_ucfirst($search));
		if(!$numFirst)
			$numFirst = mb_strpos(mb_strtolower($text), mb_strtolower($search));*/

		$result = $text;
		$resultBuf = str_replace($search,'<mark>' . $search . '</mark>', $text); 
		if($resultBuf == $result)
		{
			$resultBuf = str_replace(mb_strtoupper($search),'<mark>' . mb_strtoupper($search) . '</mark>', $result); 
		}
		if($resultBuf == $result)
		{
			$resultBuf = str_replace($this->mb_ucfirst($search),'<mark>' . $this->mb_ucfirst($search) . '</mark>', $result);  
		}
		if($resultBuf == $result)
		{
			$resultBuf = str_replace(mb_strtolower($search),'<mark>' . mb_strtolower($search) . '</mark>', $result);  
		}
		$result = $resultBuf;
		return trim($result);
	}
	
	public function noTags($text)
	{
		$result = null;
		$result = str_replace(array('&nbsp;') , '' ,trim(strip_tags($text)));
		return $result;
	}

    function redirect($url) {
        ob_start();
        header('Location: '.$url);
        ob_end_flush();
        die();
    }
}

class api
{
	public function __construct()
    {
		return true;
    }
	
	public function viewJson($param)
	{
		if($param)
			echo json_encode($param, JSON_UNESCAPED_UNICODE);
	}
	
	public function getRequest()
	{
		$arRequest=array();
		$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		$postValues = $request->getPostList()->toArray(); 
		$getValues = $request->getQueryList()->toArray();
		$arRequest=array_merge($postValues,$getValues);
		return $arRequest;
	}
	
	public function viewResponceStatus($arError)
	{
		if($arError['ERROR'])
		{
			foreach($arError as $error)
			{
				/*$response = new \Bitrix\Main\HttpResponse();
				$response->addHeader('Content-Type', 'text/plain');
				$response->setContent($error); */
				http_response_code(400); 
			}
		}else{
			http_response_code(200);
		}
		
		return true;
	}
}

class HighLoad
{
	public $entityHgload;
	
	public function __construct($HgId)
    {
		try{
			$hlblock = HL\HighloadBlockTable::getById($HgId)->fetch(); 
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$this -> entityHgload = $entity->getDataClass();
		}catch (Exception $e){
			$result['ERROR'][] = $e->getMessage();
		}
		return $result;
    }
	
	public function add($data)
	{
		try{
			if($data)
			{
				$result = $this->entityHgload::add($data);
			}
		}catch (Exception $e){
			$result['ERROR'][] = $e->getMessage();
		}	
		return $result;
	}
	
	public function update($id , $data)
	{
		try{
			if($data && $id)
			{
				$result = $this->entityHgload::update($id , $data);
			}
		}catch (Exception $e){
			$result['ERROR'][] = $e->getMessage();
		}	
		return $result;
	}
	
	
	public function getList($select = array('UF_ID', 'UF_NAME') , $filter = array(),$order = 'ID', $orderBy = 'ASC' , $limit = 100000 , $offset = 0 , $page = 1)
	{
		$result = array();
		// подготавливаем данные
		$orderArray = array('ID' => 'ASC');
		if($order && $orderBy)
			$orderArray = array($order => $orderBy);
		$rsData = $this->entityHgload::getList(array(
			"select" => $select,
			"filter" => $filter,
			"limit" => $limit, //ограничим выборку пятью элементами
			"order" => $orderArray,
			'offset' => $offset,
			'count_total' => true,
		));
		//view($filter);
		$count = $rsData->getCount();
		$rsData = new \CDBResult($rsData); // записываем в переменную объект CDBResult
		while($arRes = $rsData->Fetch()){
			$result['items'][] = $arRes;
		}
		$result['pager']=array(
			'page' => $page,
			'count' => $count,
			'pageCount' => ceil($count/$limit)
		);
		
		return $result;
	}

    public function getAll($select = array('UF_ID', 'UF_NAME') , $name ,$id)
    {
        $result = array();
        $rsData = $this->entityHgload::getList(array(
            "select" => $select,
        ));
        $rsData = new \CDBResult($rsData); // записываем в переменную объект CDBResult
        while($arRes = $rsData->Fetch()){
            $result[$arRes[$name]] = $arRes[$id];
        }
        return $result;
    }
	
}


