<?
namespace Api;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Lingvo
{
  public const url = 'https://developers.lingvolive.com/';
  public const key = 'ZTU4NjZjMDgtY2E0ZS00YThlLWJjYzctM2ZkMDQ2ZjdkMjBjOjY3MzQ1YjU0OTUzZDRkMDU4ZGVkZDRkYWUyMGQxNjA2';

  public static function getLexems($word){
    if(empty($word)){
      return ['error' => 'Невалидные данные'];
    }
    $result = [];
    $word = trim($word);
    $lingvo = self::sendRequest($word)['result'];
    view($lingvo);
    if($lingvo == 'Слово не найдено для языка 1049'){
      return [ 'main' => $word, 'words' => []];
    }
    if($lingvo == 'Частота запросов превышает допустимые 50000 символов в сутки'){
      return ['error' => $lingvo];
    }
    $mainWord = false;
    if(empty($lingvo)){
      return ['error' => 'Ничего не найдено'];
    }
    foreach($lingvo as $var){
      if(!$mainWord) {
        $mainWord = $var['Lexem'];
      }
      foreach($var['ParadigmJson']['Groups'] as $item){
          foreach($item['Table'] as $keyLexems => $lexems){
            if($keyLexems > 0){
              foreach($lexems as $keyWords => $words){
                if($keyWords > 0){
                  $words['Value'] = trim(str_replace(['*',  ' ' , '.'], '', $words['Value']));
                  if(!empty($words['Value'])) {
                    $buf = explode(',', $words['Value']);
                    if(count($buf) > 1){
                      foreach($buf as $item){
                        $result[$item] = $item;
                      }
                    }else {
                      $result[$words['Value']] = $words['Value'];
                    }
                  }
                }
              }
            }
        }
      }
    }
    return ['words' => array_values($result), 'main' => $mainWord];
  }


  public static function sendRequest($word){
    $header = array();
    $header[] = 'Content-length: 0';
    $header[] = 'Content-type: application/json';
    $header[] = 'Authorization: Basic ' . self::key . '';
    $myCurl = curl_init();
    curl_setopt_array($myCurl, array(
      CURLOPT_URL => 'https://developers.lingvolive.com/api/v1.1/authenticate',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_HTTPHEADER => $header
    ));
    $bearer_token = curl_exec($myCurl);
    curl_close($myCurl);
    $header[2] = 'Authorization: Bearer ' . $bearer_token;

    $myCurl = curl_init();
    curl_setopt_array($myCurl, array(
      CURLOPT_URL => "https://developers.lingvolive.com/api/v1/WordForms?text={$word}&lang=1049",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HTTPHEADER => $header
    ));
    $response = curl_exec($myCurl);
    curl_close($myCurl);

    if($e = curl_error($myCurl)) {
      curl_close($myCurl);
      return ['error' => $e];
    } else {
      curl_close($myCurl);
      return ['result' => json_decode($response, true)];
    }
  }
}