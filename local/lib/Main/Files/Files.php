<?php

namespace Main;

class Files
{
  public static function getFilesResize($idImg, $size = [])
  {
    try {
      if (empty($idImg)) {
        return ['error' => 'Необходим идентификатор'];
      }
      $size['height'] = intval($size['height']);
      $size['width'] = intval($size['width']);
      if (!empty($size['width']) && !empty($size['height'])) {
        $resize = \CFile::ResizeImageGet(
          $idImg,
          [
            "width" => $size['width'],
            "height" => $size['height']
          ],
          BX_RESIZE_IMAGE_PROPORTIONAL,
          true,
        );
        $file = (intval($resize['width']) > 0) ? $resize['src'] : false;
      }
      else {
        $file = \CFile::GetPath($idImg) ?? '\local\img\noPhotoSmall.jpg';
      }
      return ['src' => $file];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex->getMessage()];
    }
  }

  public static function getFilesResizeArray($idsImg, $size = [])
  {
    try {
      $files = [];
      foreach ($idsImg as $idImg) {
        $files[] = self::getFilesResize($idImg, $size)['src'];
      }
      return ['files' => $files];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex->getMessage()];
    }
  }

  public static function getFilesProperty($idsImg)
  {
    try {
      $files = [];
      foreach ($idsImg as $idImg) {
        $files[] = self::getFileProperty($idImg)['file'];
      }
      return ['files' => $files];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex->getMessage()];
    }
  }

  public static function getFileProperty($idImg)
  {
    try {
      $arFile = \CFile::GetFileArray($idImg);
      $file['name'] = $arFile['ORIGINAL_NAME'];
      $file['size'] = $arFile['FILE_SIZE'];
      $file['src'] = $arFile['SRC'];
      $file['id'] = $idImg;
      return ['file' => $file];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex->getMessage()];
    }
  }

  public static function getFileArrays($imgsPath)
  {
    try {
      $result = [];
      foreach ($imgsPath as $path) {
        $result[] = \CFile::MakeFileArray($path);
      }
      return ['files' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex->getMessage()];
    }
  }

  public static function pdfPreview($path, $name = 'pdf', $dir = '/local/docs/')
  {
    try {
      $img = new Imagick($path);
      $img->setImageFormat('jpg');
      $img->setImageBackgroundColor('white');
      $img->setResolution(300, 300);
      $img->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
      $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
      $img->setImageAlphaChannel(Imagick::VIRTUALPIXELMETHOD_WHITE);
      $num_pages = $img->getNumberImages();
      $result = [];
      $img->setImageCompressionQuality(100);
      for ($i = 0; $i < $num_pages; $i++) {
        $img->setIteratorIndex($i);
        $img->setImageFormat('jpg');
        $img->setImageBackgroundColor('white');
        $img->setResolution(300, 300);
        //$img->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
        $img->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
        $img->setImageAlphaChannel(Imagick::VIRTUALPIXELMETHOD_WHITE);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . $dir . $name . '-' . $i . '.jpg', $img);
        $result[] = $dir . $name . '-' . $i . '.jpg';
      }
      //file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/local/docs/'. $name . '.jpg', $im);
      $img->clear();
    } catch (ImagickException $ex) {
      return ['error' => $ex->getMessage()];
    }
    return ['imgs' => $result];
  }

  public static function formatSizeUnits($bytes)
  {
    if ($bytes >= 1073741824) {
      $bytes = number_format($bytes / 1073741824, 2) . ' Гб';
    }
    elseif ($bytes >= 1048576) {
      $bytes = number_format($bytes / 1048576, 2) . ' Мб';
    }
    elseif ($bytes >= 1024) {
      $bytes = number_format($bytes / 1024, 2) . ' Кб';
    }
    elseif ($bytes > 1) {
      $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1) {
      $bytes = $bytes . ' byte';
    }
    else {
      $bytes = '0 bytes';
    }
    return $bytes;
  }
}