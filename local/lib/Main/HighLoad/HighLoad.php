<?
namespace Main;

\CModule::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class HighLoad
{
  public static function init($hlId){
    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlId)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    return $entity->getDataClass();
  }

  public static function getListEmpty($hlId, $map, $request = [])
  {
    try {
      $select = array_values($map);
      if(!empty($request['select'])){
        $select = [];
        foreach($request['select'] as $code){
          $select[] = $map[$code] ?? $code;
        }
        $select = $select ?: ['ID'];
        foreach($map as $keyMap => $code){
          if(!in_array($code, $select ?? [])){
            unset($map[$keyMap]);
          }
        }
      }
      if($request['cacheTime']){
        $cacheTime = $request['cacheTime'];
        unset($request['cacheTime']);
        $result = \Main\Cache::getIblock('\Main\HighLoad::getListEmpty', $hlId, $map, $request, $request, $cacheTime)['items'];
        return ['items' => $result];
      }

      $rsData = self::init($hlId)::getList([
        'order' => $request['order'] ?? $request['sort'] ?? [],
        "select" => $select,
        "filter" => $request['filter'] ?? [],
        'limit' => $request['nav']['limit'] ?? $request['limit'] ?? 0,
        'offset' => $request['nav']['offset'] ??  $request['offset'] ?? 0,
      ]);

      $rsData = new \CDBResult($rsData);
      while ($arRes = $rsData->Fetch()) {
        $obj = [];
        foreach($map as $code => $codeBase){
          $obj[$code] = $arRes[$codeBase];
        }
        $result[$arRes[$request['key']] ?? $arRes['ID']] = $obj;
      }
      return ['items' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function updateEmpty($elId,$hlId, $map, $data)
  {
    try {
      if(empty($elId) || empty($data)){
        return ['error' => 'HL updateEmpty => невалидные данные'];
      }
      foreach($data as $key => $value){
        if(!empty($map[$key])){
          $data[$map[$key]] = $value;
          unset($data[$key]);
        }
      }
      $mapFlip = array_flip($map);
      foreach($data as $key => $value){
        if(empty($mapFlip[$key])){
          unset($data[$key]);
        }
      }
      $result = self::init($hlId)::update($elId , $data);
      $result = $result -> isSuccess() ?: $result -> getErrors();
      return ['update' => $result, 'data' => $data, 'id' => $elId];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }
}
