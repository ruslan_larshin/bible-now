<?php
namespace Main;

class Pager
{

  public static function get($pageCount, $pageNumber = 1){
    $result = [];
    if($pageCount <= 6){
      for($i=1;$i<=$pageCount;$i++)
      {
        $result['pagers'][] = ['code' => $i, 'urlCode' => $i];
      }
    }else{
      if($pageNumber <= 4)
      {
        for($i=1;$i<=$pageNumber+1;$i++){
          $result['pagers'][] = ['code' => $i, 'urlCode' => $i];
        }
        $result['pagers'][] = ['code' => '...', 'urlCode' => ceil(($pageCount-$pageNumber+1)/2)];
        $result['pagers'][] = ['code' => $pageCount, 'urlCode' => $pageCount];
      }else{
        $result['pagers'][] = ['code' => 1, 'urlCode' => 1];
        $result['pagers'][] = ['code' => '...', 'urlCode' => ceil(($pageNumber-2)/2)];
        $result['pagers'][] = ['code' => $pageNumber - 1, 'urlCode' => $pageNumber - 1];
        $result['pagers'][] = ['code' => $pageNumber, 'urlCode' => $pageNumber];

        if($pageNumber != $pageCount && ($pageCount-$pageNumber) > 3){
          $result['pagers'][] = ['code' => $pageNumber + 1, 'urlCode' => $pageNumber + 1];
        }

        if(($pageCount-$pageNumber) <= 3)
        {
          for($i=$pageNumber+1;$i<=$pageCount;$i++){
            $result['pagers'][] = ['code' => $i, 'urlCode' => $i];
          }
        }else{
          $result['pagers'][] = ['code' => '...', 'urlCode' => ceil(($pageCount-$pageNumber-1)/2+$pageNumber)];
          $result['pagers'][] = ['code' => $pageCount, 'urlCode' => $pageCount];
        }
      }
    }
    return $result;
  }
}
