<?php

namespace Main;

class Captcha
{
  private const default = [
    'width' => 300,
    'height' => 75,
    'length' => 5,
  ];

  public static function load(){
    try{
      $cptcha = new \CCaptcha();
      $cptcha->SetImageSize(self::default['width'], self::default['height']);
      $cptcha->SetCodeLength(self::default['length']);
      $cptcha->SetWaveTransformation(true);
      $cptcha->SetCode();
      $code = $cptcha->GetSID();
      return ['captcha' => ['code' => $code]];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function valid($captchaInput, $captchaCode){
    if(empty($captchaInput) || empty($captchaCode)){
      return ['error' => 'Пустые данные'];
    }
    global $APPLICATION;
    if(!$APPLICATION->CaptchaCheckCode($captchaInput, $captchaCode)) {
      return ['error' => 'Неверно введены символы!'];
    } else {
      return ['success' => 'Проверка пройдена!'];
    }
  }
}