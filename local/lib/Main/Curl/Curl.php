<?
namespace Main;

\Bitrix\Main\Loader::IncludeModule('iblock');
\Bitrix\Main\Loader::includeModule("highloadblock");

class Curl
{
  public static function getCurlJson($url, $type ='GET', $body = [], $header = ''){
    $curl = curl_init();
    if(!empty($body)) {
      if($type == 'POST') {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body, JSON_UNESCAPED_UNICODE));
      }else{
        $url = $url . '?' . http_build_query($body);
      }
    }
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
    if(!empty($header)){
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $header ));
      //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    }
    $response = curl_exec($curl);
    if($e = curl_error($curl)) {
      curl_close($curl);
      return ['error' => $e];
    } else {
      curl_close($curl);
      return ['result' => json_decode($response, true)];
    }
  }

}