<?php

namespace Main;

class User
{
  public $userId;
  private const mainLogin = 'MainUser';

  public function __construct($userId = null)
  {
    global $USER;
    $this->userId = $userId ? $userId : $USER->getID();
  }

  public static function getMap()
  {
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'lastName' => 'LAST_NAME',
      'secondName' => 'SECOND_NAME',
      'login' => 'LOGIN',
      'photo' => 'PERSONAL_PHOTO',
      'email' => 'EMAIL',
      'password' => 'PASSWORD',
      'recoveryCode' => 'UF_RECOVERY_CODE',
      'dateRecoveryCode' => 'UF_DATE_RECOVERY',
    ];
  }

  public static function getId()
  {
    global $USER;
    return intval($USER->getID());
  }

  public static function getUsersByGroup($groupId)
  {
    try {
      $result = [];
      if (empty($groupId)) {
        return ['error' => ["Необходим идентификатор группы!"]];
      }
      $dbUser = \Bitrix\Main\UserTable::getList([
        'select' => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'LOGIN', 'WORK_POSITION',
          'PERSONAL_PHOTO', 'EMAIL','PERSONAL_PHONE'],
        'filter' => ['GROUPS.GROUP_ID' => $groupId],
      ]);

      while ($arUser = $dbUser->fetch()) {
          $result[] = intval($arUser['ID']);
      }
      return ['users' => $result];
    } catch (Exception $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function getById($userId)
  {
    try {
      $result = [];
      if (empty($userId)) {
        return ['error' => "Необходим идентификатор пользователя!"];
      }
      $dbUser = \Bitrix\Main\UserTable::getList([
        'select' => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'LOGIN', 'WORK_POSITION', 'PERSONAL_PHOTO', 'EMAIL'],
        'filter' => ['ID' => $userId],
        'limit' => 1,
      ]);
      if ($arUser = $dbUser->fetch()) {
        return ['user' =>
          [
            'id' => intval($arUser['ID']),
            'name' => $arUser['NAME'],
            'lastName' => $arUser['LAST_NAME'],
            'secondName' => $arUser['SECOND_NAME'],
            'login' => $arUser['LOGIN'],
            'position' => $arUser['WORK_POSITION'],
            'photo' => [
            'main' => \CFile::GetPath($arUser['PERSONAL_PHOTO']),
            'small' => \CFile::ResizeImageGet(
              $arUser['PERSONAL_PHOTO'],
                ['width' => 250, 'height' => 250],
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true
              )['src'],
            ],
            'email' => $arUser['EMAIL'],
          ]
        ];
      }
      return ['error' => 'Пользователь не найден'];
    } catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }


  public static function add($request)
  {
    try {
      $arFields = [
        "NAME" => $request['name'],
        "LAST_NAME" => $request['lastName'],
        "SECOND_NAME" => $request['secondName'],
        "EMAIL" => $request['email'] ?? $request['login'] . '@vitamax.ru',
        "PERSONAL_PHONE" => $request['phone'],
        "LOGIN" => $request['login'],
        "ACTIVE" => "Y",
        "GROUP_ID" => [2, 9],
        "PASSWORD" => $request['password'],
        "CONFIRM_PASSWORD" => $request['password'],
      ];
      $user = new \CUser;
      $id = $user->Add($arFields);
      if (!empty($id)) {
        return ['id' => $id];
      }
      return ['error' => $user->LAST_ERROR];
    } catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function setPhoto($file){
    try{
      if(empty($file)){
        return ['error' => 'Файл пуст'];
      }
      $userId = \Vitamax\User::getId();
      if(empty($userId)){
        return ['error' => 'Авторизуйтесь'];
      }
      $user = new \CUser;
      $fields = [
        "PERSONAL_PHOTO" => $file,
      ];
      $result = $user->Update($userId, $fields);
      if(!empty($user->LAST_ERROR)){
        return ['error' => $user->LAST_ERROR];
      }else{
        return ['success' => true, 'result' => $result];
      }

    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function update($request)
  {
    try {
      if(empty($request['name'])){
        return ['error' => 'Имя не может быть пустым'];
      }
      if(empty($request['lastName'])){
        return ['error' => 'Фамилия не может быть пустой'];
      }
      if(empty($request['login'])){
        return ['error' => 'Login не может быть пустым'];
      }
      if(empty($request['email'])){
        return ['error' => 'Email не может быть пустым'];
      }
      if(!\Vitamax\Utils::validation('email', $request['email'])['result']){
        return ['error' => 'Невалидный email'];
      }
      if(empty($request['phone'])){
        return ['error' => 'Телефон не может быть пустым'];
      }
      if((empty($request['password']) && !empty($request['passwordConfirm'])) || (!empty($request['password']) && empty($request['passwordConfirm'])) ){
        return ['error' => 'Неправильные пара: пароль, подтверждение'];
      }

      $arFields = [
        "NAME" => $request['name'],
        "LAST_NAME" => $request['lastName'],
        "SECOND_NAME" => $request['secondName'],
        "EMAIL" => $request['email'],
        "PERSONAL_PHONE" => $request['phone'],
        "LOGIN" => $request['login'],
      ];
      if(!empty($request['password'])){
        $arFields['PASSWORD'] = $request['password'];
        $arFields['CONFIRM_PASSWORD'] = $request['passwordConfirm'];
      }
      $userId = self::getId();
      if(empty($userId)){
        return ['error' => 'Авторизуйтесь!'];
      }
      $user = new \CUser;
      $id = $user->Update($userId, $arFields);
      if (!empty($id)) {
        return ['id' => $id];
      }
      return ['error' => $user->LAST_ERROR];
    } catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function updateField($userId, $field, $value)
  {
    $access = ['NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHONE', 'EMAIL'];
    if(empty($userId) || !In_array($field, $access) || empty($value)){
      return ['error' => 'Невалидные данные'];
    }
    try {
      if($field == 'EMAIL') {
        if (!\Vitamax\Utils::validation('email', $value)) {
          return ['error' => 'Невалидный email'];
        }
      }
      $arFields = [
        $field => $value,
      ];
      $user = new \CUser;
      $id = $user->Update($userId, $arFields);
      if (!empty($id)) {
        return ['id' => $id];
      }
      return ['error' => $user->LAST_ERROR];
    } catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function getUrlRecoveryPassword($email){
    try{
      $result = [];
      $dbUser = \Bitrix\Main\UserTable::getList([
        'select' => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'LOGIN', 'WORK_POSITION', 'PERSONAL_PHOTO', 'EMAIL',
          'UF_DATE_RECOVERY', 'UF_RECOVERY_CODE'],
        'filter' => [
           [
            "LOGIC" => "OR",
              ['LOGIN' => $email],
              ['EMAIL' => $email],
          ],
        ],
        'limit' => 1,
      ]);
      if ($arUser = $dbUser->fetch()) {
        $recoveryCode = \Vitamax\Utils::randomPassword();
        $user = new \CUser;
        $fields =[
          "UF_RECOVERY_CODE" => $recoveryCode,
          "UF_DATE_RECOVERY" => new \Bitrix\Main\Type\DateTime(),
        ];
        $data = [
          'name' => $arUser['NAME'] . ' ' . $arUser['LAST_NAME'],
          'login' => $arUser['LOGIN'],
          'code' => $recoveryCode,
          'email' => $arUser['EMAIL'],
          'userId' => $arUser['ID'],
          'date' => $fields['UF_DATE_RECOVERY'],
          'url' => \Service\Settings::siteUrl . \Service\Settings::urls['recoveryPassword'] . '?userId=' . $arUser['ID'] . '&code=' . $recoveryCode,
        ];
        $result['update'] = $user -> Update($arUser['ID'], $fields);
        if (!empty( $result['update'])) {
          $result['mail'] = \Mail\Mail::sendB2bRecoveryPassword($data);
          return ['success' => $result];
        }
        return ['error' => $user->LAST_ERROR];
      }
      return ['error' => 'Такой пользователь не зарегистрирован в системе'];
    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function changePassword($request){
    try{
      if(empty($request['userId'])){
        return ['error' => 'Необходим идентификатор пользователя'];
      }
      if(empty($request['code'])){
        return ['error' => 'Необходим секретный код'];
      }
      if(empty($request['password'])){
        return ['error' => 'Необходим пароль'];
      }
      if(empty($request['confirmPassword'])){
        return ['error' => 'Необходимо подтверждение пароля'];
      }
      if($request['confirmPassword'] != $request['password']){
        return ['error' => 'Пароли не совпадают'];
      }
      $access = self::accessChangePassword($request['userId'], $request['code']);
      if(!empty($access['error'])){
        return $access;
      }
      $arFields = [
        "PASSWORD" => $request['password'],
        "CONFIRM_PASSWORD" => $request['confirmPassword'],
        "UF_RECOVERY_CODE" => '',
      ];
      $user = new \CUser;
      $id = $user->Update($request['userId'], $arFields);
      if (empty($id)) {
        return ['error' => $user->LAST_ERROR];
      }
      return ['success' => 'Пароль успешно изменен'];
    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function accessChangePassword($userId, $code){
    try{
      if(empty($userId)){
        return ['error' => 'Необходим идентификатор пользователя'];
      }
      $dbUser = \Bitrix\Main\UserTable::getList([
        'select' => ['ID', 'NAME', 'UF_DATE_RECOVERY', 'UF_RECOVERY_CODE'],
        'filter' => ['ID' => $userId],
        'limit' => 1,
      ]);
      if ($arUser = $dbUser->fetch()) {
        if($arUser['UF_RECOVERY_CODE'] == $code){
          return ['access' => true];
        }else{
          return ['error' => 'Код восстановления недействителен'];
        }
      }
      return ['error' => 'Пользователь не найден'];
    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function getGroups($userId){
    try{
      if(empty($userId)){
        return ['error' => 'Необходим идентификатор пользователя'];
      }
      $arGroups = \CUser::GetUserGroup($userId);
      return ['groups' => $arGroups];
    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function accesGroup($arGroupAccess = [1]){
    try{
      $arGroups = \CUser::GetUserGroup(self::getId());
      foreach($arGroups as $group){
        if(in_array($group, $arGroupAccess)){
          return ['access' => true];
        }
      }
      return ['access' => false];
    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

  public static function accesGroupCode($arGroupAccess = ['admin']){
    try{
      $groups = [];
      foreach($arGroupAccess as $group){
        $groups[] = self::group[$group];
      }
      return self::accesGroup($groups);
    }catch (\Bitrix\Main\SystemException $e) {
      return ['error' => [$e->getMessage()]];
    }
  }

}
