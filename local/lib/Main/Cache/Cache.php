<?

namespace Main;

\CModule::IncludeModule('iblock');

class Cache
{
  public const cacheTime = 3600;

  public static function get($func, $args = [], $options = [], $cacheTime = 3600){
    $result = [];
    $cache = \Bitrix\Main\Data\Cache::createInstance();
    if ($cache->initCache($cacheTime ?? self::cacheTime, json_encode($options))) {
      $result = $cache->getVars();
    }
    elseif ($cache->startDataCache()) {
      $result = $func($args);
      $cache->endDataCache($result);
    }
    return $result;
  }

  public static function getIblock($func, $iblockCode, $getMap, $request , $options = [], $cacheTime = 3600){
    $result = [];
    $cache = \Bitrix\Main\Data\Cache::createInstance();
    if ($cache->initCache($cacheTime ?? self::cacheTime, json_encode($options))) {
      $result = $cache->getVars();
    }
    elseif ($cache->startDataCache()) {
      $result = $func($iblockCode, $getMap, $request);
      $cache->endDataCache($result);
    }
    return $result;
  }
}