<?

namespace Main;

\CModule::IncludeModule('iblock');

class Iblock
{
  public const special = ['NAME', 'SORT', 'ID', 'MODIFIED_BY', 'ACTIVE', 'CODE', 'SORT', 'IBLOCK_SECTION_ID',
    'DATE_CREATE', 'DETAIL_TEXT', 'PREVIEW_TEXT'];

  public static function getMap()
  {
    return [];
  }

  public static function select($map)
  {
    return array_values($map);
  }

  public static function getFieldForAddUpdate($iblockCode, $map, $request)
  {
    $result = [
      "MODIFIED_BY" => \Main\User::getId(),
      "IBLOCK_ID" => self::getIdByCode($iblockCode)['id'],
    ];
    foreach ($map as $code => $property) {
      if (!empty($request[$code])) {
        if (in_array($property, self::special)) {
          $result[$property] = $request[$code];
        }
        else {
          $propCode = str_replace('PROPERTY_', '', $property);
          $result['PROPERTY_VALUES'][$propCode] = $request[$code];
        }
      }
    }
    return $result;
  }
  public static function getListFormatEmpty($iblockCode, $map, $request = [], $format = [])
  {
    try {
      $items = [];
      $list = self::getListEmpty($iblockCode, $map, $request)['items'];
      if(empty($format)){
        return ['error' => 'Пустой массив форматированного вывода'];
      }
      foreach($list as $keyList => $item) {
        $result = [];
        foreach ($format as $keyGroup => $group) {
          if (empty($request['format']) || in_array($keyGroup, $request['format'] ?? [])) {
            $result[$keyGroup] = [];
            foreach ($group as $keyProperty => $codeBB) {
              if (empty($request['select']) || in_array($keyProperty, $request['select'] ?? [])) {
                $result[$keyGroup][$keyProperty] = $item[$keyProperty];
              }
            }
            if (empty($result[$keyGroup])) {
              unset($result[$keyGroup]);
            }
          }
        }
        $items[$keyList] = $result;
      }
      return ['items' => $items];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }


  public static function deleteEmpty($idElement, $iblockCode)
  {
    try {
      if (empty($idElement)) {
        return ['error' => 'Невалидные данные'];
      }
      $element = self::getListEmpty($iblockCode, ['id' => 'ID'], ['filter' => ['ID' => intval($idElement)], 'limit' => 1])['items'][0];
      if(!empty($element['id'])){
        \CIBlockElement::Delete($idElement);
      }
      return true;

    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }


  public static function updateEmpty($idElement, $iblockCode, $map, $request)
  {
    try {
      if (empty($idElement)) {
        return ['error' => 'Невалидные данные'];
      }
      $el = new \CIBlockElement;
      $fields = self::getFieldForAddUpdate($iblockCode, $map, $request);
      $property = $fields['PROPERTY_VALUES'];
      unset($fields['PROPERTY_VALUES']);
      if (!empty($property)) {
        \CIBlockElement::SetPropertyValuesEx($idElement, $iblockCode, $property);
      }
      $newId = $el->Update($idElement, $fields);
      if (!empty($newId)) {
        return ['success' => true, 'id' => $newId, 'data' => $fields, 'property' => $property, 'method' => 'update'];
      }
      else {
        return ['error' => $el->LAST_ERROR];
      }
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function addEmpty($iblockCode, $map, $request, $doubleControl = false)
  {
    try {
      if (empty($request['name'])) {
        return ['error' => 'Невалидные данные'];
      }
      if($doubleControl){
        $item = self::getListEmpty($iblockCode, $map, [
          'filter' => [
            'NAME' => $request['name'] ?? $request['NAME'],
          ],
          'nav' => ['limit' => 1],
        ])['items'];
        if(!empty($item)){
          return ['error' => ['text' => 'Дубль записи!','item' => $item] ];
        }
      }
      $el = new \CIBlockElement;
      $fields = self::getFieldForAddUpdate($iblockCode, $map, $request);
      $newId = $el->Add($fields);
      if(!empty($newId)){
        return ['success' => true, 'id' => $newId, 'data' => $fields, 'method' => 'add'];
      }else{
        return ['error' => $el->LAST_ERROR];
      }
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getListEmpty($iblockCode, $getMap, $request = [])
  {
    try {
      $request['sort'] = $request['sort'] ?? $request['order'];
      if ($request['sort']) {
        foreach ($request['sort'] as $sortKey => $sort) {
          if (!empty($sort) && in_array($sort, ['asc', 'desc', 'rand'])) {
            $sort = strtoupper($sort);
            $request['sort'][$sortKey] = $sort;
          }
          if (!empty($sort) && !in_array($sort, ['ASC', 'DESC', 'RAND'])) {
            unset($request['sort'][$sortKey]);
          }
          if (!empty($sortKey) && !empty($getMap[$sortKey])) {
            $request['sort'][$getMap[$sortKey]] = $sort;
            unset($request['sort'][$sortKey]);
          }
        }
      }

      if(!empty($request['group']) && !empty($getMap[$request['group']])){
        if(!empty($request['select'])){
          return ['error' => 'Нельзя указывать группировку и селект'];
        }
        if(!empty($request['nav'])){
          return ['error' => 'Нельзя указывать группировку и навигацию'];
        }
        if(!empty($request['sort'])){
          return ['error' => 'Нельзя указывать группировку и sort'];
        }
        $getMap = [$request['group'] => $getMap[$request['group']]];
      }
      if(!empty($request['select'])){
        foreach($getMap as $code => $propName){
          if(!in_array($code, $request['select'] ?? [])){
            unset($getMap[$code]);
          }
        }
      }
      $nav = [];
      if(!empty($request['nav']['limit']) || !empty($request['limit'])){
        $nav['nTopCount'] = $request['nav']['limit'] ?: $request['limit'];
      }
      if(!empty($request['nav']['offset'])  || !empty($request['offset'])){
        $nav['nOffset'] = $request['nav']['offset'] ?: $request['offset'];
      }
      if(!empty($request['filter'])) {
        foreach ($request['filter'] as $code => $filter) {
          if(!empty($getMap[$code])){
            $request['filter'][$getMap[$code]] = $filter;
          }
        }
      }
      //view($request);
      $request['filter']['IBLOCK_CODE'] = $iblockCode;

      if($request['cacheTime']){
        $cacheTime = $request['cacheTime'];
        unset($request['cacheTime']);
        $result = \Main\Cache::getIblock('\Main\IBlock::getListEmpty', $iblockCode, $getMap, $request, $request, $cacheTime)['items'];
        return ['items' => $result];
      }
      $res = \CIBlockElement::GetList(
        $request['sort'] ?? [],
        $request['filter'] ?? [],
        !empty($request['group']) ? [$getMap[$request['group']] ?? $request['group']] : false,
        $nav ?: false,
        empty($request['group']) ? self::select($getMap) : false
      );
      $result = [];
      while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        if(!empty($request['key']) && !empty($arFields[$request['key']])) {
          $result[$arFields[$request['key']]] = self::formatFields($arFields, $iblockCode, $getMap);
        }else{
          if(empty($request['group'])) {
            $result[] = self::formatFields($arFields, $iblockCode, $getMap);
          }else{
            $result[$arFields['CNT']] = $arFields[($getMap[$request['group']] ?: $request['group']) . '_VALUE'];
          }
        }
      }
      return ['items' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function formatFields($arFields, $iblockCode, $fields)
  {
    $properties = self::getProperties($iblockCode);
    $result = [];
    foreach ($fields as $code => $property) {
      if (in_array($property, self::special)) {
        $result[$code] = $arFields[$property];
      }
      else {
        $propCode = str_replace('PROPERTY_', '', $property);
        $property = $property . '_VALUE';
        if ($properties[$propCode]['type'] == 'F') {
          if ($properties[$propCode]['multiply']) {
            $result[$code] = \Main\Files::getFilesProperty($arFields[$property])['files'];
          }
          else {
            $result[$code] = \Main\Files::getFileProperty($arFields[$property])['file'];
          }
        }
        elseif (($properties[$propCode]['type'] == 'S') && ($properties[$propCode]['typeName'] == 'HTML')) {
          $result[$code] = $arFields['~' . $property]['TEXT'];
        }
        else {
          $result[$code] = $arFields[$property];
        }
      }

    }
    return $result;
  }

  public static function getProperties($iblockCode)
  {
    try {
      $result = [];
      $properties = \CIBlockProperty::GetList(
        [],
        [
          'IBLOCK_CODE' => $iblockCode,
        ]
      );
      while ($propFields = $properties->GetNext()) {
        if (empty($result[$propFields['CODE']])) {
          $result[$propFields['CODE']] = [
            'id' => $propFields['ID'],
            'name' => $propFields['NAME'],
            'code' => $propFields['CODE'],
            'type' => $propFields['PROPERTY_TYPE'],
            'typeName' => $propFields['USER_TYPE'],
            'multiply' => ($propFields['MULTIPLE'] === "Y") ? true : false,
          ];
        }
      }
      return $result;
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getIdByCode($ibCode)
  {
    try {
      if (empty($ibCode)) {
        return 0;
      }
      $res = \CIBlock::GetList(
        [],
        [
          'CODE' => $ibCode,
        ],
        true
      );
      if ($arRes = $res->Fetch()) {
        return ['id' => intval($arRes['ID'])];
      }
      return 0;
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getByName($name, $ibCode)
  {
    try {
      if (empty($name) || empty($ibCode)) {
        return ['error' => 'Невалидные данные'];
      }
      $res = \CIBlockElement::GetList(
        [],
        [
          'NAME' => $name,
          'IBLOCK_CODE' => $ibCode
        ],
        false,
        false,
        ['ID', 'NAME']
      );
      if ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        return [
          'id' => intval($arFields['ID']),
          'name' => $arFields['NAME'],
        ];
      }
      return ['error' => 'Не найден'];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getByCode($code, $ibCode)
  {
    try {
      if (empty($code) || empty($ibCode)) {
        return ['error' => 'Невалидные данные'];
      }
      $res = \CIBlockElement::GetList(
        [],
        [
          'CODE' => $code,
          'IBLOCK_CODE' => $ibCode
        ],
        false,
        false,
        ['ID', 'NAME']
      );
      if ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        return [
          'id' => intval($arFields['ID']),
          'name' => $arFields['NAME'],
        ];
      }
      return ['error' => 'Не найден'];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getSectionId($sectionCode, $ibCode)
  {
    try {
      $dbList = \CIBlockSection::GetList([], ['CODE' => $sectionCode, 'IBLOCK_CODE' => $ibCode], false, ['ID', 'NAME', 'CODE']);
      while ($arResult = $dbList->GetNext()) {
        return ['id' => intval($arResult['ID'])];
      }
      return ['error' => 'Не найден'];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getSectionList($ibCode)
  {
    try {
      $result = [];
      $dbList = \CIBlockSection::GetList([], ['IBLOCK_CODE' => $ibCode], false, ['ID', 'NAME', 'CODE']);
      while ($arResult = $dbList->GetNext()) {
        $result[$arResult['CODE']] = [
          'id' => intval($arResult['ID']),
          'code' => $arResult['CODE'],
          'name' => $arResult['NAME'],
        ];

      }
      if (!empty($result)) {
        return ['list' => $result];
      }
      return ['error' => 'Не найден'];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getPropertyEnumList($iblocId, $propertyCode)
  {
    try {
      $property_enums = \CIBlockPropertyEnum::GetList(
        [],
        [
          "IBLOCK_ID" => $iblocId,
          "CODE" => $propertyCode
        ]
      );
      $result = [];
      while ($enum_fields = $property_enums->GetNext()) {
        $result[$enum_fields["XML_ID"]] = [
          'id' => intval($enum_fields["ID"]),
          'code' => $enum_fields["XML_ID"],
          'name' => $enum_fields["VALUE"],
          'codeFilter' => 'PROPERTY_' . $propertyCode,
        ];
      }
      return ['items' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function getSections($ibCode)
  {
    try {
      $rsSections = \CIBlockSection::GetList(
        [
          'SORT' => 'ASC',
          'ID' => 'ASC'
        ],
        [
          'IBLOCK_CODE' => $ibCode,
          'ACTIVE' => 'Y',
        ]
      );
      $result = [];
      while ($arSection = $rsSections->Fetch())
      {
        $result[intval($arSection['ID'])] = [
          'id' => intval($arSection['ID']),
          'code' => $arSection['CODE'],
          'name' => $arSection['NAME'],
        ];
      }
      return ['menu' => $result];
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

}
