<?
namespace Main;


class Text
{
  public static function cleanText($text){
    if(empty($text)){
      return ['error' => 'Пустые данные'];
    }
    $text = trim(strip_tags($text));
    $text = self::switcherRU($text);
    $text = preg_replace("/[^а-яё ]/iu", '', $text);
    return ['text' => $text];
  }

  public static function switcherRU($value)
  {
    $converter = [
      'f' => 'а',	',' => 'б',	'd' => 'в',	'u' => 'г',	'l' => 'д',	't' => 'е',	'`' => 'ё',
      ';' => 'ж',	'p' => 'з',	'b' => 'и',	'q' => 'й',	'r' => 'к',	'k' => 'л',	'v' => 'м',
      'y' => 'н',	'j' => 'о',	'g' => 'п',	'h' => 'р',	'c' => 'с',	'n' => 'т',	'e' => 'у',
      'a' => 'ф',	'[' => 'х',	'w' => 'ц',	'x' => 'ч',	'i' => 'ш',	'o' => 'щ',	'm' => 'ь',
      's' => 'ы',	']' => 'ъ',	"'" => "э",	'.' => 'ю',	'z' => 'я',

      'F' => 'А',	'<' => 'Б',	'D' => 'В',	'U' => 'Г',	'L' => 'Д',	'T' => 'Е',	'~' => 'Ё',
      ':' => 'Ж',	'P' => 'З',	'B' => 'И',	'Q' => 'Й',	'R' => 'К',	'K' => 'Л',	'V' => 'М',
      'Y' => 'Н',	'J' => 'О',	'G' => 'П',	'H' => 'Р',	'C' => 'С',	'N' => 'Т',	'E' => 'У',
      'A' => 'Ф',	'{' => 'Х',	'W' => 'Ц',	'X' => 'Ч',	'I' => 'Ш',	'O' => 'Щ',	'M' => 'Ь',
      'S' => 'Ы',	'}' => 'Ъ',	'"' => 'Э',	'>' => 'Ю',	'Z' => 'Я',

      '@' => '"',	'#' => '№',	'$' => ';',	'^' => ':',	'&' => '?',	'/' => '.',	'?' => ',',
    ];

    $value = strtr($value, $converter);
    return $value;
  }

  public static function boldResult($text, $words){
    foreach($words as $word){
      $word = mb_strtolower($word);
      $text = str_replace($word, "<span class='searchBold'>" . $word . "</span>", $text);
      $word = self::mb_ucfirst($word);
      $text = str_replace($word, "<span class='searchBold'>" . $word . "</span>", $text);
    }
    return ['text' => $text];
  }

  public static function mb_ucfirst($string, $encoding = 'UTF-8')
  {
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, null, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
  }
}