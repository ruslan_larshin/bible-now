<?

namespace Main;

class Date
{
  public const monthsRus = [
    1 => 'января',
    2 => "февраля",
    3 => "марта",
    4 => "апреля",
    5 => "мая",
    6 => "июня",
    7 => "июля",
    8 => "августа",
    9 => "сентября",
    10 => "октября",
    12 => "декабря",
  ];

  public static function newsFormat($date)
  {
    try {
      return date('d', strtotime($date)) . ' ' . self::monthsRus[intval(date('m', strtotime($date)))] . ' ' . date('Y', strtotime($date));
    } catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }
}