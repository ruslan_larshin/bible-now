<?
namespace Test;

\CModule::IncludeModule('iblock');

class Questions extends \Main\Iblock
{
  public const ibCode = 'questions';
  public const select = ['ID', 'NAME', 'PROPERTY_LEVEL', 'PROPERTY_BOOK', 'PROPERTY_VARIANT1' , 'PROPERTY_VARIANT2',
    'PROPERTY_VARIANT3', 'PROPERTY_VARIANT4', 'PROPERTY_ANSWER', 'PROPERTY_ONE_ANSWER', 'PROPERTY_CROSSWORD_ANSWER',
    'PROPERTY_CROSSWORD', 'PROPERTY_BOOKS'];

  public static function getMap(){
    return [
      'id' => 'ID',
      'name' => 'NAME',
      'level' => 'PROPERTY_LEVEL',
      'book' => 'PROPERTY_BOOK',
      'books' => 'PROPERTY_BOOKS',
      'variant1' => 'PROPERTY_VARIANT1',
      'variant2' => 'PROPERTY_VARIANT2',
      'variant3' => 'PROPERTY_VARIANT3',
      'variant4' => 'PROPERTY_VARIANT4',
      'answer' => 'PROPERTY_ANSWER',
      'oneAnswer' => 'PROPERTY_ONE_ANSWER',
      'crosswordAnswer' => 'PROPERTY_CROSSWORD_ANSWER',
      'crossword' => 'PROPERTY_CROSSWORD',
    ];
  }

  public static function formatQuestion($question){
    foreach($question['book'] as $key => $book){
      if($book == 'NO'){
        unset($question['book'][$key]);
      }
    }
    $question['booksString'] = implode(', ', $question['book']);
    $answer = explode('/', $question['answer']);
    for($i = 1; $i < 5; $i++){
      if($answer[$i-1] == 1){
        $question['true'][] = $question['variant' . $i];
      }else{
        $question['false'][] = $question['variant' . $i];
      }
    }
    return $question;
  }

  public static function getList($request = []){
    try{
      if(!empty($request['books'])){
        $books = \Bible\Books::getList()['books'];
      }
      $select = self::select;
      $filter = $request['filter'] ?? [];
      $filter['IBLOCK_CODE'] = self::ibCode;
      $result = [];
      $res = \CIBlockElement::GetList(
        ['PROPERTY_LEVEL' => 'ASC'],
        $filter,
        false,
        false,
        $select
      );
      while ($ob = $res -> GetNextElement()) {
        $question = $ob -> GetFields();
        $booksDetail = [];
        if(!empty($request['books']) && !empty($question['PROPERTY_BOOKS_VALUE'])){
          foreach($question['PROPERTY_BOOKS_VALUE'] as $bookId){
            $booksDetail[] = $books[$bookId];
          }
        }
        $result[] = self::formatQuestion(
          [
            'id' => $question['ID'],
            'name' => $question['NAME'],
            'level' => $question['PROPERTY_LEVEL_VALUE'],
            'book' => $question['PROPERTY_BOOK_VALUE'],
            'books' => (!empty($request['books'])) ? $booksDetail : $question['PROPERTY_BOOKS_VALUE'],
            'variant1' => $question['PROPERTY_VARIANT1_VALUE'],
            'variant2' => $question['PROPERTY_VARIANT2_VALUE'],
            'variant3' => $question['PROPERTY_VARIANT3_VALUE'],
            'variant4' => $question['PROPERTY_VARIANT4_VALUE'],
            'answer' => $question['PROPERTY_ANSWER_VALUE'],
            'oneAnswer' => $question['PROPERTY_ONE_ANSWER_VALUE'],
            'crosswordAnswer' => $question['PROPERTY_CROSSWORD_ANSWER_VALUE'],
            'crossword' => $question['PROPERTY_CROSSWORD_VALUE'],
          ]
        );
      }
      return ['questions' => $result];
    }catch (\Bitrix\Main\SystemException $ex) {
      return ['error' => $ex];
    }
  }

  public static function updateBooks(){
    $books = \Bible\Books::getList(['idKeys' => 'NAME'])['books'];
    $questions = \Test\Questions::getList()['questions'];
    foreach($questions as $question){
      if(!empty($question['book'])){
        $newBooks = [];
        foreach($question['book'] as $bookName){
          $newBooks[] = $books[$bookName]['id'];
        }
        \CIBlockElement::SetPropertyValuesEx($question["id"], self::getIdByCode(self::ibCode), ["BOOKS" => $newBooks]);
      }
    }
  }
}