<?php
/** настрйоки */
use Bitrix\Main\EventManager;
use WebArch\BitrixNeverInclude\BitrixNeverInclude;
use Bitrix\Main\Loader;
use Bitrix\Main;
use Bitrix\Highloadblock\HighloadBlockTable as HL;
Loader::includeModule("highloadblock"); 
Loader::includeModule("iblock"); 
//require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bible.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Bibles.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Verse.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Main/Main.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Main/Pager.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/View/Main.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/View/Breadcrumb.php');
$view = new \Larshin\View\MainView(); 
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Search.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Quote.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Bible/Rubric.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/MyBook/Sermon.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/OtherBook/Apocrypha.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/OtherBook/Dictionary.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/Test/Test.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/lib/Larshin/View/Test.php');

define('url', 'http://bible-now.ru/');
define('Bible', 241);
define('Symphony', 242);
define("HighloadBook", 227);
define("allways", 0);
define("HighloadFirstBook", 148);
define("bibleQuoteIbId", 5);
define("bibleRubricIbId", 6);
define("sermonIbId", 7);
define("questionsIbId", 17);
define("dictionaryNikiforovIbId", 7);
define("HgMorfDictionary", 244);
define("HgMorfDictionary2", 245);
define("apocrypha", array('enoh' => 8 ,'varuh' => 9 , 'didahe' => 10 , 'foma' => 11 , 'true' => 12 , 'evrei' => 13 , 'petr' => 14 , 'fil' => 15));
define("dictionary", array('enoh' => 8 ,'varuh' => 9 , 'didahe' => 10 , 'foma' => 11 , 'true' => 12 , 'evrei' => 13 , 'petr' => 14 , 'fil' => 15));
define("alphabet", array('а','б','в','г','д','е','ж','з','и','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','э','ю','я'));

$classes = [
  '\Main\HighLoad' => '/local/lib/Main/HighLoad/HighLoad.php',
  '\Main\Iblock' => '/local/lib/Main/Iblock/Iblock.php',
  '\Main\Settings' => '/local/lib/Main/Settings/Settings.php',
  '\Main\User' => '/local/lib/Main/User/User.php',
  '\Main\Files' => '/local/lib/Main/Files/Files.php',
  '\Main\Text' => '/local/lib/Main/Text/Text.php',
  '\Main\Cache' => '/local/lib/Main/Cache/Cache.php',
  '\Main\Captcha' => '/local/lib/Main/Captcha/Captcha.php',
  '\Main\Date' => '/local/lib/Main/Date/Date.php',
  '\Main\Encrypt' => '/local/lib/Main/Encrypt/Encrypt.php',
  '\Main\Pager' => '/local/lib/Main/Pager/Pager.php',
  '\Main\Orm' => '/local/lib/Main/Orm/Orm.php',

  '\Bible\Books' => '/local/lib/Bible/Books/Books.php',
  '\Bible\Quote' => '/local/lib/Bible/Quote/Quote.php',
  '\Bible\Verses' => '/local/lib/Bible/Verses/Verses.php',
  '\Bible\Topics' => '/local/lib/Bible/Topics/Topics.php',
  '\Bible\Symphony' => '/local/lib/Bible/Symphony/Symphony.php',
  '\Bible\Peoples' => '/local/lib/Bible/Peoples/Peoples.php',

  '\Test\Questions' => '/local/lib/Test/Questions/Questions.php',

  '\Learn\Sermons' => '/local/lib/Learn/Sermons/Sermons.php',
  '\Learn\Search' => '/local/lib/Learn/Search/Search.php',
  '\Learn\Articles' => '/local/lib/Learn/Articles/Articles.php',
  '\Learn\Sing' => '/local/lib/Learn/Sing/Sing.php',

  '\Yandex\Speller' => '/local/lib/Yandex/Speller/Speller.php',
  '\Yandex\Dictionary' => '/local/lib/Yandex/Dictionary/Dictionary.php',

  '\Main\Curl' => '/local/lib/Main/Curl/Curl.php',

  '\Api\Lingvo' => '/local/lib/Api/Lingvo/Lingvo.php',

  '\Orm\SongsTable' => '/local/lib/Orm/Songs/Songs.php',
];
$request = $_REQUEST;
\Bitrix\Main\Loader::registerAutoLoadClasses(null, $classes);
function view($text)
{
	echo '<pre>'; print_r($text); echo '</pre>';
	return null;
}
?>