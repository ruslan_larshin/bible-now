class Tab{
    constructor (tabClass, tabResultClass) {
        this.tabClass = '.' + tabClass;
        this.tabResultClass = '.' + tabResultClass;
        this.initTabs();
    }

    initTabs(){
        let obj = this;
        $('body').on('click', this.tabClass, function (event) {
            if(!$(obj).hasClass('active')){
                let code = $(this).attr('data-code');
                $(obj.tabClass).removeClass('active');
                $(obj.tabClass + '.' + code).addClass('active');
                $(obj.tabResultClass).removeClass('active');
                $(obj.tabResultClass + '.' + code).addClass('active');
            }
        });
    }

}