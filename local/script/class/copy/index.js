class Copy{

    static initCopy(){
        let obj = this;
        $('body').on('click', '.copyText', function () {
            let copyObj = $(this);
            let text = $(this).attr('data-copy');
            navigator.clipboard.writeText(text).then(function () {
                $(this).prop('title', 'скопировано');
                Tooltip.view(copyObj, 'скопировано');
            }, function () {
                alert('Failure to copy. Check permissions for clipboard')
            });
        });
    }

}