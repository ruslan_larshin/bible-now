class Form{

    constructor (formClass, buttonClass) {
        this.formClass = '.' + formClass;
        this.init();
    }

    static enterInputClickButton(input, button){
        $('body').on('keypress', $('.' + input), function (event) {
            if($('.' + button)[0]) {
                let keyCode = event.keyCode ? event.keyCode :
                    event.charCode ? event.charCode :
                        event.which ? event.which : void 0;
                if (keyCode == 13) {
                    $('.' + button)[0].click();
                }
            }
        });
    }

    static disabledButtonByEmptyInput(input, button){
        $('body').on('blur input keyup', '.' + input, function(){
            let val = $(this).val();
            if(val){
                $('.' + button).removeClass('disabled');
            }else{
                $('.' + button).addClass('disabled');
            }
        });
    }

    static isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    static validInput (obj){
        let code = $(obj).attr('data-code');
        let title = $(obj).attr('data-title');
        let error = false;
        if($(obj).hasClass('require') && !$(obj).val()){
            $('.errorForm.' + code).html('Поле ' + title + ' обязательно к  заполнению');
            error = true;
            $(obj).addClass('alert');
        }
        if($(obj).hasClass('email') && $(obj).val()){
            let valid = Form.isEmail($(obj).val());
            if(!valid) {
                $('.errorForm.' + code).html('Невалидный email адрес!');
                error = true;
                $(obj).addClass('alert');
            }
        }
        if($(obj).hasClass('length')){
            let length = $(obj).attr('data-length');
            let value = $(obj).val().length;
            if(value < length){
                $('.errorForm.' + code).html('Поле ' + title + ' должно быть не меньше ' + length + ' символов');
                error = true;
                $(obj).addClass('alert');
            }
        }
        return error;
    }

    validForm(){
        let error = false;
        let obj = this;
        $(obj.formClass + ' .inputFormData').removeClass('alert');
        $(obj.formClass + ' .inputFormData').each(function(){
          error = Form.validInput(this);
        });
        if($(obj.formClass + ' .agreeDiv .agree')){
            if(!$(obj.formClass + ' .agreeDiv .agree')[0].checked){
                $('.errorForm.passwordConfirm').html('Необходимо принять условия!');
                error = true;
            }
        }
        if($(obj.formClass + ' .inputFormData.password') && $(obj.formClass + ' .inputFormData.passwordConfirm')){
            if($(obj.formClass + ' .inputFormData.password').val() !== $(obj.formClass + ' .inputFormData.passwordConfirm').val()){
                $(obj.formClass + ' .inputFormData.passwordConfirm').addClass('alert');
                $('.errorForm.passwordConfirm').html('Пароли не совпадают');
                error = true;
            }
        }
        if(error){
            return false;
        }else{
            return true;
        }
    }

    getFormData(){
        let data = {};
        let obj = this;
        $(obj.formClass + ' .inputFormData').each(function(){
            data[$(this).attr('data-code')] = $(this).val();
        });
        return {'data' : data}
    }

    Register(){
        let obj = this;
        $(obj.formClass + ' .buttonForm').addClass('disabled');
        let data = this.getFormData()['data'];
        if(!obj.validForm()){
            return false;
        }
        data['url'] = $('.registerComponent').attr('data-url') +'/ajax.php';
        Ajax.ajaxJson(
            data['url'],
            'POST', data,
            function(html){
                $(obj.formClass + ' .buttonFormAfter').removeClass('noDisplay');
                $(obj.formClass + ' .buttonForm').addClass('noDisplay');
                $(obj.formClass + ' .formResult').html(html);
                $(obj.formClass + ' .formResultErrors').html('');
                 $('.registerContainerMain').addClass('noDisplay');
                 $('.successRegistry').removeClass('noDisplay');
                 $('.emailAgreeTextPopup')[0].click();
            },
            function(html){
                $(obj.formClass + ' .formResultErrors').html(html);
                $(obj.formClass + ' .formResult').html('');
            }
        );
    }

    init(){
        let obj = this;
        $('document').ready(function(){

            $('body').on('change keyup input click','.onlyText', function() {
                if (this.value.match(/[^a-яА-Яa-zA-Z +]/g)) {
                    this.value = this.value.replace(/[^a-яА-Яa-zA-Z +]/g, '');
                }
            });

            $('body').on('change keyup input click','.numOrChars input', function() {
                obj = this;
                obj.value = obj.value.replace(/[^а-яА-ЯёЁa-zA-Z0-9]/ig,'');
            });

            $('body').on('change keyup input click','.russianText', function() {
                obj = this;
                obj.value = obj.value.replace(/[^а-яА-ЯёЁ -]/ig,'');
            });

            $('body').on('change keyup input click','.phone', function() {
                if (this.value.match(/[^0-9()+]/g)) {
                    this.value = this.value.replace(/[^0-9()+]/g, '');
                }
            });

            $('body').on('change keyup input click','.number', function() {
                if (this.value.match(/[^0-9+]/g)) {
                    this.value = this.value.replace(/[^0-9+]/g, '');
                }
            });

            $('body').on('click', obj.formClass + ' .inputFormData', function(){
                let code = $(this).attr('data-code');
                $('.errorForm.' + code).html('');
                $(this).removeClass('alert');
            });
            $('body').on('blur', obj.formClass + ' .inputFormData', function(){
                Form.validInput(this);
            });
        });
    }
}