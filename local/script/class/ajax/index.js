class Ajax{

    static updateAjax(component, template, resultClassBlock = false, params = [], func = false){
        let data = {
            'component' : component,
            'template' : template,
            'resultClassBlock' : resultClassBlock,
            'params' : params ?? [],
        };
        $.ajax({
            type: "POST",
            url: "/local/ajax/main/component/",
            data: data,
            dataType: 'html',
            success: function (html) {
                if(func){
                    func();
                }
                if(resultClassBlock) {
                    $('.' + resultClassBlock).html(html);
                }
            },
        });
    }

    static ajax(url, type='POST', data ={}, func = false){
        $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: 'html',
            success: function (html) {
                if(func){
                    func();
                }
            },
        });
    }
}
