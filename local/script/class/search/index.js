class Search{


    constructor (inputClass, updateFunction) {
        this.inputClass = '.' + inputClass;
        this.updateFunction = updateFunction;
        this.loadJsFromSearch();
        this.loadInput();
    }

    static loadSearchResult(){
        let data = {
            'search' : $('.searchInput').val(),
        };
        if($('.searchResult').hasClass('stop')){
            return false;
        }
        $('.searchResult').addClass('stop');
        Ajax.updateAjax(
            'bible:search.result',
            '', 'searchResult',
            data,
            function(){
                $('.searchResult').removeClass('stop');
                $('.searchInput').attr('data-search', data['search']);
                history.pushState(null, null, '?search=' + data['search']);
            }
        );
    }

    updateBySearch(enter = false){
        let obj = this;
        let date = Date.now();
        let keyUp =  $(obj.inputClass).attr('data-keyUp');
        $(obj.inputClass).attr('data-keyUp', date);
        if((parseInt(date) - parseInt(keyUp)) < 500){
            if(!enter) {
                return false;
            }
        }
        let timeLastUpdate = $(obj.inputClass).attr('data-time') ?? 0;
        if( ((parseInt(date) - parseInt(timeLastUpdate)) > 1000) || enter) {
            $(obj.inputClass).attr('data-time', date);
            let dataSearch = $(obj.inputClass).attr('data-search');
            let valSearch = $(obj.inputClass).val();
            if (!valSearch && !dataSearch) {
                return false;
            }
            if (valSearch !== dataSearch) {
               // $(obj.inputClass).attr('data-search', valSearch);
                obj.updateFunction();
            }
            return true;
        }else{
            return false;
        }
    }

    loadJsFromSearch() {
        let obj = this;
        $('body').on('keyup', this.inputClass, function (event) {
            obj.updateBySearch();
        });
        $('body').on('click', this.inputClass, function (event) {
            obj.updateBySearch();
        });
        $('body').on('blur', this.inputClass, function (event) {
            obj.updateBySearch();
        });
        $('body').on('keypress', this.inputClass, function (event) {
            var keyCode = event.keyCode ? event.keyCode :
                event.charCode ? event.charCode :
                    event.which ? event.which : void 0;
            if (keyCode == 13) {
                obj.updateBySearch(true);
            }
        });
        setInterval(obj.updateBySearch.bind(this), 1000);
    }

    loadInput() {
        let obj = this;
        $('body').on('keypress', this.inputClass, function (event) {
            let date = Date.now();
            $(obj.inputClass).attr('data-time', date);
        });
        setInterval(this.enterAfter.bind(this), 1000);
    }

    enterAfter() {
        let obj = this;
        var old = parseInt($(this.inputClass).attr('data-time'));
        var now = Date.now();
        if (now - old > 1000 && old > 1000) {
            var e = jQuery.Event("keypress");
            e.which = 13; //choose the one you want
            e.keyCode = 13;
            $(obj.inputClass).trigger(e);
        }
        return true;
    }

}