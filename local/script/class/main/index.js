class Main{

    static init(){
        $('document').ready(function(){

            $('body').on('change keyup input click','.onlyText', function() {
                if (this.value.match(/[^a-яА-Яa-zA-Z +]/g)) {
                    this.value = this.value.replace(/[^a-яА-Яa-zA-Z +]/g, '');
                }
            });

            $('body').on('change keyup input click','.numOrChars input', function() {
                obj = this;
                obj.value = obj.value.replace(/[^а-яА-ЯёЁa-zA-Z0-9]/ig,'');
            });

            $('body').on('change keyup input click','.russianText', function() {
                obj = this;
                obj.value = obj.value.replace(/[^а-яА-ЯёЁ -]/ig,'');
            });

            $('body').on('change keyup input click','.phone', function() {
                if (this.value.match(/[^0-9()+]/g)) {
                    this.value = this.value.replace(/[^0-9()+]/g, '');
                }
            });

            $('body').on('change keyup input click','.number', function() {
                if (this.value.match(/[^0-9+]/g)) {
                    this.value = this.value.replace(/[^0-9+]/g, '');
                }
                let maxValue = parseInt($(this).attr('data-maxValue'));
                let value = parseInt($(this).val());
                if(maxValue && value &&  (value > maxValue)){
                    $(this).val(maxValue);
                }
            });

        });

    }

    static enterInputClickButton(input, button){
        $('body').on('keypress', $('.' + input), function (event) {
            if($('.' + button)[0]) {
                let keyCode = event.keyCode ? event.keyCode :
                    event.charCode ? event.charCode :
                        event.which ? event.which : void 0;
                if (keyCode == 13) {
                    $('.' + button)[0].click();
                }
            }
        });
    }

}