class ConfirmForm {
    constructor() {
        let form = $('.confirmAdc');
        let classForm = '.confirmAdc';
    }

    getFormData(){
        let items = {};
        $('.confirmAdc input').each(function(index){
            items[index] = {
                'val' : $(this).val(),
                'code' : $(this).attr('data-code'),
                'smallCode' : $(this).attr('data-smallCode'),
                'entity' : $(this).attr('data-entity'),
                'type' : $(this).attr('data-type'),
                'title' : $(this).attr('data-title'),
                'last' : $(this).attr('data-last'),
            };
        });
        return items;
    }


    sendData(){
        let form = $('.confirmAdc');
        let template = form.attr('data-template');
        let data = {
            'adcId' : form.attr('data-adcId'),
            'items' : this.getFormData(),
        };
        console.log(data);
        $.ajax({
            type: "POST",
            url: template + "/send.php",
            data: data,
            dataType: "json",
            success: function (data) {
                $('.confirmAdc .confirmButton').html(data.success);
                window.location.href = "/b2b/"
            },
            error: function (data) {
                $('.confirmAdc .confirmButton').html(data.errors);
            }
        });
    }
}