<?
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);
define('CHK_EVENT', true);
define('BX_WITH_ON_AFTER_EPILOG', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
set_time_limit(0);
ini_set('max_execution_time', '0');
$count = 1000;
$page = $_REQUEST['page'] ?? 1;
$result = [];
$symphony = \Bible\Symphony::getList(['order' => ['ID' => 'ASC'], 'filter' => ['UF_DICTIONARY_WORDS' => false], 'nav' => ['limit' => $count, 'offset' => ($page -1) * $count]], true)['symphony'];
foreach($symphony as $item){
  $synonym = \Yandex\Dictionary::getResult($item['name'])['words'];
  if(!empty($synonym)){
    $symphonySynonyms = \Bible\Symphony::getList(['filter' => ['UF_NAME' => $synonym]], true)['symphony'];
  }
  $add = [
    'dictionaryWords' => [],
    'dictionaryIds' => $item['verseIds'],
  ];
  foreach($symphonySynonyms as $syn){
    $add['dictionaryWords'][$syn['name']] = $syn['name'];
    $add['dictionaryIds'] = array_merge($add['dictionaryIds'] ?? [], $syn['verseIds']);
  }
  $add['dictionaryWords'] = array_values($add['dictionaryWords']);
  $add['dictionaryIds'] = implode('-', $add['dictionaryIds']);
  $result[] = \Bible\Symphony::update($item['id'], $add);
}
view($result);
?>
