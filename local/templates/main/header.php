<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	 <link rel="icon" href="/biblelogoico.png" type="image/x-icon">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>

	<link rel="stylesheet" href="/local/templates/main/styles.css">
    <link rel="stylesheet" href="/local/style/articles.css"/>
    <link rel="stylesheet" href="/local/style/main.css"/>
    <link rel="stylesheet" href="/local/style/bible.css"/>

	<script src="/local/templates/main/js/jquery.js"></script>
	<script src="/local/templates/main/js/main.js"></script>
    <?
        Asset::getInstance()->addJs( "/local/script/class/search/index.js");
        Asset::getInstance()->addJs( "/local/script/class/ajax/index.js");
        Asset::getInstance()->addJs( "/local/script/class/copy/index.js");
        Asset::getInstance()->addJs( "/local/script/class/tooltip/index.js");
        Asset::getInstance()->addJs( "/local/script/class/main/index.js");
    ?>
	<title><?global $APPLICATION;
	$APPLICATION->ShowTitle(false);?></title>
	<?
    $arMenu = [
		[
			'name'=>'Изучение',
			'url'=>'/learn/',
		],
		[
			'name'=>'Краткая Библия',
			'url'=>'/quote/',
		],
		[
			'name'=>'Симфония',
			'url'=>'/symphony/',
		],
		[
			'name'=>'Библия',
			'url'=>'/bible/',
		],
	];
    global $USER;
    if($USER -> isAdmin()){
        $arMenu[] = [
            'name' => 'Инструменты',
            'url' => 'tools',
        ];
    }
	?>
	<? $APPLICATION->ShowHead(); ?>
	<? $APPLICATION->ShowPanel() ?>
    <?require_once($_SERVER['DOCUMENT_ROOT'] . '/local/img/svg/index.php')?>
	<div class='topMenu'>
		<div class='contentTopMenu'>
			<div class='logo'>
				<a href = '/bible/' >
					<img src ='/local/img/bibleLogo4.png'/>
				</a>
			</div>
			<div class='search'>
				<input class='search_input searchHeader' data-time='0' type='text' placeholder='Введите запрос для поиска'/>
			</div>
			<div class='menu'>
				<?foreach($arMenu as $menu){?>
					<div class='menuItem'><a href='<?=$menu['url']?>' ><?=$menu['name']?></a></div>
				<?}?>
			</div>
			<div class='menuMobile'>
				<img src= '/local/img/menu.png' />
			</div>
		</div>
	</div>
	<div class='whitePodmenu'>
		<?for($i = count($arMenu); $i >= 0 ; $i--){?>
			<div class='menuItemMobile'><a href='<?=$arMenu[$i]['url']?>' ><?=$arMenu[$i]['name']?></a></div>
		<?}?>
	</div>
</head>
<body>
	<div class='main'>
	