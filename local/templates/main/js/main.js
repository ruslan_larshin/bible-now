//Функции для вывода всей главы с подсвеченным стихом при нажатии на этот стих
function viewChapterByVerse(verseId)
{
	var data = 'verseId=' + verseId;
	$(".chapterByVerseAjax").html('');
	if($('.verseOpenChapter_'+verseId).hasClass('active')){
		$('.verseOpenChapter_'+verseId).removeClass('active');
		$(".result_"+verseId).html(''); 
	}else{
		$.ajax({
			type: "POST", 
			url: "/local/ajax/chapter/chapterByVerse.php", 
			data: data,
				success: function(html){ 
					$(".result_"+verseId).html(html); 
					$('.verseOpenChapter_'+verseId).addClass('active');
				}
		});
	}
}
//--Функции для вывода всей главы с подсвеченным стихом при нажатии на этот стих -внимание проверять одноименность подключаемых файлоы!!url: "/local/ajax/search/" + searchClass + ".php", 

//Функции для вывода краткой библии уцитатами
function viewQuoteDetail(quoteId)
{
	var data = 'quoteId=' + quoteId;
	$(".resultQuoteDeatil").html('');
	if($('.quoteDetail'+quoteId).hasClass('active')){
		$('.quoteDetail').removeClass('active');
		$('.resultQuoteDeatil').removeClass('active');
		$('.quoteDetail'+quoteId).removeClass('active');
		$('.resultQuoteDeatil'+quoteId).removeClass('active');
		$(".resultQuoteDeatil"+quoteId).html(''); 
	}else{
		$('.quoteDetail').removeClass('active');
		$('.resultQuoteDeatil').removeClass('active');
		$.ajax({
			type: "POST", 
			url: "/local/ajax/quote/quoteDetail.php", 
			data: data,
				success: function(html){ 
					$(".resultQuoteDeatil"+quoteId).html(html); 
					$('.resultQuoteDeatil'+quoteId).addClass('active');
					$('.quoteDetail'+quoteId).addClass('active');
				}
		});
	}
}
//--Функции для вывода краткой библии цитатами

//выпадающее меню мобилка
function openMobileMenu()
{
	if($('.whitePodmenu').hasClass('active')){
		$('.whitePodmenu').removeClass('active');
	}else{
		$('.whitePodmenu').addClass('active');
	}
}
//выпадающее меню мобилка


//табы
	function tabClick(id)
	{
		$('.tabTtitleElment').removeClass('active');
		$('.tabTtitleElment').addClass('noActive');
		$('.tabContent').addClass('noDisplay');
		
		$('#tabs' + id).addClass('active');
		$('#tabs' + id).removeClass('noActive');
		$('#tabsElement' + id).removeClass('noDisplay');
	}
//--табы

//функции для поиска
updateSearch = function(searchClass , inputClass, name = 'search' , flag = false)
{
	var data = 'search='+$('.' + inputClass).val();
	if($('.' + searchClass).attr('data-search') !=  'search=' + $('.' + inputClass).val())
	{
		if($('.' + searchClass).hasClass('active') || flag){
			$('.' + searchClass).html('<div style="width: 98%; position: absolute; text-align:center; vertical-align: middle; top: 45%;"><img src="/images/load.gif" /></div>'); 
			$('.' + searchClass).removeClass('active'); 
				$.ajax({
					type: "POST", 
					url: "/local/ajax/"+ name+ "/" + searchClass + ".php", 
					data: data,
						success: function(html){ 
							$('.' + searchClass).html(html); 
							$('.' + searchClass).attr('data-search' , data); 
							history.pushState(null, null, '/'+ name +'/?'+data);
							$('.' + searchClass).addClass('active');
						}
				});
				
		}
	}
	return true;
}

loadJsFromSearch = function (searchClass , inputClass ,  name = 'search')
{
	$('body').on('keyup', '.' + inputClass, function(event){
		updateSearch(searchClass , inputClass , name);
	});
	$('body').on('blur', '.' + inputClass, function(event){
		updateSearch(searchClass , inputClass , name);
	});
	$('body').on('keypress', '.' + inputClass, function(event){
		var keyCode = event.keyCode ? event.keyCode : 
		event.charCode ? event.charCode :
		event.which ? event.which : void 0;
		if(keyCode == 13)
		{
			updateSearch(searchClass , inputClass , name);
		}
	});
	update = function()
	{
		updateSearch(searchClass , inputClass , name);
	}
	setInterval(update , 1000);
}

loadInput = function(inputClass)
{
	$('body').on('keypress', '.' +inputClass , function(event){
		var date = Date.now();
		$('.' + inputClass).attr('data-time' , date);
	});
	enterAfterInterval = function()
	{
		enterAfter(inputClass);
	}
	setInterval(enterAfterInterval , 1000);
}

enterAfter = function(inputClass)
{
	var old = $('.' + inputClass).attr('data-time');
	var now = Date.now();

	if(now - old > 1000 && old >1000)
	{
		var e = jQuery.Event("keypress");
		e.which = 13; //choose the one you want
		e.keyCode = 13;
		$("." + inputClass).trigger(e);
	}
	return true;
}

LoadFromHeader = function()
{
	$('body').on('keypress', '.searchHeader', function(event){
		var keyCode = event.keyCode ? event.keyCode : 
		event.charCode ? event.charCode :
		event.which ? event.which : void 0;
		if(keyCode == 13)
		{
			window.location.href = "/search/?search=" + $('.searchHeader' ).val();
		}
	});
	loadInput('searchHeader');
	return true;
}
//--функции для поиска

//скрипты для теста
function trueAnswer()
{
	$('body').on('click' , '.variantTest' , function(){
		answer = $(this).attr('data-answer');
		if($(this).hasClass('redBlock') || $(this).hasClass('greenBlock'))
		{
			$(this).removeClass('redBlock');
			$(this).removeClass('greenBlock');
		}else{
			if(answer == 0)
			{
				$(this).addClass('redBlock');
			}
			if(answer == 1)
			{
				$(this).addClass('greenBlock');
			}
		}
	});
	
	$('body').on('click' , '.viewAnswer' , function(){
		$(this).toggleClass('active');
	});
}
$('body').on('click' , '.variantTest' , function(){
	answer = $(this).attr('data-answer');
	alert(answer);
});
//--скрипты для теста

function clearTextarea()
{
	$('body').on('click' , '.noClear' , function(){
		$(this).val('');
		$(this).removeClass('noClear');
	});
}

function makeWordverse()
{
	text = $('.bigTextarea').val();
	$('.searchResult').html('<div style="width: 98%; position: absolute; text-align:center; vertical-align: middle; top: 45%;"><img src="/images/load.gif" /></div>'); 
	$.ajax({
		type: "POST", 
		url: "/local/ajax/tools/verseByWord.php", 
		data: 'text=' +text + '&name=' + $('.nameVerseWord').val(),
			success: function(html){ 
				$('.searchResult').html(html); 
			}
	});
}

function moreQuestionsList(page , newList = false, data ='')
{
	$('.moreQuestion').addClass('noDisplay');

		data = data +'&page=' + page;
	$.ajax({
		type: "POST",
		url: "/local/ajax/test/listGame.php",
		data: data,
		success: function(html){
			if(!newList){
				$('.testListresultAjax').append(html);
			}else{
				$('.testListresultAjax').html(html);
			}

		}
	});
}

function newGame(data ='')
{

	data = data ;
	$.ajax({
		type: "POST",
		url: "/local/ajax/test/game.php",
		data: data,
		success: function(html){
			$('.gameMain').html(html);
		}
	});
}

function getTestFilterParams() {
	data = 'page=1&';
	level = 'level=0,';
	$(".groupLevel .checkbox.active").each(function (i, n) {
		level = level + $(this).attr('data-level') + ',';
	});
	data = data + level;
	if ($('.oneAnswer').hasClass('active')) {
		data = data + '&oneAnswer=1';
	}
	book = '&book=1,';
	$(".groupBook .selecetedItemValue.active").each(function (i, n) {
		book = book + $(this).attr('data-id') + ',';
	});
	data = data + book;
	if ($('.checkbox.viewBook').hasClass('active')) {
		data = data + '&viewBook=y';
	}
	if ($('.checkbox.noViewAnswer').hasClass('active')) {
		data = data + '&noViewAnswer=y';
	}
	moreQuestionsList(1 , true , data);
}

function getGameFilterParams() {
	data = '';
	level = 'level=0,';
	$(".groupLevel .checkbox.active").each(function (i, n) {
		level = level + $(this).attr('data-level') + ',';
	});
	data = data + level;
	if ($('.oneAnswer').hasClass('active')) {
		data = data + '&oneAnswer=1';
	}
	book = '&book=1,';
	$(".groupBook .selecetedItemValue.active").each(function (i, n) {
		book = book + $(this).attr('data-id') + ',';
	});
	data = data + book;
	if ($('.checkbox.stopError').hasClass('active')) {
		data = data + '&stopError=y';
	}
	newGame(data);
}

//а здесь мы будем подключать все события на обьекты
$(document).ready(function(){
	clearTextarea();
	trueAnswer();
	$('body').on('click' , '.moreQuestion' , function(){
		moreQuestionsList($(this).attr('data-page') , false, $(this).attr('data-filter'));
	});
	$('body').on('click' , '.verseOpenChapter' , function(){
		viewChapterByVerse($(this).attr('data-verse-id'));
	});
	$('body').on('click' , '.testListFilter' , function(){
		getTestFilterParams();
	});
	$('body').on('click' , '.selectMulty .default' , function(){
		$('.selectMulty').toggleClass('active');
	});
	$('body').on('click' , '.verseByWord' , function(){
		makeWordverse();
	});
	$('body').on('click' , '.quoteDetail' , function(){
		viewQuoteDetail($(this).attr('data-id'));
	});
	$('body').on('click', '.menuMobile', function(){
		openMobileMenu();
	});
	$('body').on('click', '.main', function(){
		$('.whitePodmenu').removeClass('active');
	});
	$('body').on('click' , '.checkbox' , function(){
		$(this).toggleClass('active');
	});
	$('body').on('click' , '.selecetedItemValue' , function(){
		$(this).toggleClass('active');
	});
	$('body').on('click' , '.noFilter' , function(){
		$('.filterTest').toggleClass('noActive');
	});
	$(document).mouseup(function (e) {
		var container = $(".selectMulty");
		if (container.has(e.target).length === 0){
			$('.selectMulty').removeClass('active');
		}
	});
	$('body').on('click', '.tabTtitleElment', function(){
		if(!$(this).hasClass('active'))
		{
			id = $(this).attr('data-id');
			tabClick(id);
		}
	});
	LoadFromHeader();
})
// -- а здесь мы будем подключать все события на обьекты