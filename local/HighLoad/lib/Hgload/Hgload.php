<?php
namespace Hgload;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;	
	
Loader::includeModule("highloadblock"); 

	
define("HgloadGoods", 1);
define("RegName", "/^[A-ZА-яёЁa-zа-я0-9 .!-]{2,40}$/u");
define("RegPrice", "/^[0-9]{2,6}$/");
define("RegDate", "/^(0?[1-9]|[12][0-9]|3[01])[.](0?[1-9]|1[012])[.](19|20)?[0-9]{2}$/");

class Hgload
{
	public $HgloadId = false;
	public $entityHgload;
    public function __construct(int $HgloadId = 1)
    {
		$HgloadId = \Bitrix\Main\Config\Option::get("HighLoad", "HGId");
		$this->HgloadId = $HgloadId;
		$hlblock = HL\HighloadBlockTable::getById($HgloadId)->fetch(); 
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$this->entityHgload = $entity->getDataClass(); 
		return true;	
    }
	
	public function getList($arSelect,$arFilter,$order = array(),$limit = 10,$offset = 0) 
	{
		// подготавливаем данные
		$rsData = $this->entityHgload::getList(array(
			"select" => $arSelect,
			"filter" => $arFilter,
			"limit" => $limit, //ограничим выборку пятью элементами
			"order" => $arOrder
		));
		 
		// выполняем запрос. Передаем в него наши данные и название таблицы, которое мы получили в самом начале
		$rsData = new \CDBResult($rsData, $sTableID); // записываем в переменную объект CDBResult
		 
		// а далее простой цикл и знакомый нам метод Fetch (или GetNext, кому что нравится)
		while($arRes = $rsData->Fetch()){
			$result[] = $arRes;
		}
		return $result;
	}
	
	public function getListGoods($page = 1)
	{
		$result = array();
		$arFilter = array(); 
		$arSelect = array('UF_NAME','UF_PRICE','UF_DATE','UF_PICTURE','ID'); 
		$arOrder = array("ID"=>"ASC"); 
		if(!$page)
			$page = 1;
		$limit = 5;
		$offset = $limit*($page - 1);
		 
		// подготавливаем данные
		$rsData = $this->entityHgload::getList(array(
			"select" => $arSelect,
			"filter" => $arFilter,
			"limit" => $limit, //ограничим выборку пятью элементами
			"order" => $arOrder,
			'offset' => $offset,
			'count_total' => true,
		));
		$count = $rsData->getCount();
		$rsData = new \CDBResult($rsData); // записываем в переменную объект CDBResult
		while($arRes = $rsData->Fetch()){
			//$src = \CFile::GetPath($arRes['UF_PICTURE']);
			$file = \CFile::ResizeImageGet($arRes['UF_PICTURE'],array('width' => '100px', 'height' => '100px'));
			$src = $file['src'];
			if(!$src)
				$src = '/local/img/noPhoto.jpg';
			$result['ITEMS'][] = array( 
				'ID' => $arRes['ID'],
				'NAME' => $arRes['UF_NAME'],
				'PRICE' => $arRes['UF_PRICE'],
				'DATE' => $arRes['UF_DATE'],
				'PICTURE' => $src,
			);
		}
		$result['PAGER']=array(
			'LIMIT' => $limit,
			'PAGE' => $page,
			'COUNT' => $count,
			'PAGE_COUNT' => ceil($count/$limit)
		);
		
		return $result;
	}
	
	public function getById($id)
	{
		$result = array();
		if(is_numeric($id)){		
			$arFilter = array("ID"=>$id); 
			$arSelect = array('UF_NAME','UF_PRICE','UF_DATE','UF_PICTURE','ID'); 

			$rsData = $this->entityHgload::getList(array(
				"select" => $arSelect,
				"filter" => $arFilter,
			));
 			$rsData = new \CDBResult($rsData); // записываем в переменную объект CDBResult
			while($arRes = $rsData->Fetch()){
				$buf = explode('.',$arRes['UF_DATE']);
				$buf = str_replace('',' ',$buf[2] . '-' . $buf[1] . '-' . $buf[0]);
				$src = \CFile::GetPath($arRes['UF_PICTURE']);
				if(!$src)
					$src = '/local/img/noPhoto.jpg';
				$result['ITEMS'][] = array( 
					'ID' => $arRes['ID'],
					'NAME' => $arRes['UF_NAME'],
					'PRICE' => $arRes['UF_PRICE'],
					'DATE' => $arRes['UF_DATE'],
					'DATEINPUT' => $buf,
					'PICTURE' => $src,
				);
			}
		}
		
		return $result;
	}
	
	public function add($data)
	{
		if($data)
		{
			$result = $this->entityHgload::add($data);
		}
		
		return $result;
	}
	
	public function save($arRequest,$FILES)
	{
		$arResult = array();
		try
		{
			if($arRequest['UF_DATE'])
			{
				$Date = explode('-',$arRequest['UF_DATE']);
				$Date = $Date[2].'.'.$Date[1].'.'.$Date[0];
			}
			$data = array();
			if(preg_match(RegName, $arRequest['UF_NAME'], $matches))
			{
				$data['UF_NAME'] = $arRequest['UF_NAME'] ?? 'Имя не задано';
			}else{
				$arResult['ERRORS'][] = 'Имя ' . $arRequest['UF_NAME'] . ' не соответествует регулярному выражению ' . RegName . '<br/>';
			}
			if(preg_match(RegDate,$Date))
			{
				$data['UF_DATE'] = $Date ?? '01.01.1991';
			}else{
				$arResult['ERRORS'][] = 'Дата ' . $Date . ' не соответествует регулярному выражению ' . RegDate . '<br/>';
			}
			if(preg_match(RegPrice,$arRequest['UF_PRICE']))
			{
				$data['UF_PRICE'] = $arRequest['UF_PRICE'] ?? '0';
			}else{
				$arResult['ERRORS'][] = 'Цена ' . $arRequest['UF_PRICE'] . ' не соответествует регулярному выражению ' . RegPrice . '<br/>';
			}
			if($FILES['UF_PICTURE']['name'])
			{
				$data['UF_PICTURE'] = $FILES['UF_PICTURE'];
			}
			if($data && !$arResult['ERRORS'])
				$result = $this->entityHgload::add($data);
			if( $arResult['ERRORS']){
				throw new SystemException($error); 
			}else{
				$arResult['SUCCESS'][] = 'Обновление  прошло успешно : id = ' . $result->getid();
			}
		}
		catch (SystemException $exception)
		{
			echo $exception->getMessage();
		}
			
		return $arResult;
	}
	
	
	public function update($id,$arRequest,$FILES)
	{
		$arResult = array();
		if($id)
		{
			try
			{
				if($arRequest['UF_DATE'])
				{
					$Date = explode('-',$arRequest['UF_DATE']);
					$Date = $Date[2].'.'.$Date[1].'.'.$Date[0];
				}
				$data = array();
				if(preg_match(RegName, $arRequest['UF_NAME'], $matches))
				{
					$data['UF_NAME'] = $arRequest['UF_NAME'] ?? 'Имя не задано';
				}else{
					$arResult['ERRORS'][] = 'Имя ' . $arRequest['UF_NAME'] . ' не соответествует регулярному выражению ' . RegName . '<br/>';
				}
				if(preg_match(RegDate,$Date))
				{
					$data['UF_DATE'] = $Date ?? '01.01.1991';
				}else{
					$arResult['ERRORS'][] = 'Дата ' . $Date . ' не соответествует регулярному выражению ' . RegDate . '<br/>';
				}
				if(preg_match(RegPrice,$arRequest['UF_PRICE']))
				{
					$data['UF_PRICE'] = $arRequest['UF_PRICE'] ?? '0';
				}else{
					$arResult['ERRORS'][] = 'Цена ' . $arRequest['UF_PRICE'] . ' не соответествует регулярному выражению ' . RegPrice . '<br/>';
				}
				if($FILES['UF_PICTURE']['name'])
				{
					$data['UF_PICTURE'] = $FILES['UF_PICTURE'];
				}
				if($data && $id)
					$result = $this->entityHgload::update($id, $data);
				if(!$result->getid() || $arResult['ERRORS']){
					$errors = $result->	getErrorMessages();
					$errors[]=$errors[0];
					foreach($errors as $error)
					{
						//echo '<pre>'; print_r('Сохранение не прошло : ' . $error); echo '</pre>';
						$arResult['ERRORS'][] = $error;
					}
					/*foreach($arResult['ERRORS'] as $key=>$error)
					{
						if($error)
						{
							echo '<pre>'; print_r(($key+1).') Сохранение не прошло : ' . $error); echo '</pre>';
						}
					}*/
					
					throw new SystemException($error); 
				}else{
					$arResult['SUCCESS'][] = 'Обновление  прошло успешно : id = ' . $result->getid();
					//echo '<pre>'; print_r('Обновление  прошло успешно : id = ' . $result->getid()); echo '</pre>';
				}
			}
			catch (SystemException $exception)
			{
				echo $exception->getMessage();
			}
		}else{
			$arResult['ERRORS'][] = 'Не задан Id элемента- необходимый для редактирования!';
		}
		return $arResult;
	}
	
	public function getRequest()
	{
		$arRequest=array();
		$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		$postValues = $request->getPostList()->toArray(); 
		$getValues = $request->getQueryList()->toArray();
		$arRequest=array_merge($postValues,$getValues);
		
		return $arRequest;
	}
}