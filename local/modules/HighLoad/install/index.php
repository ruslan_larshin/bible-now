<?php
/*
 * Файл local/modules/HighLoad/install/index.php
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\UserTable,
	Bitrix\Main,
	Bitrix\Main\SystemException,
	Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;	
	
Loader::includeModule("highloadblock");
Loc::loadMessages(__FILE__);

class HighLoad extends CModule {

    public function __construct() {
        if (is_file(__DIR__.'/version.php')){
            include_once(__DIR__.'/version.php');
            $this->MODULE_ID           = get_class($this);
            $this->MODULE_VERSION      = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->MODULE_NAME         = Loc::getMessage('HighLoad_NAME');
            $this->MODULE_DESCRIPTION  = Loc::getMessage('HighLoad_DESCRIPTION');
        } else {
            CAdminMessage::ShowMessage(
                Loc::getMessage('HighLoad_FILE_NOT_FOUND').' version.php'
            );
        }
    }
    
    public function DoInstall() {

        global $APPLICATION;

        // мы используем функционал нового ядра D7 — поддерживает ли его система?
        if (CheckVersion(ModuleManager::getVersion('main'), '14.00.00')) {
            // копируем файлы, необходимые для работы модуля
            $this->InstallFiles();
            // создаем таблицы БД, необходимые для работы модуля
            $this->InstallDB();
            // регистрируем модуль в системе
            ModuleManager::registerModule($this->MODULE_ID);
            // регистрируем обработчики событий
            $this->InstallEvents();
        } else {
            CAdminMessage::ShowMessage(
                Loc::getMessage('HighLoad_INSTALL_ERROR')
            );
            return;
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage('HighLoad_INSTALL_TITLE').' «'.Loc::getMessage('HighLoad_NAME').'»',
            __DIR__.'/step.php'
        );
    }
    
    public function InstallFiles() {
        // копируем js-файлы, необходимые для работы модуля
        CopyDirFiles(
            __DIR__.'/assets/',
            Application::getDocumentRoot().'/local/'.$this->MODULE_ID.'/',
            true,
            true
        );
		 CopyDirFiles(
            __DIR__.'/components/',
            Application::getDocumentRoot().'/local/components/',
            true,
            true
        );
        // копируем css-файлы, необходимые для работы модуля
       /* CopyDirFiles(
            __DIR__.'/assets/styles',
            Application::getDocumentRoot().'/bitrix/css/'.$this->MODULE_ID.'/',
            true,
            true
        );*/
    }
    
    public function InstallDB() {
        return;
    }
	
	public function addPropertyHighload($highLoadBlockId,$name,$type,$title,$size = 20) //$type=string || integer
	{
		$userTypeEntity    = new \CUserTypeEntity();
		$userTypeData    = array(
			'ENTITY_ID'         => 'HLBLOCK_'.$highLoadBlockId,
			'FIELD_NAME'        => 'UF_' . $name,
			'USER_TYPE_ID'      => $type,
			'XML_ID'            => 'XML_ID_NAME',
			'SORT'              => 500,
			'MULTIPLE'          => 'N',
			'MANDATORY'         => 'Y',
			'SHOW_FILTER'       => 'N',
			'SHOW_IN_LIST'      => '',
			'EDIT_IN_LIST'      => '',
			'IS_SEARCHABLE'     => 'N',
			'SETTINGS'          => array(
				'DEFAULT_VALUE' => '',
				'SIZE'          => $size,
				'ROWS'          => '1',
				'MIN_LENGTH'    => '0',
				'MAX_LENGTH'    => '0',
				'REGEXP'        => '',
		),
			'EDIT_FORM_LABEL'   => array(
				'ru'    => $title,
				'en'    => 'Property name',
			),
			'LIST_COLUMN_LABEL' => array(
				'ru'    => $title,
				'en'    => 'Property name',
			),
			'LIST_FILTER_LABEL' => array(
				'ru'    => $title,
		'en'    => 'Property name',
		),
			'ERROR_MESSAGE'     => array(
				'ru'    => 'Ошибка при заполнении пользовательского свойства ' . $title,
				'en'    => 'An error in completing the user field <Property name>',
		),
			'HELP_MESSAGE'      => array(
				'ru'    => '',
		'en'    => '',
			),
		);
		$userTypeId = $userTypeEntity->Add($userTypeData);
		return $userTypeId;
	}
	
    public function InstallEvents() {
        // перед выводом буферизированного контента добавим свой HTML код,
        // в котором сохраним настройки для нашей кнопки прокрутки наверх
        EventManager::getInstance()->registerEventHandler(
            'main',
            'OnBeforeEndBufferContent',
            $this->MODULE_ID,
            'HighLoad\\Main',
            'appendJavaScriptAndCSS'
        );
		
		$result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
			'NAME' => 'HighLoadModules',//должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
			'TABLE_NAME' => strtolower('HighLoadModules'),//должно состоять только из строчных латинских букв, цифр и знака подчеркивания
		));
		if (!$result->isSuccess()) {
			$errors = $result->getErrorMessages();
			echo '<pre>'; print_r($errors); echo '</pre>'; 
		} else {
			$id = $result->getId();
			$this->addPropertyHighload($id,'NAME','string','Наименование товара');
			$this->addPropertyHighload($id,'PRICE','integer','Цена');
			$this->addPropertyHighload($id,'PICTURE','file','Изображение товара');
			$this->addPropertyHighload($id,'DATE','datetime','Дата создания'); 
			Option::set("HighLoad", "HGId", $id);
		}
    }

    public function DoUninstall() {

        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage('HighLoad_UNINSTALL_TITLE').' «'.Loc::getMessage('HighLoad_NAME').'»',
            __DIR__.'/unstep.php'
        );
		Option::delete("HighLoad", array(
			"name" => "HGId",
			"site_id" => "s1"
		) );

    }

    public function UnInstallFiles() {
        // удаляем js-файлы
        Directory::deleteDirectory(
            Application::getDocumentRoot().'/local/'.$this->MODULE_ID
        );
		Directory::deleteDirectory(
            Application::getDocumentRoot().'/local/components/larshin/'
        );
        // удаляем настройки нашего модуля
		echo '</pre>'; print_r(Option::get("HighLoad", "HGId")); echo '</pre>';
		Bitrix\Highloadblock\HighloadBlockTable::delete(Option::get("HighLoad", "HGId")); 
        Option::delete($this->MODULE_ID);
    }
    
    public function UnInstallDB() {
        return;
    }
    
    public function UnInstallEvents(){
        // удаляем наш обработчик события
        EventManager::getInstance()->unRegisterEventHandler(
            'main',
            'OnBeforeEndBufferContent',
            $this->MODULE_ID,
            'HighLoad\\Main',
            'appendJavaScriptAndCSS'
        );
    }

}