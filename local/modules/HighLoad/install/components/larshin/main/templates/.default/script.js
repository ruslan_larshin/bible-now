var jsonUpdate;
$('body').on('keyup','#searchList',function(){
	//alert($(this).val()); 
});
$('body').on('click','.control__button',function(){
	if(!$(this).hasClass('allCheckbox')){
		$(this).toggleClass('active');
		$('.allCheckbox').removeClass('active');
	}else{
		if($(this).hasClass('active')){
			$('.control__button').removeClass('active');
		}else{
			$('.control__button').addClass('active');
		}
	}
});
//редактирование в строке таблицы(доп вариант)
$('body').on('click','.edit',function(){
	if(!$('.popup').hasClass('stop')){
		$('.popup').addClass('stop')
		var id = $(this).attr('data-id');
		var data = 'ID='+id;
		$.ajax({
			type: "POST", 
			url: "/local/HighLoad/ajax/editHgloadLine.php", 
			data: data,
				success: function(html){ 
					$(".itemList"+id).html(html);					
					$(".itemList"+id).addClass('active');	
					$('.popup').removeClass('stop');
				}
		});
	}
});
//--редактирование в строке таблицы(доп вариант)
	
//редактирование popup
$('body').on('click','.editPopup',function(){
	if(!$('.popup').hasClass('stop')){
		$('.popup').addClass('stop')
		var id = $(this).attr('data-id');
		popupOpen();
		var data = 'ID='+id;
		$.ajax({
			type: "POST", 
			url: "/local/HighLoad/ajax/Hgload/edit.php", 
			data: data,
				success: function(html){ 
					$(".contentPopup").html(html);	
					$('.popup').removeClass('stop');					
				}
		});
	}
});
//--редактирование popup

//addNew popup
$('body').on('click','.addHgload',function(){
	if(!$('.popup').hasClass('stop')){
		$('.popup').addClass('stop')
		popupOpen();
		$.ajax({
			type: "POST", 
			url: "/local/HighLoad/ajax/Hgload/add.php", 
				success: function(html){ 
					$(".contentPopup").html(html);	
					$('.popup').removeClass('stop');						
				}
		});
	}
});
//--addNew popup

function hgloadSave(data,url)
{
	$('.errorPopup').html(''); 
	$('.resultPopup').html(''); 
	$(".resultPopup").html('Подождите! Идет загрузка данных на сервер!');  
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		url: url,
		data: data,
		processData: false,
		contentType: false,
		cache: false,
		timeout: 800000,
		success: function (html) {
			$(".resultPopup").html(''); 
			jsonUpdate = JSON.parse(html);
			if(jsonUpdate.SUCCESS != 'undefined'){
				$('.resultPopup').html(jsonUpdate.SUCCESS);
			}
			if(jsonUpdate.ERROR != 'undefined'){
				$('.errorPopup').html(jsonUpdate.ERRORS); 
			}		
		},
		error: function (e) {
			alert('Ошибка аякса-файл не запущен!"');
		}
	});
}

$('body').on('click', '.savePopup', function(){
	var id = $(this).attr('data-id');
	var form = $('.form'+id)[0]; 
	var data = new FormData(form);
	data.append('ID',id);
	hgloadSave(data,"/local/HighLoad/ajax/Hgload/update.php")

});	

$('body').on('click', '.addPopup', function(){
	var form = $('.formAdd')[0]; 
	var data = new FormData(form);
	hgloadSave(data,"/local/HighLoad/ajax/Hgload/save.php")

});	
	
$(document).ready(function(){
	$('body').on('click', '.save', function(){
		var id = $(this).attr('data-id');
		var form = $('#form'+id)[0]; 
		var data = new FormData(form);
		data.append('ID',id);
		$.ajax({
			type: "POST",
			enctype: 'multipart/form-data',
			url: "/local/HighLoad/ajax/Hgload/update.php",
			data: data,
			processData: false,
			contentType: false,
			cache: false,
			timeout: 800000,
			success: function (html) {
				$(".result"+id).html(html);  
			},
			error: function (e) {
				alert('Ошибка аякса-файл не запущен!"');
			}
		});
	});
	$('#PICTURE').change(function () {
		var input = $(this)[0];
		if (input.files && input.files[0]) {
			if (input.files[0].type.match('image.*')) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.fileLoad').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				console.log('ошибка, не изображение');
			}
		} else {
			console.log('хьюстон у нас проблема');
		}
	});
});