<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Компонент редактирвоани элемента HighLoad блока',
	"DESCRIPTION" => 'Компонент редактирвоани элемента HighLoad блока',
	"ICON" => "/images/news_list.gif",
	"SORT" => 1,
//	"SCREENSHOT" => array(
//		"/images/post-77-1108567822.jpg",
//		"/images/post-1169930140.jpg",
//	),
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "larshin",
			"NAME" => GetMessage("T_IBLOCK_DESC_NEWS"),
			"SORT" => 1,
			"CHILD" => array(
				"ID" => "news_cmpx",
			),
		),
	),
);

?>