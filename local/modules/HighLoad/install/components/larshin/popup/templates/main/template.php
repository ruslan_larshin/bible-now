<?//$APPLICATION->SetAdditionalCSS($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/style.css',true);?>
<style>
.main.opacity{
	opacity: 0.2;
}
.closePopup{
	position: absolute;
    right: 11%;
    width: 3%;
	cursor: pointer;
}
.popupContainer{
	display: none;
	width: 99%;
	height: 99%;
	position: absolute;
	left: 0;
	top: 0;
}
.popupContainer.active{
	display: flex;
}
.popup{
	border: 1px solid black;
	border-radius: 2%;
	background-color: white;
	margin-top: 10%;
	margin-left:10%;
	margin-right: 10%;
	margin-bottom: 10%;
	width: 79%;
	height: 69%;
	align-items: center;
	justify-content: center;
}
.contentPopup{
	overflow: auto;
    width: 85%;
    height: 80%;
	margin: 6%;
}
</style>
<div class='popupContainer'> <?//origin_name ������������ ��� scripts and js?>
	<div class='popup'>
		<img class='closePopup' src='/local/img/close.png' />
		<div class='contentPopup'>
		</div>
	</div>
</div>
<script>
function popupOpen(){
	$('.popupContainer').addClass('active');
	//$('.main').addClass('opacity');
	$(".contentPopup").html('');	
}
function popupClose(){
	$('.popupContainer').removeClass('active');
	//$('.main').removeClass('opacity');
}
$('body').on('click','.closePopup',function(){
	popupClose();
});
</script>
