<div class='itemListFirst'>
	<div class='hgloadId'>ID</div>
	<div class='hgloadPicture'>Изображение</div>
	<div class='hgloadName'>Наименование</div>
	<div class='hgloadPrice'>Цена</div>
	<div class='hgloadDate'>Дата</div> 
	<div class='hgloadEdit'>Редактировать</div> 
</div>
<br/>
<?php foreach($arResult['ITEMS'] as $item){?> 
	<form id='form<?=$item['ID']?>'>
		<div class='itemList itemList<?=$item['ID']?>'>
			<div class='hgloadId flex'><div class="control__button"></div><?=$item['ID']?></div>
			<div class='hgloadPicture'><img src="<?=$item['PICTURE']?>"/></div>
			<div class='hgloadName'><?=$item['NAME']?></div>
			<div class='hgloadPrice'><?=$item['PRICE']?></div>
			<div class='hgloadDate'><?=$item['DATE']?></div>
			<div class='hgloadEdit'>
				<div class='editPopup btn btnEdit' data-id='<?=$item['ID']?>'>Редактировать</div>
				<div class='deletePopup btn btnDel' data-id='<?=$item['ID']?>'>Удалить</div>
				<div class='save blockBlue' data-id='<?=$item['ID']?>'>Сохранить</div>
				
			</div>
		</div>
	</form>
	<div class='result<?=$item['ID']?>'></div>
<?}?>
<div class='pager'>
	<?$APPLICATION->IncludeComponent("larshin:pager", "chapter_no_ajax", array('ALL_PAGE'=>$arResult['PAGER']['PAGE_COUNT'],'PAGE'=>$arParams['PAGE']),false);?> 
</div>