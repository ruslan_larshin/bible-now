<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
$params = $arParams;
?>
<div class="componentClass <?=$params['class']?>">
    <div class='titleSearch'><?=$params['title']?></div>
    <div class='searchSing active searchInputDiv' data-search='<?=$request["search"]?>'>
        <input class='searchInput  searchSingInput' type='text' placeholder='<?=$params['placeholder']?>' value='<?=$request["search"]?>'/>
    </div>
    <br/>
    <div class='singResult searchResultComponent active' data-search='search=<?=$request["search"]?>'>
      <?require_once($_SERVER["DOCUMENT_ROOT"] . $params['ajaxUrl']);?>
    </div>
</div>
<script>
    $(function() {
        let singSearch = new MainSearch(
            "<?=$params['class'] ?? 'search'?>",
            "<?=$params['ajaxUrl'] ?? '/ajax.php'?>",
            "<?=$params['url'] ?? '/'?>",
            "<?=$params['pause'] ?? '1000'?>"
        );
    });
</script>