class MainSearch{
    constructor (componentClass, urlAjax, url, pause = 1000) {
        this.componentClass = componentClass;
        this.urlAjax = urlAjax;
        this.url = url;
        this.pause = parseInt(pause);
        this.input = $('.componentClass.' + this.componentClass + ' .searchInput');
        this.result = $('.componentClass.' + this.componentClass + ' .searchResultComponent');
        this.loadJsFromSearch();
        this.loadInput();
    }

    loadJsFromSearch() {
        let obj = this;
        $('body').on('keyup', this.input, function (event) {
            obj.updateBySearch();
        });
        $('body').on('click', this.input, function (event) {
            obj.updateBySearch();
        });
        $('body').on('blur', this.input, function (event) {
            obj.updateBySearch();
        });
        $('body').on('keypress', this.input, function (event) {
            var keyCode = event.keyCode ? event.keyCode :
                event.charCode ? event.charCode :
                    event.which ? event.which : void 0;
            if (keyCode == 13) {
                obj.updateBySearch(true);
            }
        });
        setInterval(obj.updateBySearch.bind(this), obj.pause);
    }



    updateFunction(){
        let obj = this;
        let data = {
            'search' : this.input.val(),
        };
        if(obj.result.hasClass('stop')){
            return false;
        }
        obj.result.addClass('stop');
        $.ajax({
            type: 'POST',
            url: obj.urlAjax,
            data: data,
            dataType: 'html',
            success: function (html) {
                obj.result.removeClass('stop');
                obj.result.html(html);
                obj.result.attr('data-search', data['search']);
                obj.input.attr('data-search', data['search']);
                history.pushState(null, null, '?search=' + data['search']);
            },
        });
    }

    updateBySearch(enter = false){
        let obj = this;
        let date = Date.now();
        let keyUp =  obj.input.attr('data-keyUp');
        obj.input.attr('data-keyUp', date);
        if((parseInt(date) - parseInt(keyUp)) < obj.pause/2){
            if(!enter) {
                return false;
            }
        }
        let timeLastUpdate = obj.input.attr('data-time') ?? 0;
        if( ((parseInt(date) - parseInt(timeLastUpdate)) > obj.pause) || enter) {
            obj.input.attr('data-time', date);
            let dataSearch = obj.input.attr('data-search');
            let valSearch = obj.input.val();
            if (!valSearch && !dataSearch) {
                return false;
            }
            if (valSearch !== dataSearch) {
                //obj.input.attr('data-search', valSearch);
                obj.updateFunction();
            }
            return true;
        }else{
            return false;
        }
    }



    loadInput() {
        let obj = this;
        $('body').on('keypress', this.input, function (event) {
            let date = Date.now();
            obj.input.attr('data-time', date);
        });
        setInterval(this.enterAfter.bind(this), obj.pause);
    }

    enterAfter() {
        let obj = this;
        var old = parseInt(this.input.attr('data-time'));
        var now = Date.now();
        if (now - old > obj.pause && old > obj.pause) {
            var e = jQuery.Event("keypress");
            e.which = 13; //choose the one you want
            e.keyCode = 13;
            obj.input.trigger(e);
        }
        return true;
    }

}