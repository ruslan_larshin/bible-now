<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\SystemException;

class MainForm extends CBitrixComponent
{
  public function executeComponent()
  {
    try {
      $this->getResult();
    } catch (SystemException $e) {
      ShowError($e->getMessage());
    }
  }


  protected function getResult()
  {
    if ($this->startResultCache()) {
      $this->SetResultCacheKeys(
        []
      );
      $this->IncludeComponentTemplate();
    }
  }
}
