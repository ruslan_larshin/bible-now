<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
Asset::getInstance()->addCss("/local/styles/order.css");
$form = $arParams['form'];
?>

<div class="form <?=$form['class']?>">
    <?foreach($form['items'] as $item){?>
        <?if($item['type'] == 'select'){?>
            <div class="flex formItem mobileNoFlex  <?=$item['class']?>">
                <div class="label  "><?=$item['title']?> <?if($item['require']){?>*<?}?> : </div>
                <div class="inputBlock">
                    <select class="inputWellenta inputFormData   <?if($item['require']){?>require<?}?> <?=$item['code']?> <?=$item['type']?>"
                           value="<?=$item['value']?>"
                           type="text"
                           data-code="<?=$item['code']?>"
                           data-title="<?=$item['title']?>"
                           placeholder="<?=$item['placeholder']?>
                        ">
                        <option value=""><?=$item['placeholder']?></option>
                        <?foreach($item['items'] as $code => $value){?>
                            <option value="<?=$value?>" <?if(!empty($item['value'] && ($item['value'] == $code))){?> selected <?}?>><?=$value?></option>
                        <?}?>
                    </select>
                    <div class="error <?=$item['code']?> alert errorForm">

                    </div>
                </div>
            </div>
        <?}else{?>
            <div class="flex formItem mobileNoFlex  <?=$item['class']?>">
                <div class="label  "><?=$item['title']?> <?if($item['require']){?>*<?}?> : </div>
                <div class="inputBlock">
                    <input class="inputWellenta  <?if($item['require']){?>require<?}?> <?if($item['length']){?>length<?}?> inputFormData <?=$item['code']?> <?=$item['type']?>"
                           value="<?=$item['value']?>"
                           type="text"
                           data-title="<?=$item['title']?>"
                           <?if($item['length']){?> data-length="<?=$item['length']?>"<?}?>
                           data-code="<?=$item['code']?>"
                           placeholder="<?=$item['placeholder']?>"
                           <?if($item['disabled']){?>
                            disabled
                           <?}?>
                    >
                    <div class="error <?=$item['code']?> alert errorForm">

                    </div>
                </div>
            </div>
        <?}?>
    <?}?>
  <?if(!empty($form['agree'])){?>
      <div class="agreeDiv flex">
        <input type="checkbox" class="agree" />
        <a href ='<?=$form['agree']['href']?>' download>
            <div class="text"><?=$form['agree']['title']?></div>
        </a>
      </div>

  <?}?>
    <?if(!empty($form['button'])){?>
      <div class="buttonWellenta buttonForm <?=$form['button']['class']?>" onclick="<?=$form['button']['jsFunction']?>"><?=$form['button']['title']?></div>
    <?}?>
      <?if(!empty($form['buttonAfter'])){?>
          <div class="buttonWellenta noDisplay buttonFormAfter <?=$form['buttonAfter']['class']?>" onclick="<?=$form['buttonAfter']['jsFunction']?>"><?=$form['buttonAfter']['title']?></div>
      <?}?>
    <div class="formResult"></div>
    <div class="formResultErrors"></div>
</div>