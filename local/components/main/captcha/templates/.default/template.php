<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
$captcha = \Main\Captcha::load()['captcha'];
$code = $captcha['code']
?>
<div class="mainCaptcha <?=$arParams['class']?>" data-template="<?= $templateFolder ?>">
    <div class="captcha">
        <div class="text">Подтвердите, что вы не робот: </div>
        <input type="hidden" class='captchaSid' name="captcha_sid" value="<?=$code;?>" />
        <div class="flex reloadDivFlex">
            <div class="captchaBorder">
                <img class="captchaImg" src="/bitrix/tools/captcha.php?captcha_sid=<?=$code?>"  alt="CAPTCHA" />
            </div>
            <div class="reloadDiv">
                <img class='reload' src="/local/img/reload.png"/>
            </div>
        </div>
        <div class="flex reloadDivFlex">
            <div class="captchaInput">
                <input class='captchaMainInput' name="captcha_word" type="text" value="" />
            </div>
            <div class="button buttonWellenta buttonCaptchaEnter flexCenter">OK</div>
        </div>
        <div class="resultText">
            <div class="success green"></div>
            <div class="error red"></div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        let captcha = new Captcha("<?=$arParams['class'] ?? 'none'?>", <?=$arParams['successFunction']?>);
    });
</script>