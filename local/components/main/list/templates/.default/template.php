<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
?>
<div class="listComponents <?=$arParams['class']?>">
    <?foreach($arParams['items'] as $item){?>
      <div class="listItem">
            <?if(!empty($item['href'])){?>
              <a href ='<?=$item['href']?>' ><?=$item['title']?></a>
            <?}?>
            <?if(!empty($item['popup'])){?>
                <div class="popupOpenButton" data-url="<?=$item['popup']?>">
                  <?=$item['title']?>
                </div>
            <?}?>
      </div>
    <?}?>
</div>
