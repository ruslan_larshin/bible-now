<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
?>
<div class="MainPagerComponent <?= $arParams['class'] ?> flex"
     data-class="<?= $arParams['class'] ?>"
     data-url='<?= $arParams['url'] ?>'
>
    <div class='pager'>
      <? foreach ($arResult['pagers'] as $pagerItem) { ?>
          <div class='pagerCantainer <? if ($pagerItem['code'] == $arParams['pageNumber']) {
            echo 'active';
          } ?>'>
              <a href='<?= $arParams['url'] ?>/<?= $pagerItem['urlCode'] ?>/'><?= $pagerItem['code'] ?></a>
          </div>
      <? } ?>
    </div>
    <div class="inputChapterBlock flexCenter">
      <? if ($arParams['pageNumber'] > 1) { ?>
          <a href="<?= $arParams['url'] ?>/<?= ($arParams['pageNumber'] - 1) ?>/">
              <div class="preview"><?= $GLOBALS['svg']['arrowLeft'] ?></div>
          </a>
      <? } elseif (!empty($arParams['book']['preview'])) { ?>
              <a href="/bible/<?= $arParams['book']['preview']['code'] ?>/<?= $arParams['book']['preview']['chapterCount'] ?>/">
                  <div class="preview"><?= $GLOBALS['svg']['arrowLeft'] ?></div>
              </a>
      <? } ?>
        <div class="inputChapter flexCenter">
            <input type="text" class="inputChapterNumber number" data-class="<?= $arParams['class'] ?>"
                   value="<?= $arParams['pageNumber'] ?>" data-maxValue="<?= $arParams['pageCount'] ?>"/>
            <div class="chapterGo"><?= $GLOBALS['svg']['rowRight'] ?></div>
        </div>

      <? if ($arParams['pageNumber'] < $arParams['book']['now']['chapterCount']) { ?>
          <a href="<?= $arParams['url'] ?>/<?= ($arParams['pageNumber'] + 1) ?>/">
              <div class="next"><?= $GLOBALS['svg']['arrowRight'] ?></div>
          </a>
      <? } elseif (!empty($arParams['book']['next'])) { ?>
          <a href="/bible/<?= $arParams['book']['next']['code'] ?>/1/">
              <div class="next"><?= $GLOBALS['svg']['arrowRight'] ?></div>
          </a>
      <? } ?>
    </div>
</div>
