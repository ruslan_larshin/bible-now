$('document').ready(function(){
    $('body').on('click', '.MainPagerComponent .chapterGo', function(){
        let button = this;
        let className = $(this).parents('.MainPagerComponent').attr('data-class');
        let url = $(this).parents('.MainPagerComponent').attr('data-url');
        let value = $('.MainPagerComponent.' + className + ' .inputChapterNumber').val();
        if(value && url){
            window.location.href = url + '/' + value + '/';
        }
    });

    $('body').on('keypress', '.inputChapterNumber', function (event) {
        let className = $(this).parents('.MainPagerComponent').attr('data-class');
        let button = $('.MainPagerComponent.' + className + ' .chapterGo');
        let keyCode = event.keyCode ? event.keyCode :
            event.charCode ? event.charCode :
                event.which ? event.which : void 0;
        if (keyCode == 13) {
            button.click();
        }
    });
});