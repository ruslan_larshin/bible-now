<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");

?>
<div class="bibleBreadcrumbs" data-template="<?= $templateFolder ?>">
    <div class='Breadcrumbs Flex'>
      <?foreach($arParams['items'] as $breadcrumbs){?>
          <div class='font blue bc'>
              <a class = 'blue noDecoration' <?if(!empty($breadcrumbs['url'])){?>href="<?=$breadcrumbs['url']?>"<?}?>>
                <?=$breadcrumbs['title']?>
                <?if(!empty($breadcrumbs['url'])){?>
                    &nbsp;<img class="open" data-id="1" src="/local/img/bcArrow2.png" />&nbsp;
                <?}?>
              </a>
          </div>
      <?}?>
    </div>
</div>
