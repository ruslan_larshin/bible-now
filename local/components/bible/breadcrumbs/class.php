<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

class BibleBreadcrumbs extends CBitrixComponent
{
  public function executeComponent()
  {
    try {
      $this->getResult();
    } catch (SystemException $e) {
      ShowError($e->getMessage());
    }
  }

  protected function getResult()
  {
      $this->IncludeComponentTemplate();
  }
}
