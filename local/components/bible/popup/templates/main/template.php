<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
?>
<div class='popupContainer'>
    <div class='popup'>
        <div class="closePopupContainer">
            <img class='closePopup' src='/local/img/close.png'/>
        </div>
        <div class='contentPopup popupResultAjax'>
            <div class="popupAjax"></divclass>
        </div>
    </div>
</div>

