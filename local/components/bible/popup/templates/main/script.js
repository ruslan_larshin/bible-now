$(document).ready(function(){
    $('body').on('click', '.popupOpenButton', function(){
        if(!$(this).hasClass('disabled')){
            let button = $(this);
            $(this).addClass('disabled');
            $(this).addClass('load');
            let url = $(this).attr('data-url');
            if(url){
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'html',
                    success: function (html) {
                        $('.popupResultAjax').html(html);
                        popupOpen();
                        button.removeClass('disabled');
                        button.removeClass('load');
                    },
                    error: function (data) {
                        button.removeClass('disabled');
                        button.removeClass('load');
                        let json = $.parseJSON(data.responseText);
                        if (json.error) {
                            $('.error').html(json.error);
                        }
                    }
                });
            }
        }
    });

    $('body').on('click', '.popupOpenButtonData', function(){
        if(!$(this).hasClass('disabled')){
            let button = $(this);
            $(this).addClass('disabled');
            $(this).addClass('load');
            let url = $(this).attr('data-url');
            let data = {
                'id' : $(this).attr('data-id'),
                'path' : $(this).attr('data-path'),
                'src' : $(this).attr('data-src'),
                'imgs' : $(this).attr('data-imgs'),
                'key' : $(this).attr('data-key'),
            };
            console.log(url);
            if(url){
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    success: function (html) {
                        $('.popupResultAjax').html(html);
                        popupOpen();
                        button.removeClass('disabled');
                        button.removeClass('load');
                    },
                    error: function (data) {
                        button.removeClass('disabled');
                        button.removeClass('load');
                        let json = $.parseJSON(data.responseText);
                        if (json.error) {
                            $('.error').html(json.error);
                        }
                    }
                });
            }
        }
    });

    $('body').on('click', '.closePopup', function () {
        popupClose();
    });
});


    function popupOpen() {
        $('.popupContainer').addClass('active');
        $('.main').addClass('opacity');
    }

    function popupClose() {
        $('.popupContainer').removeClass('active');
        $('.main').removeClass('opacity');
    }


