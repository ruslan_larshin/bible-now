$('document').ready(function(){
    $('body').on('click','.chapterBlock .copy', function(){
        if($('.chapterBlock .copy.active').length === 0){
            Tooltip.view($(this), 'скопировано');
        }
        $(this).toggleClass('active');
        copyChapter();
    });

    function copyChapter(){
        let text = '';
        let verses = '';
        $($('.chapterBlock .copy.active').each(function(){
            let number = $(this).parents('.verse').children('.verseNumber').text();
            verses += number;
            text += number + $(this).parents('.verse').children('.verseText').text() + "\r\n";
        }))
        let path = '[ ' +$('.chapterBlock').attr('data-short') + ' ' + $('.chapterBlock').attr('data-chapter') + ' : '  + verses.replaceAll('.',',').slice(0,-1) + ' ]';
        text += "\r\n" + path;
        navigator.clipboard.writeText(text).then(function () {
            $(this).prop('title', 'скопировано');
        }, function () {
            alert('Failure to copy. Check permissions for clipboard')
        });
    }
});