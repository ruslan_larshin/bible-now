<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
global $APPLICATION;
?>
<div class="BibleChapterComponent">
    <div class='title1 bold'>Книга <?= $arResult['book']['now']['fullName'] ?> <span
                class=''> <?= $arParams['chapter'] ?> глава</span>
    </div>

  <? $APPLICATION->IncludeComponent(
    'main:pager',
    '',
    [
      'pageCount' => $arResult['book']['now']['chapterCount'],
      'pageNumber' => $arParams['chapter'],
      'url' => "/bible/{$arResult['book']['now']['code']}",
      'class' => 'first',
      'book' => $arResult['book'],
    ],
  ); ?>

    <div class="chapterBlock" data-short ="<?=$arResult['book']['now']['shortName']?>" data-chapter="<?= $arParams['chapter'] ?>">
      <? foreach ($arResult['verses'] as $verse) { ?>
          <div class='verse'>
              <div class="copy flexCenter">
                <span class="svg"><?=$GLOBALS['svg']['copy']?></span>
                <span class="ok"><?=$GLOBALS['svg']['active']?></span>
              </div>
              <div class='verseNumber font'><?= $verse['verseNumber'] ?>.</div>
              <div class='verseText'><?= $verse['text'] ?></div>
          </div>
      <? } ?>
    </div>

  <? $APPLICATION->IncludeComponent(
    'main:pager',
    '',
    [
      'pageCount' => $arResult['book']['now']['chapterCount'],
      'pageNumber' => $arParams['chapter'],
      'url' => "/bible/{$arResult['book']['now']['code']}",
      'class' => 'second',
      'book' => $arResult['book'],
    ],
  ); ?>

    <div class="bottomNavigation flexCenter">
        <?if($arParams['chapter'] > 1){?>
        <a href="/bible/<?= $arResult['book']['now']['code'] ?>/<?= ($arParams['chapter'] - 1) ?>/}">
            <div><?= $arResult['book']['now']['name'] ?> <?= ($arParams['chapter'] - 1) ?> глава</div>
        </a>
        <?}elseif(!empty($arResult['book']['preview'])){?>
            <a href="/bible/<?= $arResult['book']['preview']['code'] ?>/<?= $arResult['book']['preview']['chapterCount'] ?>/}">
                <div><?= $arResult['book']['preview']['name'] ?> <?= $arResult['book']['preview']['chapterCount'] ?> глава</div>
            </a>
        <?}?>
        <div>
            <div class="preview"><?= $GLOBALS['svg']['arrowLeft'] ?></div>
        </div>
        <div><?= $arResult['book']['now']['name'] ?> <?=$arParams['chapter']?></div>
        <div>
            <div>
                <div class="preview"><?= $GLOBALS['svg']['arrowRight'] ?></div>
            </div>
        </div>
      <?if($arParams['chapter'] < $arResult['book']['now']['chapterCount']){?>
        <a href="/bible/<?= $arResult['book']['now']['code'] ?>/<?= ($arParams['chapter'] + 1) ?>/">
            <div><?= $arResult['book']['now']['name'] ?> <?= ($arParams['chapter'] + 1) ?> глава</div>
        </a>
      <?}elseif(!empty($arResult['book']['next'])){?>
          <a href="/bible/<?= $arResult['book']['next']['code'] ?>/1/">
              <div><?= $arResult['book']['next']['name'] ?> 1 глава</div>
          </a>
      <?}?>
    </div>
</div>
