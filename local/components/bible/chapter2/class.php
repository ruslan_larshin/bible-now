<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

class BibleChapter2 extends CBitrixComponent
{
  public function executeComponent()
  {
    try {
      $this->getResult();
    } catch (SystemException $e) {
      ShowError($e->getMessage());
    }
  }

  protected function getResult()
  {
    $chapter = \Bible\Verses::getVerses($this -> arParams['book'], $this -> arParams['chapter']);

    $books = \Bible\Books::getListIblock(['cacheTime' => \Main\Settings::cashTimeD7])['books'];
    $firstVerse = '';
    $i =0;
    foreach($chapter['verses'] as $verse) {
      $i++;
      if ($i <= 3) {
        $firstVerse .= "{$verse['text']} \r\n";
      }
    }
    foreach ($books as $keyBook => $book) {
      if (
        ($book['name'] == $this -> arParams['book']) || ($book['fullName'] == $this -> arParams['book'])
        || ($book['shortName'] == $this -> arParams['book']) || ($book['code'] == $this -> arParams['book'])
      ) {
        $chapter['book'] = [
          'now' => $book,
          'preview' => $books[$keyBook - 1],
          'next' => $books[$keyBook + 1],
        ];
      }
    }
    global $APPLICATION;
    $APPLICATION->SetTitle("Библия : {$chapter['book']['now']['name']} {$this -> arParams['chapter']} глава");
    $APPLICATION->SetPageProperty('title', "Библия : {$chapter['book']['now']['name']} {$this -> arParams['chapter']} глава");
    $APPLICATION->SetPageProperty("keywords", "Библия, {$chapter['book']['now']['name']}, {$chapter['book']['now']['fullName']}");
    $APPLICATION->SetPageProperty("description", "Библия : {$chapter['book']['now']['fullName']} {$this -> arParams['chapter']} глава. \r\n {$firstVerse}");
    $this -> arResult = $chapter;
    $this->IncludeComponentTemplate();
  }
}
