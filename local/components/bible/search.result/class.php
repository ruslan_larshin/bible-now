<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

class BibleSearchResult extends CBitrixComponent
{
  public function executeComponent()
  {
    try {
      $this->getResult();
    } catch (SystemException $e) {
      ShowError($e->getMessage());
    }
  }

  protected function getResult()
  {
    $options = [
      'search' => $this -> arParams['search'],
    ];
    $cache = \Bitrix\Main\Data\Cache::createInstance();
    if ($cache->initCache(\Main\Settings::cashTimeD7, json_encode($options))) {
      $this -> arResult = $cache->getVars();
    }
    elseif ($cache->startDataCache()) {
      if(!empty($this -> arParams['search'])){
        $this -> arResult['search'] = $this -> arParams['search'];
        $this -> arResult['result'] = \Learn\Search::searchParams($this -> arParams['search'], false);
      }else{
        return ['error' => 'BibleSearchResult: Необходим текст'];
      }
      $cache->endDataCache($this -> arResult);
    }
    $this->IncludeComponentTemplate();
  }
}
