<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
global $APPLICATION;
$result = $arResult['result'];
$APPLICATION->SetTitle("Поиск по текстам Священного Писания : " . $result['text']);
?>
<div class="searchResultText">
  <div>По запросу : <span class="blue bold">"<?=$result['text']?>"</span> найдено <?=count($result['ids'] ?? [])?> совпадений в <?=count($result['view'] ?? [])?> книгах:</div>
  <?if((count($result['view'] ?? []) > 4) && (count($result['ids'] ?? []) >= 10)){?>
    <div class="flex bookListHrefs">
        <?foreach($result['view'] as $book){?>
        <div class="bookHref">
            <a href="#<?=$book['code']?>">
                <?=$book['name']?> [<?=count($book['items'] ?? [])?>]
            </a>
        </div>
        <?}?>
    </div>
  <?}?>
  <?foreach($result['view'] as $book){?>
    <div id="<?=$book['code']?>" class="bookAnchor"><?=$book['fullName']?></div>
      <?foreach($book['items'] as $verse){?>
        <div class="verseItem flex">
          <div class="path quoteDetailTextArticle popupOpenButtonData"
               data-url="/local/ajax/bible/chapter/"
               data-path="<?=$book['shortName']?> <?=$verse['chapter']?> : <?=$verse['verseNumber']?>"
          >
            <?=$book['shortName']?> <?=$verse['chapter']?> : <?=$verse['verseNumber']?>
          </div>
          <div>
            <?=$verse['bold'] ?? $verse['text']?>
          </div>
        </div>
      <?}?>
  <?}?>
</div>
