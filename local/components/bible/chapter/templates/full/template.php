<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
global $APPLICATION;
?>
<div class="BibleVerseComponent full">
    <?foreach($arResult['verses'] as $verse){?>
        <div>
            <span class="<?if(empty($verse['bold'])){?>opacityVerse<?}else{?>black<?}?>"><?=$verse['verseNumber']?>. </span>
            <span class="<?if(empty($verse['bold'])){?>opacityVerse<?}else{?>black<?}?>"><?=$verse['text']?></span>
        </div>
    <?}?>
    <div class="path">[ <?=$arResult['path']?> ]</div>
</div>
