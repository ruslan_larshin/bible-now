<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

class BibleChapter extends CBitrixComponent
{
  public function executeComponent()
  {
    try {
      $this->getResult();
    } catch (SystemException $e) {
      ShowError($e->getMessage());
    }
  }

  protected function getResult()
  {
    $options = [
      'path' => $this -> arParams['path'],
    ];
    $cache = \Bitrix\Main\Data\Cache::createInstance();
    if ($cache->initCache(\Main\Settings::cashTimeD7, json_encode($options))) {
      $this -> arResult = $cache->getVars();
    }
    elseif ($cache->startDataCache()) {
      if(!empty($this -> arParams['path'])){
        $this -> arResult['path'] = $this -> arParams['path'];
        if(!empty($this -> arParams['verses'])){
          $this -> arResult['verses'] = $this -> arParams['verses'];
        }else{
          $this -> arResult['verses'] = \Bible\Verses::getVersesByPath($this -> arParams['path'], $this -> arParams['full'] ?? false)['verses'];
        }
      }else{
        return ['error' => 'BibleVerse: Необходим путь'];
      }
      $cache->endDataCache($this -> arResult);
    }
    $this->IncludeComponentTemplate();
  }
}
