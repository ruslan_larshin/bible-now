<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
global $APPLICATION;
?>
<div class="BibleVerseComponent popupOpenButtonData" data-url="/local/ajax/bible/chapter/" data-path="<?=$arResult['path']?>">
    <?$i = 0;?>
    <?foreach($arResult['verses'] as $verse){?>
        <div class="" >
          <?if($i == 0){?>
                <span class="black">"</span>
            <?}else{?>
              <span class="opacity">"</span>
            <?}?>

                <span><?=$verse['verseNumber']?>. </span><span><?=$verse['text']?></span>

            <?if($i == count($arResult['verses'] ?? []) - 1){?>
                <span class="black">"</span>
            <?}?>
        </div>
        <?$i++;?>
    <?}?>
    <div class="path">[ <?=$arResult['path']?> ]</div>
</div>
