<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

class BibleVerse extends CBitrixComponent
{
  public function executeComponent()
  {
    try {
      $this->getResult();
    } catch (SystemException $e) {
      ShowError($e->getMessage());
    }
  }

  protected function getResult()
  {
    if ($this->startResultCache()) {
      $this->SetResultCacheKeys(
        []
      );
      if(!empty($this -> arParams['path'])){
        $this -> arResult['path'] = $this -> arParams['path'];
        if(!empty($this -> arParams['verses'])){
          $this -> arResult['verses'] = $this -> arParams['verses'];
        }else{
          $this -> arResult['verses'] = \Bible\Verses::getVersesByPath($this -> arParams['path'], $this -> arParams['full'] ?? false)['verses'];
        }
      }else{
        return ['error' => 'BibleVerse: Необходим путь'];
      }
      $this->IncludeComponentTemplate();
    }
  }
}
